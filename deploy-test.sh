#!/bin/bash
$(aws ecr get-login --no-include-email)
echo y | docker system prune

## FRONT ##
echo "Building front container"
## deploy test env.
cp $WORKSPACE/secret-test/environment.ts $WORKSPACE/front/app/environment.ts
## Buikd and push
cd $WORKSPACE/front
docker build -t kikmoov-front .
cd $WORKSPACE
docker tag kikmoov-front:latest 853707894985.dkr.ecr.eu-west-2.amazonaws.com/kikmoov-front:latest
docker push 853707894985.dkr.ecr.eu-west-2.amazonaws.com/kikmoov-front:latest
###########

### UPDATE ECS SERVICES
echo "Updating ecs services"
# Declare an array of string with type
declare -a services=("kikmoov-front")
 
# Iterate the string array using for loop
for service in ${services[@]}; do
    aws ecs update-service --cluster kikmoov  --service $service --force-new-deployment
    echo "Service $service updated successfully"
done
