import { library } from '@fortawesome/fontawesome-svg-core';

import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons/faLinkedinIn';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons/faEnvelope';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

const icons = <IconDefinition[]>[
  faTwitter, faEnvelope, faLinkedinIn, faFacebookF
];

library.add(...icons);
