import {AfterViewInit, Component, Inject, OnDestroy, OnInit, PLATFORM_ID} from '@angular/core';

import { MetaService } from '@ngx-meta/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {UserService} from './services/user.service';
import {Title} from '@angular/platform-browser';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

    private isBrowser;

    private eventsSubscription: Subscription;
    private dataSubscription: Subscription;
    private intercomContainer;

    private allowedUrisToLoadScript = [
        '/',
        'findaspace',
        'blog',
        'postspace',
        'how-it-works',
        '/#testimonials'
    ];

    private userData;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private titleService: Title,
              @Inject(PLATFORM_ID) private platformId
  )
  {
      this.isBrowser = isPlatformBrowser(platformId);
  }

    ngOnInit() {
        this.userService.userSubject.subscribe(userData => {
            this.userData = userData;
        });
    }

    ngAfterViewInit() {
    if (this.isBrowser) {
        this.eventsSubscription = this.router.events.subscribe((e) => {
            if (e instanceof NavigationEnd) {
                if (this.userService.isLoggedin()) {
                    this.userData = this.userService.getUserData();
                }
                if (this.checkAllowedUri(this.router.url) && (!this.intercomContainer || this.userData)) {
                    this.loadScript();
                }
                const fragment = <BehaviorSubject<any>>this.route.fragment;
                if (fragment.value) {
                    const destinationElement = document.getElementById(fragment.value);
                    setTimeout(() => {
                        destinationElement.scrollIntoView(true);
                    }, 50);
                } else {
                    window.scrollTo(0, 0);
                }
                if (this.route.firstChild) {
                    this.dataSubscription = this.route.firstChild.data.subscribe(data => {
                        if ('title' in data) {
                            this.titleService.setTitle(data['title']);
                        }
                    });
                }
                this.intercomScript();
            }
        });
    }
    }

    ngOnDestroy() {
        if (this.isBrowser) {
            this.eventsSubscription.unsubscribe();
            this.dataSubscription.unsubscribe();
        }
    }

    intercomScript() {
        if (this.isBrowser) {
            this.intercomContainer = document.getElementById('intercom-container');
            if (this.checkAllowedUri(this.router.url)) {
                if (this.intercomContainer) this.intercomContainer.style.visibility = 'visible';
                return;
            } else {
                if (this.intercomContainer) this.intercomContainer.style.visibility = 'hidden';
                return;
            }
        }
    }

    checkAllowedUri(uri) {
        if (this.isBrowser) {
            let allowed = false;
            this.allowedUrisToLoadScript.forEach(v => {
                if (v !== '/' && uri.includes(v)) allowed = true;
            });
            return allowed || this.allowedUrisToLoadScript.find(v => v === uri);
        }
    }

    loadScript() {
        if (this.isBrowser) {
            let intercomSettings: any = {
                app_id: "ucpz0mg7"
            };
            if (this.userData) {
                intercomSettings = {
                    ...intercomSettings,
                    name: this.userData['fullname'],
                    email: this.userData['email']
                }
            }
            window['intercomSettings'] = intercomSettings;
            const w = window;
            const ic = w['Intercom'];
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', w['intercomSettings']);
            } else {
                const d = document;
                const i = function () {
                    i['c'](arguments);
                };
                i['q'] = [];
                i['c'] = function (args) {
                    i['q'].push(args);
                };
                w['Intercom'] = i;
                const l = function () {
                    const s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/ucpz0mg7';
                    const x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                };
                l();
            }
        }
    }
}
