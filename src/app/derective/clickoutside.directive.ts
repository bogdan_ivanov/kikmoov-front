import {Directive, ElementRef, Output, EventEmitter, HostListener, HostBinding} from '@angular/core';

@Directive({
    selector: '[appClickoutside]'
})

export class ClickoutsideDirective {

    @HostBinding('class.active') showDropdown: boolean;

    constructor(private _elementRef: ElementRef) {
    }

    @Output()
    public clickOutside = new EventEmitter<MouseEvent>();

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if (!targetElement) {
            return;
        }

        const clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.showDropdown = false;
            this.clickOutside.emit(event);
        } else {
            this.showDropdown = !this.showDropdown;
        }
    }
}