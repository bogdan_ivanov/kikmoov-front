import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signinvoice',
  templateUrl: './signinvoice.component.html',
  styleUrls: ['./signinvoice.component.scss']
})
export class SigninvoiceComponent implements OnInit {
  private user = {};
  public errMsg = '';
  public success = false;
  public invoiceFormGroup = this.fb.group({
    invoiceEmail: [null, [Validators.required, Validators.email]],
    companyName: [null, [Validators.required]],
    address: [null, [Validators.required]],
    optionalAddress: [null, []],
    town: [null, [Validators.required]],
    postcode: [null, [Validators.required]]
  });

  constructor(
    private userServise: UserService,
    private router: Router,
    private route: ActivatedRoute,
    public fb: FormBuilder
  ) {
    this.route.queryParams.subscribe(params => {
      if ('email' in params) {
        this.invoiceFormGroup.controls['invoiceEmail'].setValue(params['email']);
        this.user = this.userServise.getUserData();
      }
    });
  }

  ngOnInit() {}

  doInvoiceSignup(form) {
    this.errMsg = '';
    if (form.valid) {
      this.userServise
        .registerSeller(this.user, this.invoiceFormGroup.value)
        .subscribe(
          res => {
            this.success = true;
            setTimeout(() => {
              this.userServise.clearUserData();
              this.router.navigate(['/signin/seller']);
            }, 5000);
          },
          err => {
            this.errMsg = err.message;
          }
        );
    } else {
      Object.keys(form['controls']).forEach(control => {
        form.get(control).markAsTouched();
      });
    }
  }
}
