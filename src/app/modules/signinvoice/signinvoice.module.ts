import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { UserService } from '../../services/user.service';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SigninvoiceRoutes } from './signinvoice.routing';
import { SigninvoiceComponent } from './signinvoice.component';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';

@NgModule({
  imports: [
      CommonModule,
      SigninvoiceRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule.forRoot(),
      MatCardModule,
      FormsModule,
      ReactiveFormsModule,
      MatFormFieldModule,
      PostSpaceBarModule
  ],
  declarations: [
      SigninvoiceComponent
  ],
    providers: [
        UserService
    ]
})
export class SigninvoiceModule {}
