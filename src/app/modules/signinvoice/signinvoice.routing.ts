import { Routes, RouterModule } from '@angular/router';
import { SigninvoiceComponent } from './signinvoice.component';

const routes: Routes = [
  {
    path: '',
    component: SigninvoiceComponent,
    data: {
      meta: {
        title: 'invoicing.title',
        description: 'invoicing.text',
        override: true,
      },
    },
  },
];

export const SigninvoiceRoutes = RouterModule.forChild(routes);
