import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninvoiceComponent } from './signinvoice.component';

describe('SigninvoiceComponent', () => {
  let component: SigninvoiceComponent;
  let fixture: ComponentFixture<SigninvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
