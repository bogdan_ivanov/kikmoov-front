import { Routes, RouterModule } from '@angular/router';
import { SavedWorkspacesComponent } from './saved-workspaces.component';

const routes: Routes = [
  {
    path: '',
    component: SavedWorkspacesComponent,
    data: {
      meta: {
        title: 'profileSavedWorkspaces.title',
        description: 'profileSavedWorkspaces.text',
        override: true,
      },
    },
  },
];

export const SavedWorkspacesRoutes = RouterModule.forChild(routes);
