import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { ProfileService } from '../../services/profile.service';
import { SavedWorkspacesRoutes } from './saved-workspaces.routing';
import { SavedWorkspacesComponent } from './saved-workspaces.component';
import { WorkspaceModule } from '@shared/layouts/workspace/workspace.module';

@NgModule({
  imports: [
      CommonModule,
      SavedWorkspacesRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      WorkspaceModule
  ],
  declarations: [
      SavedWorkspacesComponent
  ],
    providers: [
        ProfileService
    ]
})
export class SavedWorkspacesModule {}
