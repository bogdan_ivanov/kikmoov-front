import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-saved-workspaces',
  templateUrl: './saved-workspaces.component.html',
  styleUrls: ['./saved-workspaces.component.scss']
})
export class SavedWorkspacesComponent implements OnInit {
  public workSpaces;
  public workSpaceTypes = {
    desk: 'Desk',
    'private-office': 'Private Office',
    'meeting-room': 'Meeting Space'
  };

  public deskTypeNames = {
    hourly_hot_desk: 'Hot Desk',
    monthly_hot_desk: 'Desk',
    monthly_fixed_desk: 'Fixed Desk'
  };

  constructor(private profileService: ProfileService) {
    this.profileService.getSavedWorkSpaces().subscribe(res => {
      this.workSpaces = res['workspaces'];
    });
  }

  ngOnInit() {
  }

  handleWorkSpaceLike(workspace) {
    this.workSpaces = this.workSpaces.filter(ws => ws['workspace'].id !== workspace.id);
  }

}
