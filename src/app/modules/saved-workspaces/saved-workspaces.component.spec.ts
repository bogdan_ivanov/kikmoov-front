import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedWorkspacesComponent } from './saved-workspaces.component';

describe('SavedWorkspacesComponent', () => {
  let component: SavedWorkspacesComponent;
  let fixture: ComponentFixture<SavedWorkspacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedWorkspacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedWorkspacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
