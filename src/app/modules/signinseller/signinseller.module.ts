import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { MatCardModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SigninsellerRoutes } from './signinseller.routing';
import {SignInSellerComponent} from './signinseller.component';
import {ForgotPasswordModule} from '@shared/layouts/forgot-password/forgot-password.module';
import {PostSpaceBarModule} from '@shared/layouts/post-space-bar/post-space-bar.module';
import {LogindialogModule} from '@shared/layouts/logindialog/logindialog.module';

@NgModule({
  imports: [
      CommonModule,
      SigninsellerRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule.forRoot(),
      MatCardModule,
      FormsModule,
      ReactiveFormsModule,
      PostSpaceBarModule,
      ForgotPasswordModule,
      LogindialogModule,
  ],
  declarations: [
      SignInSellerComponent
  ],
    providers: [
        NgxSmartModalService
    ]
})
export class SigninsellerModule {}
