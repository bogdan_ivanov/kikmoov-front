import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-signinseller',
  templateUrl: './signinseller.component.html',
  styleUrls: ['./signinseller.component.scss'],
})
export class SignInSellerComponent implements OnInit {
  constructor(public ngxSmartModalService: NgxSmartModalService) {}

  ngOnInit() {}

  modalHandler(event) {
    if (event === 'close') {
      this.ngxSmartModalService.getModal('recover').close();
    } else if (event.action === 'close' && event.loggedIn) {
      this.ngxSmartModalService.getModal('recover').close();
    }
  }
}
