import { Routes, RouterModule } from '@angular/router';
import { SignInSellerComponent } from './signinseller.component';

const routes: Routes = [
  {
    path: '',
    component: SignInSellerComponent,
    data: {
      meta: {
        title: 'signinSeller.title',
        description: 'signinSeller.text',
        override: true,
      },
    },
  },
];

export const SigninsellerRoutes = RouterModule.forChild(routes);
