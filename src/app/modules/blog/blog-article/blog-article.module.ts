import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { BlogService } from '../../../services/blog.service';
import { BlogArticleComponent } from './blog-article.component';
import { BlogArticleRoutes } from './blog-article.routing';

@NgModule({
  imports: [
      CommonModule,
      BlogArticleRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      FormsModule,
      ReactiveFormsModule,
      MatFormFieldModule,
  ],
  declarations: [
      BlogArticleComponent
  ],
  providers: [
      BlogService
  ]
})

export class BlogArticleModule {}
