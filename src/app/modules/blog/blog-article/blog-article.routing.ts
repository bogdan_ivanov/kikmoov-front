import { Routes, RouterModule } from '@angular/router';
import { BlogArticleComponent } from './blog-article.component';

const routes: Routes = [
  {
    path: '',
    component: BlogArticleComponent,
    data: {
      meta: {
        title: 'blogArticle.title',
        description: 'blogArticle.text',
        override: true,
      },
    },
  },
];

export const BlogArticleRoutes = RouterModule.forChild(routes);
