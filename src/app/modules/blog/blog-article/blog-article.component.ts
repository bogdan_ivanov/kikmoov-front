import {Component, Inject, OnInit, PLATFORM_ID, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../../../services/blog.service';
import {DomSanitizer, makeStateKey, TransferState} from '@angular/platform-browser';
import {isPlatformServer} from '@angular/common';
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'app-blog-article',
  templateUrl: './blog-article.component.html',
  styleUrls: ['./blog-article.component.scss']
})
export class BlogArticleComponent implements OnInit {
  public loading = true;

  public article: any;
  public category;
  public related;
  public body;
    private isServer: boolean;

  constructor(
      private blogService: BlogService,
      private router: ActivatedRoute,
      private sanitized: DomSanitizer,
      private state: TransferState,
      @Inject(PLATFORM_ID) platformId,
      @Inject('ORIGIN_URL') public baseUrl: string,
      private readonly meta: MetaService
  ) {
      this.isServer = isPlatformServer(platformId);

          this.router.params.subscribe(params => {
              if ('articleSlug' in params) {
                  if (!this.article) {
                      this.blogService.getArticleBySlug(params.articleSlug).subscribe(
                          res => {
                              this.article = res['article'];
                              this.body = sanitized.bypassSecurityTrustHtml(this.article.body);
                              this.category = res['category'];
                              this.related = res['related'];
                              this.meta.setTitle(this.article.title, true);
                              this.meta.setTag('description', this.article.description);
                              this.loading = false;
                          },
                          err => {
                              console.log(err);
                              this.loading = false;
                          }
                      );
                  }
              }
          });
  }

  ngOnInit() {
      if (this.article) {
          this.meta.setTitle(this.article.title, true);
          this.meta.setTag('description', this.article.description);
      }
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
