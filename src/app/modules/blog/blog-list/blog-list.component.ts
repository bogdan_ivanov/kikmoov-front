import {Component, Inject, Input, OnChanges, OnInit} from '@angular/core';
import {BlogService} from '../../../services/blog.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit, OnChanges {
  @Input()
  slug;

  public articles = [];

  public blogList = [
    {
      title: 'Blog title',
      description: 'If you\'ve never run a half-marathon and you\'re currently running under 10 miles each week,' +
        ' expect to spend 12 to 14 weeks preparing for your half-marathon.'
    },
    {
      title: 'Blog title',
      description: 'You should plan on running at least three times a week in the beginning,' +
        ' and at least four times a week as your training progresses.'
    },
    {
      title: 'Blog title',
      description: 'Between training runs and official races, I have run a half marathon distance ...' +
        ' like a sailor, and couldn\'t run one mile without stopping for a much-needed break.'
    },
    {
      title: 'Blog title',
      description: 'If you\'ve never run a half-marathon and you\'re currently running under 10 miles each week, ' +
        'expect to spend 12 to 14 weeks preparing for your half-marathon.'
    },
    {
      title: 'Blog title',
      description: 'You should plan on running at least three times a week in the beginning, ' +
        'and at least four times a week as your training progresses.'
    },
    {
      title: 'Blog title',
      description: 'Between training runs and official races, I have run a half marathon distance ... ' +
        'like a sailor, and couldn\'t run one mile without stopping for a much-needed break.'
    },
    {
      title: 'Blog title',
      description: 'If you\'ve never run a half-marathon and you\'re currently running under 10 miles each week, ' +
        'expect to spend 12 to 14 weeks preparing for your half-marathon.'
    },
    {
      title: 'Blog title',
      description: 'You should plan on running at least three times a week in the beginning, ' +
        'and at least four times a week as your training progresses.'
    },
    {
      title: 'Blog title',
      description: 'Between training runs and official races, I have run a half marathon distance ... ' +
        'like a sailor, and couldn\'t run one mile without stopping for a much-needed break.'
    }
  ];

  constructor(
      private blogService: BlogService,
      private route: ActivatedRoute,
      @Inject('ORIGIN_URL') public baseUrl: string
  ) {
      if (this.route.firstChild) {
          this.route.firstChild.params.subscribe(params => {
              this.slug = params['categorySlug'];
              if (this.slug) {
                  this.getArticles();
              }
          });
      }
  }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

    public async ngOnChanges(changes) {
        this.getArticles();
    }

    getArticles() {
        if (this.slug) {
            this.blogService.getCategory(this.slug).subscribe(
                res => {
                    this.articles = res['articles'];
                },
                err => {
                    console.log(err);
                }
            );
        }
    }
}
