import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavCommunityComponent } from './nav-community.component';

describe('NavCommunityComponent', () => {
  let component: NavCommunityComponent;
  let fixture: ComponentFixture<NavCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
