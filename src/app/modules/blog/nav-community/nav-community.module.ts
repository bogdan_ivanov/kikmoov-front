import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NavCommunityComponent } from './nav-community.component';
import { NavCommunityRoutes } from './nav-community.routing';
import { BlogService } from '../../../services/blog.service';
import { BlogListComponent } from '../blog-list/blog-list.component';
import {TechCityComponent} from '../community-market/child-components/tech-city/tech-city.component';
import {WestEndComponent} from '../community-market/child-components/west-end/west-end.component';
import {WestLondonComponent} from '../community-market/child-components/west-london/west-london.component';
import {SouthLondonComponent} from '../community-market/child-components/south-london/south-london.component';
import {NorthLondonComponent} from '../community-market/child-components/north-london/north-london.component';
import {MidtownComponent} from '../community-market/child-components/midtown/midtown.component';
import {EastLondonComponent} from '../community-market/child-components/east-london/east-london.component';
import {CitySquareMileComponent} from '../community-market/child-components/city-square-mile/city-square-mile.component';
import {CommunityMarketComponent} from '../community-market/community-market.component';
import {MenuMarketComponent} from '../community-market/child-components/menu-market/menu-market.component';

@NgModule({
  imports: [
      CommonModule,
      NavCommunityRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      FormsModule,
      ReactiveFormsModule,
      MatFormFieldModule
  ],
  declarations: [
      NavCommunityComponent,
      BlogListComponent,
      TechCityComponent,
      CitySquareMileComponent,
      EastLondonComponent,
      MidtownComponent,
      NorthLondonComponent,
      SouthLondonComponent,
      WestLondonComponent,
      WestEndComponent,
      CommunityMarketComponent,
      MenuMarketComponent
  ],
  providers: [
      BlogService
  ]
})

export class NavCommunityModule {}
