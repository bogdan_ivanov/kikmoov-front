import {Component, Inject, OnInit} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { BlogService } from '../../../services/blog.service';
import { ActivatedRoute, ActivationEnd, Data, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

const CATEGORIES_KEY = makeStateKey('categories');

@Component({
  selector: 'app-nav-community',
  templateUrl: './nav-community.component.html',
  styleUrls: ['./nav-community.component.scss']
})
export class NavCommunityComponent implements OnInit {
  public categories: any;
  public articles = [];
  public apiUrl = environment.apiUrl;
  private parentData: BehaviorSubject<Data>;
  public slug: string;
  public categoryUrl: boolean;

  constructor(
      private blogService: BlogService,
      private route: ActivatedRoute,
      private router: Router,
      private http: HttpClient,
      private state: TransferState,
      @Inject('ORIGIN_URL') public baseUrl: string
  ) {
    this.parentData = <BehaviorSubject<Data>>this.route.data;
    this.router.events.subscribe(res => {
      if (res instanceof ActivationEnd) {
        if (!this.router.url.includes('community-market')) {
            setTimeout(() => {
                this.slug = res.snapshot.params['categorySlug'];
                this.categoryUrl = false;
            }, 0);
        } else {
            this.categoryUrl = true;
        }
      }
    });
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
      this.blogService.getCategories().subscribe(
          res => {
              this.categories = res['categories'];
              if (!this.categories.length) {
                  const categoryUrl = '/blog/community-market';
                  this.router.navigateByUrl(categoryUrl);
              }
              if (this.categories.length && this.router.url === '/blog') {
                  this.slug = this.categories[0].slug;
                  this.categoryUrl = false;
              }
              if (this.router.url.includes('community-market')) {
                  this.categoryUrl = true;
                  this.router.navigateByUrl(this.router.url);
              }
          },
          err => {
              console.log(err);
          }
      );
  }
}
