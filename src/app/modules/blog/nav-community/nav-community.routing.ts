import { Routes, RouterModule } from '@angular/router';
import { NavCommunityComponent } from './nav-community.component';
import {CommunityMarketComponent} from '../community-market/community-market.component';
import {TechCityComponent} from '../community-market/child-components/tech-city/tech-city.component';
import {CitySquareMileComponent} from '../community-market/child-components/city-square-mile/city-square-mile.component';
import {EastLondonComponent} from '../community-market/child-components/east-london/east-london.component';
import {MidtownComponent} from '../community-market/child-components/midtown/midtown.component';
import {NorthLondonComponent} from '../community-market/child-components/north-london/north-london.component';
import {SouthLondonComponent} from '../community-market/child-components/south-london/south-london.component';
import {WestLondonComponent} from '../community-market/child-components/west-london/west-london.component';
import {WestEndComponent} from '../community-market/child-components/west-end/west-end.component';
import {BlogListComponent} from '../blog-list/blog-list.component';

const routes: Routes = [
  {
    path: '',
    component: NavCommunityComponent,
    data: {
      meta: {
        title: 'blog.title',
        description: 'blog.text',
        override: true,
      },
    },
    children: [
      {
          path: 'category/:categorySlug',
          component: BlogListComponent,
          canActivate: [],
          data: {
              meta: {
                  title: 'blog.title',
                  description: 'blog.text',
                  override: true,
              },
          },
      },
      {
          path: 'community-market',
          component: CommunityMarketComponent,
          data: {
              title: 'Community market'
          },
          children: [
              {
                  path: '',
                  pathMatch: 'full',
                  redirectTo: 'tech-city'
              },
              {
                  path: 'tech-city',
                  component: TechCityComponent
              },
              {
                  path: 'city-square-mile',
                  component: CitySquareMileComponent
              },
              {
                  path: 'east-london',
                  component: EastLondonComponent
              },
              {
                  path: 'midtown',
                  component: MidtownComponent
              },
              {
                  path: 'north-london',
                  component: NorthLondonComponent
              },
              {
                  path: 'south-london',
                  component: SouthLondonComponent
              },
              {
                  path: 'west-london',
                  component: WestLondonComponent
              },
              {
                  path: 'west-end',
                  component: WestEndComponent
              }
          ]
      }
    ]
  },
];

export const NavCommunityRoutes = RouterModule.forChild(routes);
