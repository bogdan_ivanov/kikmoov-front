import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-west-london',
  templateUrl: './west-london.component.html'
})
export class WestLondonComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
