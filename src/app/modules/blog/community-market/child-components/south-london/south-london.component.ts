import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-south-london',
  templateUrl: './south-london.component.html'
})
export class SouthLondonComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
