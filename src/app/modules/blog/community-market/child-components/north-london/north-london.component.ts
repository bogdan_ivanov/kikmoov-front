import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-north-london',
  templateUrl: './north-london.component.html'
})
export class NorthLondonComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
