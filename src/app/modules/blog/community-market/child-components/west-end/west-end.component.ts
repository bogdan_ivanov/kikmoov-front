import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-west-end',
  templateUrl: './west-end.component.html'
})
export class WestEndComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

}
