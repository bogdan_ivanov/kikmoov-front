import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-midtown',
  templateUrl: './midtown.component.html'
})
export class MidtownComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
