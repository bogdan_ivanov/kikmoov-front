import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-tech-city',
  templateUrl: './tech-city.component.html'
})
export class TechCityComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
