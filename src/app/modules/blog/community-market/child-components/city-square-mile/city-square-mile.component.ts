import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-city-square-mile',
  templateUrl: './city-square-mile.component.html'
})
export class CitySquareMileComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

}
