import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-east-london',
  templateUrl: './east-london.component.html'
})
export class EastLondonComponent implements OnInit {

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) { }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
