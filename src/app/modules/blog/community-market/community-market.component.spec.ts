import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityMarketComponent } from './community-market.component';

describe('CommunityMarketComponent', () => {
  let component: CommunityMarketComponent;
  let fixture: ComponentFixture<CommunityMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
