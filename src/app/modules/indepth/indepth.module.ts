import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { IndepthRoutes } from './indepth.routing';
import { IndepthComponent } from './indepth.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {
    DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ShareButtons, ShareButtonsModule } from '@ngx-share/buttons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiMapModule } from '@ngui/map';
import { LocationService } from '../../services/location.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { environment } from '../../../environments/environment';
import { BookingProcessComponent } from './booking-process/booking-process.component';
import { BookingService } from '../../services/booking.service';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';
import { PaymentService } from '../../services/payment.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { LightboxModule } from 'ngx-lightbox';

const CustomDateFormats = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
  imports: [
      CommonModule,
      IndepthRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      SlickCarouselModule,
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatProgressSpinnerModule,
      MatExpansionModule,
      MatNativeDateModule,
      MatDatepickerModule,
      MatSelectModule,
      MatSnackBarModule,
      NgxSmartModalModule,
      ReactiveFormsModule,
      FormsModule,
      NguiMapModule.forRoot({
          apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.googleMapsKey
      }),
      ShareButtonsModule.withConfig({
          include: ['facebook', 'email', 'linkedin', 'twitter'],
          theme: 'circles-light',
          size: -3,
          prop: {
              twitter: {
                  icon: ['fab', 'twitter']
              },
              facebook: {
                  icon: ['fab', 'facebook-f']
              },
              linkedin: {
                  icon: ['fab', 'linkedin-in']
              },
              email: {
                  icon: ['far', 'envelope']
              }
          }
      }),
      FontAwesomeModule,
      LightboxModule
  ],
  declarations: [
      IndepthComponent,
      BookingProcessComponent
  ],
    providers: [
        LocationService,
        ShareButtons,
        BookingService,
        MessageService,
        ErrorsService,
        PaymentService,
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE]
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: CustomDateFormats
        },
    ]
})
export class IndepthModule {}
