import { ErrorsService } from './../../../services/errors.service';
import { MessageService } from './../../../services/message.service';
import { FormBuilder, Validators } from '@angular/forms';
import {Component, OnInit, Input, Output, EventEmitter, PLATFORM_ID, Inject} from '@angular/core';
import { BookingService } from '../../../services/booking.service';
import { PaymentService } from '../../../services/payment.service';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { Moment } from 'moment-timezone';
import {isPlatformBrowser} from '@angular/common';
import {UniversalStorage} from '@shared/storage/universal.storage';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-booking-process',
  templateUrl: './booking-process.component.html',
  styleUrls: ['./booking-process.component.scss'],
  exportAs: 'appBookingProcess'
})
export class BookingProcessComponent implements OnInit {
  @Input()
  workSpace$: Observable<any>;

  @Input()
  location;

  @Input()
  type;

  @Input()
  modal;

  @Output()
  close = new EventEmitter<void>();

  public done = false;

  public paymentType;

  public workSpace;

  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office'
  };

  public times = [];

  public bookStep = 'book';

  public availableFrom;

  public currentDate = new Date();

  private phonePattern = '^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$';
  private defaultRequestDate = new Date().setHours(0, 0, 0);

  private isBrowser;

  public isSending = false;

  public listOfDesks;

  public inviteAttendeeForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
  });

  public cardForm = this.fb.group({
    cardHolderName: ['', [Validators.required]],
    billingAddress: ['', [Validators.required]],
    city: ['', [Validators.required]]
  });
  public bookingForm = this.fb.group({
    startDate: [moment(), [Validators.required]],
    startTime: ['', [Validators.required]],
    time: ['', [Validators.required]],
    endTime: [''],
    phone: ['', [Validators.required, Validators.pattern(new RegExp(this.phonePattern, 'gm'))]],
    email: [this.universalStorage.getItem('email'), [Validators.required, Validators.email]],
    name: [''],
    card: [
      this.cardForm,
      this.bookStep === 'book' ? [Validators.required] : []
    ]
  });
  public bookingRequestForm = this.fb.group({
    startDate: [moment(this.defaultRequestDate), [Validators.required]],
    startTime: [''],
    phone: ['', [Validators.required, Validators.pattern(new RegExp(this.phonePattern, 'gm'))]],
    email: [this.universalStorage.getItem('email'), [Validators.required, Validators.email]],
    name: ['', [Validators.required]],
    information: ['', [Validators.required]],
    duration: ['not-sure', [Validators.required]],
    tenantType: ['freelancer', [Validators.required]],
    units: [null, [Validators.required]],
  });
  public stripeCard = {};
  public elements;
  public firstChecked = null;
  public lastChecked = null;
  public checkedCount = 0;
  public $checkboxes: NodeListOf<any> | any[];

  public currentBookingForm;

  public date: Date;

  public eventsList : any;

  public inviteAttendeeEmails = [];

  public bookingId = 0;

  public VATPercentage = 0.2;

  constructor(
    private fb: FormBuilder,
    private bookingService: BookingService,
    private messageService: MessageService,
    private errorService: ErrorsService,
    private paymentService: PaymentService,
    public ngxSmartModalService: NgxSmartModalService,
    @Inject('ORIGIN_URL') public baseUrl: string,
    @Inject(PLATFORM_ID) private platformId,
    private universalStorage: UniversalStorage
  ) {
      this.isBrowser = isPlatformBrowser(platformId);

      this.inviteAttendeeEmails = [];
  }

  ngOnInit() {
    if (this.workSpace$) {
      this.workSpace$.subscribe((workSpace) => {
          this.listOfDesks = Array.from({length: workSpace.quantity}, (x, i) => i+1);
        if (
          (workSpace && workSpace.type === 'private-office') ||
          (workSpace &&
            (workSpace.type === 'desk' &&
              workSpace.deskType !== 'hourly_hot_desk' && workSpace.deskType !== 'daily_hot_desk'))
        ) {
            this.paymentType = 'monthly';
            this.currentBookingForm = this.bookingRequestForm;

            if (workSpace.type === 'private-office') {
                this.bookingRequestForm.get('units').clearValidators();
                this.bookingRequestForm.get('units').updateValueAndValidity();
            }
        } else if ((workSpace.type === 'desk' &&
            workSpace.deskType === 'daily_hot_desk') || (workSpace.type === 'meeting-room' && workSpace.deskType === 'daily_meeting_room')) {
            this.paymentType = 'daily';
            this.currentBookingForm = this.bookingForm;
            this.bookingForm.get('time').clearValidators();
            this.bookingForm.get('time').updateValueAndValidity();
        } else {
          this.paymentType = 'hourly';
          this.currentBookingForm = this.bookingForm;
        }
        if (workSpace && workSpace.type === 'meeting-room') {
          this.bookingForm.get('name').setValidators([Validators.required]);
        }
        if (workSpace.hasOwnProperty('availableFrom')) {
          this.availableFrom = moment.unix(workSpace.availableFrom).format('MM/DD/YYYY');
        }
        if (workSpace.events) {
            this.eventsList = workSpace.events;
        }
        if (workSpace.opensFrom && workSpace.closesAt) {
          const open = Number.parseInt(workSpace.opensFrom);
          const close = Number.parseInt(workSpace.closesAt);
          const times = [];
          for (let i = open; i <= close; i++) {
            times.push(`${i < 10 ? '0' + i : i}:00`)
          }
          this.times = times;
        } else {
          this.times = [
            '09:00',
            '10:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00'
          ];
        }
        this.workSpace = workSpace;
      });
    }
    if (this.modal) {
      this.modal.onClose.subscribe(() => this.closeModal(false));
    }
  }

  filterDate = (d: Date): boolean => {
      const convertDate = d.toLocaleString('en-us', { year: 'numeric', month: '2-digit', day: 'numeric' });
      // Using a JS Object as a lookup table of valid dates
      // Undefined will be falsy.
      // console.log(convertDate);
      return !this.eventsList[convertDate];
  };

  submitViewing(form) {
    const value = form.value;
    if (form.invalid) {
      return this.showErrors(form);
    }
    // because of it https://kikmoov.atlassian.net/browse/KIK-241
    /*if (form.get('startDate') && form.get('startDate').value instanceof moment && this.workSpace.hasOwnProperty('availableFrom')) {
      const date = form.get('startDate').value.toDate();
      const tzOffset = Math.abs(date.getTimezoneOffset());
      let startDate = date.getTime();
      startDate = (startDate + (tzOffset * 60000));
      if (startDate < (this.workSpace.availableFrom * 1000)) {
        return this.showErrors(form);
      }
    }*/
    form.disable();
    this.bookingService.addViewing(this.workSpace.id, value).subscribe(
      res => {
        this.done = true;
        this.closeModal();
        this.messageService.setMessage(res['message']);
        this.messageService.showMessage();
        this.messageService.clearMessage();
        form.enable();
      },
      err => {
        if ('message' in err) {
          this.errorService.setErrors(err);
          this.errorService.showError();
          this.errorService.clearErrors();
          form.enable();
        } else {
          form.enable();
          this.errorService.setFieldErrors(err, form);
        }
      }
    );
  }

  submitBooking(form) {
    this.isSending = true;
    if (this.isBrowser) {
        const card = form.get('card').value;
        if (card.invalid) {
            return this.showErrors(card);
        }
        const value = form.value;
        form.disable();
        this.paymentService.stripe.createToken(
            this.stripeCard['cardNumberElement']
            ,
            {
                name: card.get('cardHolderName').value,
                address_city: card.get('city').value,
                address_line1: card.get('billingAddress').value
            }
        ).then((response) => {
            if ('token' in response) {
                const data = {...value, address: response.token.card.address_line1, city: response.token.card.address_city, postcode: response.token.card.address_zip};
                this.bookingService.addBooking(this.workSpace.id, data, response['token'].id).subscribe(
                    res => {
                        this.isSending = false;
                        this.done = true;
                        this.firstChecked = null;
                        this.$checkboxes = undefined;
                        this.bookingId = res['bookingId'];
                        form.enable();
                    },
                    err => {
                        if ('errors' in err) {
                            this.proceedBack();
                            form.enable();
                            this.errorService.setFieldErrors(err, form);
                        } else {
                            form.enable();
                            this.errorService.setErrors(err);
                            this.errorService.showError();
                            this.errorService.clearErrors();
                        }
                    }
                );
            } else {
                this.handleCardChange(response);
            }
        });
    }
  }

  submitBookingRequest(form) {
    if (form.invalid) {
      return this.showErrors(form);
    }

    // because of it https://kikmoov.atlassian.net/browse/KIK-241
    /*if (form.get('startDate') && form.get('startDate').value instanceof Date && this.workSpace.hasOwnProperty('availableFrom')) {
      const tzOffset = Math.abs(form.get('startDate').value.getTimezoneOffset());
      let startDate = form.get('startDate').value.getTime();
      startDate = (startDate + (tzOffset * 60000));
      if (startDate < (this.workSpace.availableFrom * 1000)) {
        return this.showErrors(form);
      }
    }*/
    const value = form.value;
    form.disable();
    this.bookingService.addBookingRequest(this.workSpace.id, value).subscribe(
      res => {
        this.done = true;
        form.enable();
      },
      err => {
        if (err.errors && 'errors' in err && 'attribute' in err.errors[0]) {
          this.bookingForm.get(err.errors[0].attribute).setErrors({[err.errors[0].attribute]: err.errors[0].details});
          form.enable();
        } else {
          form.enable();
          this.errorService.setErrors(err);
          this.errorService.showError();
          this.errorService.clearErrors();
        }
      }
    );
  }

  onTimeCheck(startTime?, endTime?) {
        //clear timepicker
      this.$checkboxes = document.querySelectorAll('.choose-time-block input');
      this.$checkboxes = Array.from(this.$checkboxes);
      this.$checkboxes.forEach(item => item.checked = false);
      this.firstChecked = null;
      this.lastChecked = null;
      if (this.bookingForm.get('time').errors) {
          delete this.bookingForm.get('time').errors['notAvailable'];
      }
      if (this.paymentType === 'daily') {
          this.$checkboxes = document.querySelectorAll('.choose-time-block input');
          this.$checkboxes = Array.from(this.$checkboxes);
          this.$checkboxes.forEach(item => item.checked = false);
          const comparedData = moment(this.bookingForm.get('startDate').value).toDate();
          if (
              comparedData.getDate() + comparedData.getMonth() + comparedData.getFullYear() !== (new Date()).getDate() + (new Date()).getMonth() + (new Date()).getFullYear()
          ) {
          this.$checkboxes = document.querySelectorAll('.choose-time-block input');
          this.$checkboxes = Array.from(this.$checkboxes);
          this.$checkboxes.forEach(item => item.checked = true);
          const start =  this.$checkboxes.findIndex(item => item.id === this.$checkboxes[0].id);
          const endid = this.$checkboxes.slice(-1)[0].id;
          const end =  this.$checkboxes.findIndex(item => item.id === endid);

          if (start >= 0 && end >= 0) {
              this.firstChecked = this.$checkboxes[start];
              this.lastChecked = this.$checkboxes[end];
          }

          this.checkedCount = 2;
          }
      }
    if (this.firstChecked && this.lastChecked) {
      startTime = this.firstChecked;
      endTime = this.lastChecked;
    } else if (this.bookingForm.get('startDate').invalid || !startTime || !endTime) {
      return {startTime: null, endTime: null};
    }
    const startDate = this.convertToUtc(this.bookingForm.get('startDate').value).toString();
    const endDate = startDate;
    let startTimestamp: any = startDate.replace(/\d+:\d+(:\d+)/g, startTime.value);
    startTimestamp = +(new Date(startTimestamp).getTime() / 1000).toFixed(0);
    let endTimestamp: any = endDate.replace(/\d+:\d+(:\d+)/g, endTime.value);
    endTimestamp = +(new Date(endTimestamp).getTime() / 1000).toFixed(0);
    this.bookingForm.get('startTime').setValue(startTimestamp);
    this.bookingForm.get('endTime').setValue(endTimestamp);
    if (this.type !== 'viewing') {
      this.bookingService.checkBookingAvailable(this.workSpace.id, {
        startTime: startTimestamp,
        endTime: endTimestamp
      }).subscribe(
        res => {
          if (!res['available']) {
            this.bookingForm.get('time')
              .setErrors({notAvailable: 'Please pick up another time'});
          } else {
            this.bookingForm.get('time').updateValueAndValidity()
          }
        },
        err => console.log(err)
      );
    }
  }

  checkTimeRange(event) {
      if (this.paymentType !== 'daily') {
          if (this.isTimePassed(event.target.value)) {
              event.target.checked = false;
              return;
          }
          this.checkedCount = 0;
          if (!this.$checkboxes) {
              this.$checkboxes = document.querySelectorAll('.choose-time-block input');
          }

          this.$checkboxes = Array.from(this.$checkboxes);
          const input = event.target;

          if (this.firstChecked === null) {
              this.firstChecked = input;
              this.checkedCount = 1;
              return;
          }

          if (this.firstChecked === input) {
              this.firstChecked.checked = !this.firstChecked.checked;
              this.firstChecked = null;
              this.checkedCount = 0;
              this.$checkboxes.forEach(item => item.checked = false);
              return;
          }

          const start = this.$checkboxes.findIndex(item => item.id === this.firstChecked.id);
          const end = this.$checkboxes.findIndex(item => item.id === input.id);
          if (start >= 0 && end >= 0) {
              this.firstChecked = this.$checkboxes[start];
              this.lastChecked = this.$checkboxes[end];
              this.onTimeCheck(this.$checkboxes[start], this.$checkboxes[end]);
          }

          const range = this.$checkboxes
              .slice(Math.min(start, end), Math.max(start, end) + 1);

          this.checkedCount = 0;

          this.$checkboxes.forEach(item => {
              const rItem = range.find($item => ($item.id === item.id));
              item.checked = rItem !== undefined;

              if (item.checked) {
                  this.checkedCount++;
              }
          });
          this.firstChecked = range.shift();
      } else {
          this.onTimeCheck();
          /*if (this.isTimePassed(event.target.value)) {
              event.target.checked = false;
              return;
          }
          this.checkedCount = 0;
          if (!this.$checkboxes) {
              this.$checkboxes = document.querySelectorAll('.choose-time-block input');
          }

          this.$checkboxes = Array.from(this.$checkboxes);
          const input = event.target;

          if (this.firstChecked === null) {
              this.firstChecked = input;
              this.checkedCount = 1;
              return;
          }

          if (this.firstChecked === input) {
              this.firstChecked.checked = !this.firstChecked.checked;
              this.firstChecked = null;
              this.checkedCount = 0;
              this.$checkboxes.forEach(item => item.checked = false);
              return;
          }
          const start =  this.$checkboxes.findIndex(item => item.id === this.$checkboxes[0].id);
          const endid = this.$checkboxes.slice(-1)[0].id;
          const end =  this.$checkboxes.findIndex(item => item.id === endid);

          if (start >= 0 && end >= 0) {
              this.firstChecked = this.$checkboxes[start];
              this.lastChecked = this.$checkboxes[end];
              this.onTimeCheck(this.$checkboxes[start], this.$checkboxes[end]);
          }

          const range = this.$checkboxes
              .slice(Math.min(start, end), Math.max(start, end) + 1);

          this.checkedCount = 0;

          this.$checkboxes.forEach(item => {
              const rItem = range.find($item => ($item.id === item.id));
              item.checked = rItem !== undefined;

              this.checkedCount = 2;
          });
          this.firstChecked = range.shift();*/
      }
  }

  isTimePassed(time?) {
    if (!time) {
      return false;
    }
    const now: any = this.convertToUtc(null, Date.now());
    const choosedDate = new Date(this.bookingForm.get('startDate').value.toString());
      if (this.paymentType !== 'daily') {
          let convertedChoosedDate = choosedDate.getFullYear() + '-' + ("0" + (choosedDate.getMonth() + 1)).slice(-2) + '-' + ("0" + choosedDate.getDate()).slice(-2);

          for (let i = 0; i < this.eventsList.length; i++) {
              let event = this.eventsList[i];

              const start = event['start'].date.split(" ");
              const end = event['end'].date.split(" ");
              const startEventDate = start[0];
              const endEventDate = end[0];

              if (startEventDate === convertedChoosedDate && endEventDate === convertedChoosedDate) {
                  const timesStart = start[1].split(':');
                  const startEventTime = timesStart[0] + ':' + timesStart[1];
                  const startEventTimeHours = timesStart[0];

                  const timesEnd = end[1].split(':');
                  const endEventTime = timesEnd[0] + ':' + timesEnd[1];

                  if(endEventTime == time && time == this.times.slice(-1)[0]) {
                    return true;
                  }

                  if ((startEventTime <= time || startEventTimeHours <= time) && endEventTime > time) {
                      return true;
                  }
              }

              if (startEventDate === convertedChoosedDate && convertedChoosedDate < endEventDate) {
                  const timesStart = start[1].split(':');
                  const startEventTime = timesStart[0] + ':' + timesStart[1];
                  const startEventTimeHours = timesStart[0];

                  if (startEventTime <= time || startEventTimeHours <= time) {
                      return true;
                  }
              }

              if (endEventDate === convertedChoosedDate && convertedChoosedDate > startEventDate) {
                  const timesEnd = end[1].split(':');
                  const endEventTime = timesEnd[0] + ':' + timesEnd[1];

                  if (endEventTime > time) {
                      return true;
                  }
              }
          }
          const currentHours = new Date(now).getHours();
          if (this.bookingForm.get('startDate').invalid) {
              const timeSplitted = time.split(':');
              return (+timeSplitted[0] < currentHours);
          }
          const date = this.convertToUtc(this.bookingForm.get('startDate').value).toString();
          let timestamp: any = date.replace(/\d+:\d+(:\d+)/g, time);
          // timestamp = new Date(timestamp).getTime();
          timestamp = new Date(timestamp).setHours(new Date(timestamp).getHours() - 1);
          return (timestamp < (now));
      } else {
          const date = this.convertToUtc(this.bookingForm.get('startDate').value).toString();
          let timestamp: any = date.replace(/\d+:\d+(:\d+)/g, time);
          timestamp = new Date(timestamp).getTime();
          const dateChoosed = new Date(timestamp);
          if (
              dateChoosed.getDate() === (new Date()).getDate() &&
              dateChoosed.getMonth() === (new Date()).getMonth() &&
              dateChoosed.getFullYear() === (new Date()).getFullYear()
          ) {
              return true;
          }
          return (timestamp <= (now));
      }
  }

  checkViewingTime(event, time) {
    if (this.isViewingTimePassed(time)) {
      event.target.checked = false;
      return;
    }
    this.onViewingTimeCheck(time);
  }

  onViewingTimeCheck(time) {
    if (this.bookingForm.get('time').value) {
      time = this.bookingForm.get('time').value
    } else if (this.bookingForm.get('startDate').invalid || !time) {
      return {startTime: null, endTime: null};
    }
    const startDate = this.bookingForm.get('startDate').value.toString();
    let startTimestamp = startDate.replace(/\d+:\d+(:\d+)/g, time);
    startTimestamp = +(new Date(startTimestamp).getTime() / 1000).toFixed(0);
    this.bookingForm.get('startTime').setValue(startTimestamp);
  }

  isViewingTimePassed(time?) {
    if (!time) {
      return false;
    }
    const now: any = this.convertToUtc(null, Date.now());
    const currentHours = new Date(now).getHours();
    if (this.bookingForm.get('startDate').invalid) {
      const timeSplitted = time.split(':');
      return (+timeSplitted[0] < currentHours);
    }
    const date = this.convertToUtc(this.bookingForm.get('startDate').value).toString();
    let timestamp: any = date.replace(/\d+:\d+(:\d+)/g, time);
    timestamp = new Date(timestamp).setHours(new Date(timestamp).getHours() - 1);
    // timestamp = new Date(timestamp).getTime();
    return (timestamp < (now));
  }

  proceedBook(form) {
    if (form.invalid) {
      return this.showErrors(form);
    }
    this.bookStep = 'card';
    setTimeout(() => {
      this.drawForm();
    }, 10);
  }

  proceedBack() {
    this.bookStep = 'book';
    this.$checkboxes = undefined;
    const startTime = new Date(this.bookingForm.get('startTime').value * 1000);
    const endTime = new Date(this.bookingForm.get('endTime').value * 1000);
    const startHours = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) +
      ':' + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
    const endHours = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) +
      ':' + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());
    setTimeout(() => {
      const startInput = document.querySelector('.choose-time-block input[value="' + startHours + '"]');
      const endInput = document.querySelector('.choose-time-block input[value="' + endHours + '"]');
      this.firstChecked = startInput;
      this.checkTimeRange({target: endInput});
    }, 100);
    this.bookingForm.get('phone').updateValueAndValidity();
  }

  drawForm() {
    if (this.isBrowser) {
        this.elements = this.paymentService.stripe.elements({
            fonts: [
                {cssSrc: 'https://fonts.googleapis.com/css?family=Montserrat'}
            ]
        });
        const style = {
            base: {
                fontSize: '14px',
                lineHeight: '20px',
                color: '#4a4a4a',
                fontFamily: '\'Montserrat\', sans-serif',
                '::placeholder': {
                    fontSize: '14px',
                    lineHeight: '16px',
                    color: '#9b9b9b',
                    fontFamily: '\'Montserrat\', sans-serif'
                }
            }
        };

        this.stripeCard['cardNumberElement'] = this.elements.create('cardNumber', {
            style: style,
            placeholder: 'Card number',
        });
        this.stripeCard['cardNumberElement'].mount('#card-number-element');
        this.stripeCard['cardNumberElement'].addEventListener('change', this.handleCardChange.bind(this));
        this.stripeCard['cardExpiryElement'] = this.elements.create('cardExpiry', {
            style: style,
            placeholder: 'Expiry date',
        });
        this.stripeCard['cardExpiryElement'].mount('#card-expiry-element');
        this.stripeCard['cardExpiryElement'].addEventListener('change', this.handleCardChange.bind(this));
        this.stripeCard['cardCvcElement'] = this.elements.create('cardCvc', {
            style: style,
            placeholder: 'CCV',
        });
        this.stripeCard['cardCvcElement'].mount('#card-cvc-element');
        this.stripeCard['cardCvcElement'].addEventListener('change', this.handleCardChange.bind(this));
        this.stripeCard['postcodeElement'] = this.elements.create('postalCode', {
            style: style,
            placeholder: 'Postcode',
        });
        this.stripeCard['postcodeElement'].mount('#card-postcode-element');
        this.stripeCard['postcodeElement'].addEventListener('change', this.handleCardChange.bind(this));
    }
  }

  handleCardChange(event) {
    this.cardForm.updateValueAndValidity();
    const errorType = event['elementType'] || event.error.code;
    if (event.error) {
      this.cardForm.setErrors({
        [errorType]: event['error'].message
      });
    } else if (!event.error && this.cardForm.hasError(errorType)) {
      delete this.cardForm.errors[errorType];
    }
  }

  closeModal(emit?: boolean) {
    emit = emit !== false;
    if (emit) {
      this.close.emit();
    }
    this.bookingForm.reset();
    this.cardForm.reset();
    this.bookingForm.get('startDate').setValue(new Date());
    this.bookingForm.get('email').setValue(this.universalStorage.getItem('email'));
    this.bookingForm.get('card').setValue(this.cardForm);
    this.bookingRequestForm.reset({
      startDate: new Date(),
      duration: 'not-sure',
      tenantType: 'freelancer',
      email: this.universalStorage.getItem('email')
    });
    this.done = false;
    this.firstChecked = null;
    this.$checkboxes = undefined;
    this.checkedCount = 0;
    this.bookStep = 'book';
  }

  formatDate(date) {
    if (date instanceof Date) {
      const options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
      // Monday 6 October 2018
      return date.toLocaleDateString('en-GB', options);
    } else if (date instanceof moment) {
      return moment(date).format('dddd DD MMMM YYYY');
    }
    return null;
  }

  formatTime(time: number) {
    const dateTime = new Date((time * 1000));
    const hours = +dateTime.getHours() < 10 ? `0${dateTime.getHours()}` : dateTime.getHours();
    const minutes = +dateTime.getMinutes() < 10 ? `0${dateTime.getMinutes()}` : dateTime.getMinutes();
    return `${hours}:${minutes}`;
  }

  showErrors(form) {
    Object.keys(form.controls).forEach(controlName => {
      form.get(controlName).markAsTouched();
    });
    // because of it https://kikmoov.atlassian.net/browse/KIK-241
    /*if (form.get('startDate') && form.get('startDate').value instanceof Date && this.workSpace.hasOwnProperty('availableFrom')) {
      if (form.get('startDate').value.getTime() < (this.workSpace.availableFrom * 1000)) {
        form.get('startDate').setErrors({'startDate': true});
      }
    }
    if (form.get('startDate') && form.get('startDate').value instanceof moment && this.workSpace.hasOwnProperty('availableFrom')) {
      const date = form.get('startDate').value.toDate();
      if (date.getTime() < (this.workSpace.availableFrom * 1000)) {
        form.get('startDate').setErrors({'startDate': true});
      }
    }*/
    return;
  }

  convertToUtc(date, time?: number): Date | number {
    if (date instanceof moment) {
        // date = date.toDate();
        date = moment(date).toDate();
    }
    if (time) {
      // const tzOffset = Math.abs(new Date().getTimezoneOffset());
      const startDate = time;
      // startDate = (startDate - (tzOffset * 60000));
      return new Date(startDate).getTime();
    } else {
      date.setHours(23, 59);
      // const tzOffset = Math.abs(date.getTimezoneOffset());
      const startDate = date.getTime();
      // startDate = (startDate - (tzOffset * 60000));
      return new Date(startDate);
    }
  }

  inviteAttendeeModal() {
    this.ngxSmartModalService.getModal('inviteAttendee').open();
  }

  inviteAttendeeModalClose() {
    this.ngxSmartModalService.getModal('inviteAttendee').close();
  }

  addEmailIntoInviteAttendeeEmails(email) {
    if (this.inviteAttendeeForm.valid) {
        const inviteAttendeeEmail = email.value;
        this.inviteAttendeeEmails.push(inviteAttendeeEmail);
        this.inviteAttendeeForm.get('email').reset('');
    }
  }

  removeEmailIntoInviteAttendeeEmails(inviteAttendeeEmail) {
    const index = this.inviteAttendeeEmails.indexOf(inviteAttendeeEmail);
    this.inviteAttendeeEmails.splice(index, 1);
  }

  submitAttendees() {
    if (this.inviteAttendeeEmails.length === 0) {
      this.messageService.setMessage('Please, add attendee emails into form');
      this.messageService.showMessage();
      this.messageService.clearMessage();
      return;
    }

    if (this.workSpace.id && this.bookingId) {
        this.bookingService.inviteAttendeeBooking(this.bookingId, this.inviteAttendeeEmails).subscribe(
            res => {
                this.inviteAttendeeModalClose();
                this.messageService.setMessage('Attendees were added successfully');
                this.messageService.showMessage();
                this.messageService.clearMessage();
            },
            err => {
                if ('message' in err) {
                    this.errorService.setErrors(err);
                    this.errorService.showError();
                    this.errorService.clearErrors();
                } else {
                    this.errorService.setErrors({message: 'These attendees were added before'});
                    this.errorService.showError();
                    this.errorService.clearErrors();
                }
            }
        );
    }
  }

  getExportBooking() {
    if (this.workSpace.id && this.bookingId) {
      return this.bookingService.addToCalendar(this.bookingId);
    }
    return '';
  }
}
