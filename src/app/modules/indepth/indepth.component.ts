import { ActivatedRoute, Router } from '@angular/router';
import {Component, Inject, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { ProfileService } from '../../services/profile.service';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ErrorsService } from '../../services/errors.service';
import { ShareButtons } from '@ngx-share/buttons';
import { LocationService } from '../../services/location.service';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { MetaService } from '@ngx-meta/core';
import { Lightbox, LightboxConfig } from 'ngx-lightbox';

@Component({
  selector: 'app-indepth',
  templateUrl: './indepth.component.html',
  styleUrls: ['./indepth.component.scss'],
})
export class IndepthComponent implements OnInit {
  @ViewChild('appBookingProcess')
  appBookingProcess;


  public slickCarousel;

  public workSpace;
  public indepthSlides = [];
  public indepthSliderConfig = {
    slidesToShow: 1,
    centerPadding: '350px',
    dots: true,
    arrows: true,
    autoplaySpeed: 4000,
    centerMode: true,
    speed: 400,
    useTransform: false,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            centerMode: false,
            centerPadding: false,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                centerMode: false,
                slidesToScroll: 1,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                centerMode: false,
                slidesToScroll: 1
            }
        }
    ]
  };

  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting space',
    'private-office': 'Private Office',
  };
  public deskTypesNames = {
    hourly_hot_desk: 'Desk',
    monthly_hot_desk: 'Hot Desk',
    monthly_fixed_desk: 'Fixed Desk'
  };
  public hourlyDeskTypesNames = {
    hourly_hot_desk: 'Desk',
    daily_hot_desk: 'Daily Desk'
  };
  public monthlyDeskTypesNames = {
    monthly_hot_desk: 'Hot Desk',
    monthly_fixed_desk: 'Fixed Desk'
  };
  public workSpaceType;
  public bookingType;
  public paymentType;
  public currentWorkspace;
  public currentWorkspace$: Observable<any>;
  public workSpaceObserver: Observer<any>;

  private map;

  public fullUrl = window.location.href;

  public isBrowser;

  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    public share: ShareButtons,
    private locationService: LocationService,
    private profileService: ProfileService,
    private userService: UserService,
    private messageService: MessageService,
    private errorService: ErrorsService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    @Inject(DOCUMENT) private document: any,
    @Inject('ORIGIN_URL') public baseUrl: string,
    @Inject(PLATFORM_ID) private platformId,
    private readonly meta: MetaService,
    private _lightbox: Lightbox,
    private _lighboxConfig: LightboxConfig
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    this._lighboxConfig.wrapAround = true;
    this.currentWorkspace$ = new Observable<any>((observer) => {
      this.workSpaceObserver = observer;
      this.changeTypes();
    });
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if ('id' in params) {
        let workSpaceId = +params['id'];
        this.route.queryParams.subscribe(queryParams => {
          if (queryParams['relatedId']) {
            workSpaceId = +queryParams['relatedId'];
          }
          this.locationService.getOffice(workSpaceId).subscribe(
            (res: Array<any>) => {
              this.currentWorkspace = res['workspace'];
              this.workSpace = res;
              this.meta.setTitle(this.workSpace['location'].name, true);
              this.meta.setTag('description', this.workSpace['location'].description);
              if (this.workSpace['location'].videoLink) {
                const videoLink = this.parseVideoLink(this.workSpace['location'].videoLink);
                this.workSpace['location'].videoLink =
                  this.sanitizer.bypassSecurityTrustResourceUrl(videoLink);
              }
              this.indepthSliderConfig.dots = this.currentWorkspace.images.length > 1;
              this.indepthSlides = this.currentWorkspace.images;
              this.handleSlidesToLightbox();
                const inArray = res['related'].find(function (el) {
                    return el.id === workSpaceId;
                });
              if (res['related'].length && this.currentWorkspace.type !== 'desk' && !inArray) {
                res['related'].unshift(this.currentWorkspace);
              }

              if (this.currentWorkspace.type === 'desk' && !inArray) {
                res['related'].unshift(this.currentWorkspace);
              }
              this.initializeMap(res);
              this.changeTypes();
            },
            err => {
              console.log(err);
              this.errorService.setErrors(err);
              this.errorService.showError();
              setTimeout(() => {
                this.router.navigate(['/findaspace']);
              }, 5000);
            }
          );
        });
      }
    });
  }

  open(index: number): void {
    this._lightbox.open(this.indepthSlides, index);
  }

  close(): void {
    this._lightbox.close();
  }

  initializeMap(workSpace) {
    if (this.isBrowser) {
        if (!workSpace) {
            return;
        }
        const mapRef = this.document.getElementById('map-canvas');
        const latLng = new google.maps.LatLng(+workSpace['location'].latitude, +workSpace['location'].longitude);
        const marker = new google.maps.Marker({icon: 'assets/img/location-marker.png', position: latLng});
        const mapOptions = {
            zoom: 14,
            center: latLng,
            disableDefaultUI: true
        };
        if (!this.map && mapRef) {
            this.map = new google.maps.Map(mapRef, mapOptions);
            marker.setMap(this.map);
        } else if (workSpace) {
            setTimeout(() => {
                this.initializeMap(workSpace);
            }, 500);
            return;
        }
    }
  }

  showBookingProcess(type) {
    if (!this.checkIfUserSignedIn()) {
      return;
    }
    this.bookingType = type;
    setTimeout(() => {
      this.ngxSmartModalService.getModal('bookingProcess').open();
    }, 10);
  }

  showShareModal() {
    this.ngxSmartModalService.getModal('shareModal').open();
  }

  handleModalClosing() {
    this.ngxSmartModalService.getModal('bookingProcess').close();
  }

  changeTypes(workSpace?) {
    workSpace = workSpace || this.currentWorkspace;
    if (
      workSpace.type === 'private-office' ||
      (workSpace.type === 'desk' && workSpace.deskType !== 'hourly_hot_desk' && workSpace.deskType !== 'daily_hot_desk')) {
      this.paymentType = 'monthly';
    } else if (workSpace.deskType === 'daily_hot_desk' || workSpace.deskType === 'daily_meeting_room') {
        this.paymentType = 'daily';
    } else {
      this.paymentType = 'hourly';
    }
    if (this.slickCarousel) {
      this.slickCarousel
        .el
        .nativeElement
        .slick
        .slickSetOption({dots: (workSpace.images.length > 1)});
    }
    this.indepthSlides = workSpace.images;
    this.handleSlidesToLightbox();
    this.workSpaceType = workSpace.type;
    if (this.workSpaceObserver) {
      this.workSpaceObserver.next(workSpace);
    }
  }

  handleSlidesToLightbox() {
    this.indepthSlides.forEach(function (slide) {
      slide.src = slide.url;
    });
  }

  returnDefaultSlideConfig() {
    this.indepthSliderConfig.centerMode = true;
    this.indepthSliderConfig.dots = true;
    this.indepthSliderConfig.centerPadding = '350px';
    if (this.slickCarousel) {
      this.slickCarousel
          .el
          .nativeElement
          .slick
          .slickSetOption(
              this.indepthSliderConfig
          );
    }
  }

  handleLikeWorkSpace(workSpace) {
    if (!this.checkIfUserSignedIn()) {
      return;
    }
    const likeState = !workSpace.isLiked;
    workSpace.isLiked = likeState;
    this.profileService.likeWorkSpace(workSpace.id).subscribe(() => {
      workSpace.isLiked = likeState;
    });
  }

  copyText(input) {
    input.focus();
    input.select();
    document.execCommand('copy');
    input.setSelectionRange(0, 0);
    this.messageService.showMessage('Copied');
  }

  parseVideoLink(link: string) {
    link = link.replace('watch?v=', '');
    if (link.includes('youtu.be') || link.includes('youtube.com')) {
      const a = document.createElement('a');
      a.href = link;
      a.host = 'youtube.com';
      if (!link.includes('embed')) {
        const pathnameArr = a.pathname
          .split('');
        pathnameArr.splice(0, 1, '/embed/');
        a.pathname = pathnameArr.join('');
        link = a.href;
      }
      a.remove();
    }
    return link;
  }

  checkIfUserSignedIn() {
    if (!this.userService.isLoggedin()) {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return false;
    } else if (
      this.userService.isLoggedin() &&
      this.userService.getUserType() === 'seller') {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['message'] = true;
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return false;
    }
    return true;
  }

  handleSlickInit(instance) {
    this.slickCarousel = instance;
  }
}
