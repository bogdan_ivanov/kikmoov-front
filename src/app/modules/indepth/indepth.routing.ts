import { Routes, RouterModule } from '@angular/router';
import { IndepthComponent } from './indepth.component';

const routes: Routes = [
  {
    path: '',
    component: IndepthComponent,
    data: {
        meta: {
            title: 'indepth.title',
            description: 'indepth.text',
            override: true,
        },
    },
  },
];

export const IndepthRoutes = RouterModule.forChild(routes);
