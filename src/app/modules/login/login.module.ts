import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { LoginRoutes } from './login.routing';
import { UserService } from '../../services/user.service';
import { LoginComponent } from './login.component';
import { MatCardModule } from '@angular/material';
import { LogindialogModule } from '@shared/layouts/logindialog/logindialog.module';

@NgModule({
  imports: [
      CommonModule,
      LoginRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      PostSpaceBarModule,
      NgxSmartModalModule.forRoot(),
      MatCardModule,
      LogindialogModule
  ],
  declarations: [
      LoginComponent
  ],
    providers: [
        UserService,
        NgxSmartModalService
    ]
})
export class LoginModule {}
