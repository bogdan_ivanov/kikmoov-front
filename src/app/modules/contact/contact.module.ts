import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { ContactRoutes } from './contact.routing';
import { ContactComponent } from './contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { ContactService } from '../../services/contact.service';
import { MessageService } from '../../services/message.service';

@NgModule({
  imports: [
      CommonModule,
      ContactRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      FormsModule,
      ReactiveFormsModule,
      MatFormFieldModule,
  ],
  declarations: [
      ContactComponent
  ],
  providers: [
      ContactService,
      MessageService
  ]
})

export class ContactModule {}
