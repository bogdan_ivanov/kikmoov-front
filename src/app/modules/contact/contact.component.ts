import {Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../../services/contact.service';
import { ErrorsService } from '../../services/errors.service';
import { MessageService } from '../../services/message.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public contactForm: FormGroup;

  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private errorService: ErrorsService,
              public ngxSmartModalService: NgxSmartModalService,
              private messageService: MessageService,
              @Inject('ORIGIN_URL') public baseUrl: string) {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  sendMessage(form) {
    this.contactService.sendMessage(form.value).subscribe(res => {
      form.reset();
      this.messageService.setMessage('Message was sent successfully');
      this.messageService.showMessage();
    }, err => {
      this.errorService.setFieldErrors(err, form);
    });
  }

  afterSendMessage() {
    this.ngxSmartModalService.getModal('afterSendMessageContact').open();
  }
}
