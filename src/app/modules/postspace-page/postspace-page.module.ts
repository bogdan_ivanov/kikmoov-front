import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import {
    MatButtonModule,
    MatCardModule, MatCheckboxModule, MatDatepickerModule, MatExpansionModule,
    MatFormFieldModule, MatIconModule,
    MatMenuModule, MatNativeDateModule, MatProgressSpinnerModule,
    MatSelectModule, MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import { PostSpacePageRoutes } from './postspace-page.routing';
import { PostspacePageComponent } from './postspace-page.component';
import { ContentService } from '../../services/content.service';
import { UserService } from '../../services/user.service';


@NgModule({
  imports: [
      CommonModule,
      PostSpacePageRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      MatCardModule,
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatProgressSpinnerModule,
      MatExpansionModule,
      MatNativeDateModule,
      MatDatepickerModule,
      MatSelectModule,
      MatSnackBarModule,
  ],
  declarations: [PostspacePageComponent],
    providers: [
        ContentService,
        UserService
    ]
})
export class PostspacePageModule {}
