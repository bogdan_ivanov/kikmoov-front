import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostspacePageComponent } from './postaspace-page.component';

describe('PostaspacePageComponent', () => {
  let component: PostspacePageComponent;
  let fixture: ComponentFixture<PostspacePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostspacePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostspacePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
