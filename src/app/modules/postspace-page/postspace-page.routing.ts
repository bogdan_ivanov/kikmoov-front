import { Routes, RouterModule } from '@angular/router';
import {PostspacePageComponent} from './postspace-page.component';

const routes: Routes = [
  {
    path: '',
    component: PostspacePageComponent,
    data: {
      meta: {
        title: 'postspace.title',
        description: 'postspace.text',
        override: true,
      },
    },
  },
];

export const PostSpacePageRoutes = RouterModule.forChild(routes);
