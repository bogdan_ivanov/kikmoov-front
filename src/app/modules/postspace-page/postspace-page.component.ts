import { Component, Inject, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { MessageService } from '../../services/message.service';
import { ContentService } from '../../services/content.service';

@Component({
  selector: 'app-postaspace-page',
  templateUrl: './postspace-page.component.html',
  styleUrls: ['./postspace-page.component.scss']
})
export class PostspacePageComponent implements OnInit {
  private userInfo = null;
  public faqContent = [
    {
      title: 'My workspace photos are not very good, can Kikmoov help? ',
      description:
        'Of course, absolutely. Here at kikmoov we relish beautiful office listings. Part of our service is to offer amazing professional pictures and VIDEO’s of your space. To request professional pictures and videos, please contact our real estate professionals.   '
    },
    {
      title: 'What are the legal aspects of renting a desk? ',
      description:
        'There are numerous legal ways to do this. To rent out a desk, co-working space or serviced office it is best done on a license basis. This license is typically a yearlong with mutual monthly breaks. For further details, please read the Kikmoov Terms of Business or get in touch with our real estate professionals. '
    },
    {
      title: 'How should one price their workspace? ',
      description:
        'As we live in free market, you can price your space as you please. However, to remain a market favourite it is important to price your space by market value. This is determined by area and office amenities. '
    },
    {
      title: 'How to post a space and advertise on kikmoov? ',
      description:
        'You can advertise your space with kikmoov. It takes minutes; all you have to do is create an account to then use our listing portal to upload your workspace to the market once reviewed and accepted. Simply done. '
    },
    {
      title:
        'Does Kikmoov take a commission for filling my space with clients? ',
      description:
        'To initially list your workspace on kikmoov, it is completely free to do so. Once we have found a tenant match for your workspace, we charge 10% for 12 months of the tenant’s duration (or the minimum term of the license whichever is the greater) for more details please see our terms of business. '
    },
    {
      title: 'What are the benefits of advertising on kikmoov? ',
      description:
        'Kikmoov is an easy, simple service to use. It takes minutes to upload your space, we send it to the market and to our database of users. We help with your listing as well paper work and legalities and have a full time professional real estate team at your disposal. We do all the work and you benefit from the reward.'
    }
  ];
  public proudToWork = [];

  constructor(public userService: UserService,
              private router: Router,
              private contentService: ContentService,
              @Inject('ORIGIN_URL') public baseUrl: string) {
    this.userService.loginStatus().subscribe(res => {
      this.userInfo = res;
    });
    this.contentService.getProudToWork().subscribe(
      res => this.proudToWork = res['proudToWork'],
      err => console.log(err)
    )
  }

  public getAbsoluteUrl(path: string) {
      return this.baseUrl + path;
  }

  ngOnInit() {}

  tryToGetStarted() {
    if (this.userService.isLoggedin() && this.userService.getUserType() === 'seller') {
      this.router.navigate(['/mainportal']);
    } else {
      this.router.navigate(['/signin/seller']);
    }
  }
}
