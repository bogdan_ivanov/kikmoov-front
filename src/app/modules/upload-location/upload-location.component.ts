/// <reference types="@types/googlemaps" />
import {AfterViewInit, Component, ElementRef, Inject, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import { LocationService } from '../../services/location.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorsService } from '../../services/errors.service';
import 'rxjs/add/operator/debounceTime';
// import PlacesServiceStatus = google.maps.places.PlacesServiceStatus;
// import DistanceMatrixStatus = google.maps.DistanceMatrixStatus;
import { PostcodesService } from '../../services/postcodes.service';
import * as moment from 'moment-timezone';
import { debounceTime, map } from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';
import {UniversalStorage} from '@shared/storage/universal.storage';

@Component({
  selector: 'app-upload-location',
  templateUrl: './upload-location.component.html',
  styleUrls: ['./upload-location.component.scss'],
})
export class UploadLocationComponent implements OnInit, AfterViewInit {
  @ViewChild('addressElement') addressElementRef: ElementRef;
  @ViewChild('area') areaElementRef: ElementRef;

  public bkList = [];
  public imageList;
  public imageFav;
  public workspaceId;
  public workSpaceList = {
    office: {pointed: true},
    desk: {state: false, pointed: false, saved: false},
    'meeting-room': {state: false, pointed: false, saved: false},
    'private-office': {state: false, pointed: false, saved: false},
  };
  public minDate = new Date();
  public officeId;
  public officeData;
  public payType = 'hour';
  public updating = false;
  public loading = false;

  public currentType = 'office';
  public currentOfficePosition = 0;
  public officeCreated = false;

  private isBrowser;

  public formTitles = {
    office: 'New Location',
    desk: 'Desk',
    'meeting-room': 'Meeting space',
    'private-office': 'Private office'
  };

  public formWorkspaceData = this.fb.group({
    name: [null, [Validators.required,  Validators.maxLength(40)]],
    address: [null, [Validators.required]],
    optionalAddress: [null],
    company: [null],
    latitude: [null, [Validators.required]],
    longitude: [null, [Validators.required]],
    town: [null, [Validators.required]],
    nearby: [[], []],
    postcode: [null, [Validators.required, Validators.maxLength(10)]],
    area: [null, [Validators.required]],
    description: [null, [Validators.required]],
  });

  public officeForms = {
    desk: this.fb.group({
      deskType: 'hourly_hot_desk',
      quantity: [null, [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^\d+$/)
      ]],
      price: [null, [
        Validators.required,
        Validators.max(1000),
        Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
      ]],
      availableFrom: [0, [Validators.required]],
      facilities: [[], []],
      description: [null, [Validators.required]],
      opensFrom: [null, [
        Validators.required,
        Validators.pattern(/^\d{2}:00$/)
      ]],
      closesAt: [null, [
        Validators.required,
        Validators.pattern(/^\d{2}:00$/)
      ]],
      minContractLength: [null, [Validators.pattern(/^\d+$/)]],
      type: 'desk',
    }),
    'private-office': this.fb.group({
      quantity: [null, [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^\d+$/)
      ]],
      price: [null, [
        Validators.required,
        Validators.max(100000),
        Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
      ]],
      size: [null, [
        Validators.minLength(1),
        Validators.min(1),
        Validators.max(500000),
        Validators.pattern(/^\d+$/)
      ]],
      capacity: [null, [
        Validators.required,
        Validators.min(1),
        Validators.max(5000),
        Validators.pattern(/^\d+$/)
      ]],
      minContractLength: [null, [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^\d+$/)
      ]],
      availableFrom: [0, [Validators.required]],
      facilities: [[], []],
      description: [null, [Validators.required]],
      type: 'private-office',
    }),
    'meeting-room': this.fb.group({
        deskType: 'hourly_meeting_room',
      quantity: [null, [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^\d+$/)
      ]],
      price: [null, [
        Validators.required,
        Validators.max(1000),
        Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
      ]],
      capacity: [null, [
        Validators.required,
        Validators.min(1),
        Validators.max(5000),
        Validators.pattern(/^\d+$/)
      ]],
      facilities: [[], []],
      opensFrom: [null, [
        Validators.required,
        Validators.pattern(/^\d{2}:00$/)
      ]],
      closesAt: [null, [
        Validators.required,
        Validators.pattern(/^\d{2}:00$/)
      ]],
      availableFrom: [
          0,
          [
              Validators.required
          ]
      ],
      description: [null, [Validators.required]],
      type: 'meeting-room',
    }),
  };

  public previousOffices = {};

  public openTimeList = [
    {value: '00:00', valid: true}, {value: '01:00', valid: true}, {value: '02:00', valid: true}, {
      value: '03:00',
      valid: true
    },
    {value: '04:00', valid: true}, {value: '05:00', valid: true}, {value: '06:00', valid: true}, {
      value: '07:00',
      valid: true
    },
    {value: '08:00', valid: true}, {value: '09:00', valid: true}, {value: '10:00', valid: true}, {
      value: '11:00',
      valid: true
    },
    {value: '12:00', valid: true}, {value: '13:00', valid: true}, {value: '14:00', valid: true}, {
      value: '15:00',
      valid: true
    },
    {value: '16:00', valid: true}, {value: '17:00', valid: true}, {value: '18:00', valid: true}, {
      value: '19:00',
      valid: true
    },
    {value: '20:00', valid: true}, {value: '21:00', valid: true}, {value: '22:00', valid: true}, {
      value: '23:00',
      valid: true
    }
  ];
  public closeTimeList = [
    {value: '00:00', valid: true}, {value: '01:00', valid: true}, {value: '02:00', valid: true}, {
      value: '03:00',
      valid: true
    },
    {value: '04:00', valid: true}, {value: '05:00', valid: true}, {value: '06:00', valid: true}, {
      value: '07:00',
      valid: true
    },
    {value: '08:00', valid: true}, {value: '09:00', valid: true}, {value: '10:00', valid: true}, {
      value: '11:00',
      valid: true
    },
    {value: '12:00', valid: true}, {value: '13:00', valid: true}, {value: '14:00', valid: true}, {
      value: '15:00',
      valid: true
    },
    {value: '16:00', valid: true}, {value: '17:00', valid: true}, {value: '18:00', valid: true}, {
      value: '19:00',
      valid: true
    },
    {value: '20:00', valid: true}, {value: '21:00', valid: true}, {value: '22:00', valid: true}, {
      value: '23:00',
      valid: true
    }
  ];
  public newLocation: boolean;

  public areasHolder = [];
  public suggestions = [];
  public needSuggestions = true;
  public needPostCodeUpdate = true;

  private PlacesServiceStatus;
  private DistanceMatrixStatus;

  constructor(
    public locationService: LocationService,
    public postcodesService: PostcodesService,
    public errorsService: ErrorsService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder,
    @Inject('ORIGIN_URL') public baseUrl: string,
    @Inject(PLATFORM_ID) private platformId,
    private universalStorage: UniversalStorage
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    if (this.isBrowser) {
        this.PlacesServiceStatus = google.maps.places.PlacesServiceStatus;
        this.DistanceMatrixStatus = google.maps.DistanceMatrixStatus;
    }
    this.route.params.subscribe(params => {
      if (!('id' in params) && !('office' in params)) {
        this.universalStorage.setItem('new_location', 'true');
      }
      if ('id' in params && 'office' in params === false) {
        this.workspaceId = params['id'];
        this.locationService.getLocation(+params['id']).subscribe(
          res => {
            const location = res;
            if (location) {
              this.updating = true;
              this.formTitles.office = `${!this.updating ? 'new ' : ''}location`;
              this.loadWorkspaceData(location);
            } else {
              throw new Error('Location not found');
            }
          },
          err => {
            this.errorsService.setErrors(err);
            this.errorsService.showError();
            this.errorsService.clearErrors();
          }
        );
      }
      if ('office' in params && ('officeId' in params) === false) {
        this.workspaceId = params['id'];
        if (this.universalStorage.checkItem('bkList')) {
          this.bkList = JSON.parse(this.universalStorage.getItem('bkList'));
          this.currentOfficePosition =
            this.bkList.findIndex(item => item === params['office']) + 1;
        }
        // if (JSON.parse(this.universalStorage.getItem('workSpaceList')) !== null) {
        if (this.universalStorage.checkItem('workSpaceList')) {
          this.workSpaceList = JSON.parse(
              this.universalStorage.getItem('workSpaceList')
          );
        }
        this.currentType = params['office'];
        // if (this.universalStorage.getItem('prevOffices') !== null) {
        if (this.universalStorage.checkItem('prevOffices')) {
          this.previousOffices = JSON.parse(this.universalStorage.getItem('prevOffices'));
          if (this.previousOffices.hasOwnProperty(params['office'])) {
            if (this.previousOffices[params['office']].hasOwnProperty('id')) {
              this.router.navigate([this.router.url, this.previousOffices[params['office']].id]);
            }
            this.loadOfficeData();
          }
        }
        this.officeForms[params['office']].valueChanges.subscribe(
          res => {
            this.addPreviousOffice(res, this.currentType);
          }
        );
        this.workSpaceList['office']['pointed'] = false;
        this.workSpaceList[params['office']]['pointed'] = true;
      } else if ('office' in params && 'officeId' in params) {
        if (this.universalStorage.checkItem('bkList')) {
          // if (JSON.parse(this.universalStorage.getItem('workSpaceList')) !== null) {
          if (this.universalStorage.checkItem('workSpaceList')) {
            this.workSpaceList = JSON.parse(
                this.universalStorage.getItem('workSpaceList')
            );
          }
          this.bkList = JSON.parse(this.universalStorage.getItem('bkList'));
          this.currentOfficePosition =
            this.bkList.findIndex(item => item === params['office']) + 1;
          this.officeCreated = true;
          this.workSpaceList['office']['pointed'] = false;
          this.workSpaceList[params['office']]['pointed'] = true;
          // if (this.universalStorage.getItem('prevOffices') !== null) {
          if (this.universalStorage.checkItem('prevOffices')) {
            this.previousOffices = JSON.parse(this.universalStorage.getItem('prevOffices'));
          }
        }
        this.currentType = params['office'];
        this.loadOfficeData(params['officeId']);
        this.workspaceId = params['id'];
        this.updating = true;
        this.formTitles.office = `${!this.updating ? 'new ' : ''}location`;
        this.officeId = +params['officeId'];
        this.officeForms[params['office']].valueChanges.subscribe(
          res => {
            this.addPreviousOffice(res, this.currentType);
          }
        );
        this.workSpaceList['office']['pointed'] = false;
        this.workSpaceList[params['office']]['pointed'] = true;
      }
    });
  }

  public getAbsoluteUrl(path: string) {
      return this.baseUrl + path;
  }

  ngOnInit() {
    this.officeForms['desk'].get('deskType').valueChanges.subscribe(
      res => {
        if (res === 'daily_hot_desk') {
          this.payType = 'day';
        } else {
            this.payType = res !== 'hourly_hot_desk' ? 'month' : 'hour';
        }
        //monthly
        if (res !== 'hourly_hot_desk' && res !== 'daily_hot_desk') {
            this.officeForms['desk'].get('minContractLength').setValidators([
                Validators.required,
                Validators.min(1),
                Validators.pattern(/^\d+$/)
            ]);
            this.officeForms['desk'].get('availableFrom').setValidators([Validators.required]);
            this.officeForms['desk'].get('price').clearValidators();
            this.officeForms['desk'].get('price').setValidators([
                Validators.max(100000),
                Validators.required,
                Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
            ]);
            this.officeForms['desk'].get('opensFrom').clearValidators();
            this.officeForms['desk'].get('closesAt').clearValidators();

            this.officeForms['desk'].get('price').updateValueAndValidity();
            this.officeForms['desk'].get('closesAt').updateValueAndValidity();
            this.officeForms['desk'].get('opensFrom').updateValueAndValidity();
            this.officeForms['desk'].get('availableFrom').updateValueAndValidity();
            this.officeForms['desk'].get('minContractLength').updateValueAndValidity();
        } else if (res === 'daily_hot_desk') {
            this.officeForms['desk'].get('minContractLength').clearValidators();
            this.officeForms['desk'].get('availableFrom').setValidators([Validators.required]);
            this.officeForms['desk'].get('price').clearValidators();
            this.officeForms['desk'].get('opensFrom').clearValidators();
            this.officeForms['desk'].get('closesAt').clearValidators();
            this.officeForms['desk'].get('price').setValidators([
                Validators.max(100000),
                Validators.required,
                Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
            ]);
            this.officeForms['desk'].get('closesAt').updateValueAndValidity();
            this.officeForms['desk'].get('opensFrom').updateValueAndValidity();
            this.officeForms['desk'].get('price').updateValueAndValidity();
            this.officeForms['desk'].get('availableFrom').updateValueAndValidity();
            this.officeForms['desk'].get('minContractLength').updateValueAndValidity();
        } else {
          this.officeForms['desk'].get('minContractLength').clearValidators();
          this.officeForms['desk'].get('availableFrom').clearValidators();
          this.officeForms['desk'].get('price').clearValidators();
          this.officeForms['desk'].get('price').setValidators([
            Validators.max(1000),
            Validators.required,
            Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
          ]);
          this.officeForms['desk'].get('opensFrom').setValidators([
            Validators.required,
            Validators.pattern(/^\d{2}:00$/)
          ]);
          this.officeForms['desk'].get('closesAt').setValidators([
            Validators.required,
            Validators.pattern(/^\d{2}:00$/)
          ]);
          this.officeForms['desk'].get('price').updateValueAndValidity();
          this.officeForms['desk'].get('closesAt').updateValueAndValidity();
          this.officeForms['desk'].get('opensFrom').updateValueAndValidity();
          this.officeForms['desk'].get('availableFrom').updateValueAndValidity();
          this.officeForms['desk'].get('minContractLength').updateValueAndValidity();
        }
      }
    );
    this.officeForms['meeting-room'].get('deskType').valueChanges.subscribe(
      res => {
        this.payType = res !== 'hourly_meeting_room' ? 'day' : 'hour';

        if (res === 'hourly_meeting_room') {
            this.officeForms['meeting-room'].get('availableFrom').clearValidators();
            this.officeForms['meeting-room'].get('price').clearValidators();
            this.officeForms['meeting-room'].get('price').setValidators([
                Validators.max(1000),
                Validators.required,
                Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
            ]);
            this.officeForms['meeting-room'].get('opensFrom').setValidators([
                Validators.required,
                Validators.pattern(/^\d{2}:00$/)
            ]);
            this.officeForms['meeting-room'].get('closesAt').setValidators([
                Validators.required,
                Validators.pattern(/^\d{2}:00$/)
            ]);
            this.officeForms['meeting-room'].get('price').updateValueAndValidity();
            this.officeForms['meeting-room'].get('closesAt').updateValueAndValidity();
            this.officeForms['meeting-room'].get('opensFrom').updateValueAndValidity();
            this.officeForms['meeting-room'].get('availableFrom').updateValueAndValidity();
        } else {
            this.officeForms['meeting-room'].get('availableFrom').clearValidators();
            this.officeForms['meeting-room'].get('closesAt').clearValidators();
            this.officeForms['meeting-room'].get('opensFrom').clearValidators();
            this.officeForms['meeting-room'].get('price').clearValidators();
            this.officeForms['meeting-room'].get('price').setValidators([
                Validators.max(1000),
                Validators.required,
                Validators.pattern(/^\d+(\.\d+|,\d+)?$/)
            ]);
            this.officeForms['meeting-room'].get('availableFrom').setValidators([Validators.required]);
            this.officeForms['meeting-room'].get('price').updateValueAndValidity();
            this.officeForms['meeting-room'].get('closesAt').updateValueAndValidity();
            this.officeForms['meeting-room'].get('opensFrom').updateValueAndValidity();
            this.officeForms['meeting-room'].get('availableFrom').updateValueAndValidity();
        }
      }
    );

    this.formWorkspaceData.get('postcode')
      .valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(async postcode => {
        if (this.needPostCodeUpdate) {
          const isValidPostcode = await this.postcodesService.validatePostCode(postcode);
          if (isValidPostcode) {
            this.postcodesService.getPostcodeData(postcode)
              .subscribe(postcodeData => {
                const area = this.areasHolder.find(v => (
                    v.name.replace(/ \(.+\)/i, '').toLowerCase().includes(postcodeData.admin_ward.toLowerCase()) ||
                    postcodeData.admin_ward.toLowerCase().includes(v.name.replace(/ \(.+\)/i, '').toLowerCase())
                  )
                );
                if (area) {
                  this.needSuggestions = false;
                  this.formWorkspaceData.get('area')
                    .setValue(area.name);
                } else {
                    this.suggestionsShow(true, this.areasHolder);
                    this.areaElementRef.nativeElement.focus();
                }
              });
          } else {
            this.formWorkspaceData.get('postcode')
              .setErrors({postcode: 'Invalid postcode'});
          }
        }
        this.needPostCodeUpdate = true;
      });

    if (this.currentType === 'office') {
      this.locationService.getAreaSuggestions()
        .pipe(
          debounceTime(500),
          map((areas: any[]) => areas.sort((a, b) => a.name > b.name ? 1 : -1))
        ).subscribe(areas => {
        this.areasHolder = areas;
      });
    }
    this.formWorkspaceData.get('area')
      .valueChanges
      .distinctUntilChanged()
      .subscribe(async area => {
        if (this.needSuggestions) {
          this.suggestionsShow(true, area);
        }
        this.needSuggestions = true;
      });
  }

  addressChanged(event) {
    if (!event.target.value) {
        this.formWorkspaceData.get('postcode').reset();
        this.formWorkspaceData.get('town').reset();
        this.formWorkspaceData.get('area').reset();
        this.suggestionsShow(false, []);
    }
  }

  ngAfterViewInit() {
    if (this.currentType === 'office')
      this.initAutoComplete();
  }

  suggestionsShow(show, area) {
    if (show && typeof area === 'string') {
      this.suggestions = this.areasHolder
        .filter(area$ => area$.name.toLowerCase().includes(area.toLowerCase()));
    } else if (show && Array.isArray(area)) {
      this.suggestions = area;
    } else {
      setTimeout(() => {
        this.suggestions = [];
      }, 150);
    }
  }

  loadWorkspaceData(location) {
    this.needPostCodeUpdate = false;
    Object.keys(location).forEach(fieldName => {
      if (fieldName === 'address') {
        this.addressElementRef.nativeElement.value = location[fieldName];
      }
      if (this.formWorkspaceData.get(fieldName) !== null) {
        this.formWorkspaceData.get(fieldName).setValue(location[fieldName]);
      }
    });
  }

  loadOfficeData(id?) {
    if (id) {
      this.locationService.getSellerOffice(id).subscribe(
        res => {
          const office = res;
          office['facilities'] = office['facilities'].map(facility => facility.slug);
          this.officeData = office;
          this.imageList = office['images'];
          if (this.imageList.length && office['coverImageUrl']) {
            this.setMainImage(+office['coverImageId']);
          }
          const officeForm = <FormGroup>this.officeForms[office['type']];
          Object.keys(office).forEach(fieldName => {
            if (officeForm.get(fieldName) !== null) {
              if (fieldName === 'availableFrom' && office[fieldName]) {
                office[fieldName] = new Date((+office[fieldName] * 1000));
              } else if (fieldName === 'opensFrom') {
                this.validateTime(office[fieldName], 'closeTimeList');
              } else if (fieldName === 'closesAt') {
                this.validateTime(office[fieldName], 'openTimeList');
              }
              officeForm.get(fieldName).setValue(office[fieldName]);
            }
          });
          if (office['type'] !== 'meeting-room') {
              setTimeout(() => {
                  officeForm.get('availableFrom').clearValidators();
                  officeForm.get('availableFrom').setValidators([Validators.required]);
                  officeForm.get('availableFrom').updateValueAndValidity();
              }, 400);
          }
        },
        err => {
          console.log(err);
        }
      );
    } else if (this.previousOffices.hasOwnProperty(this.currentType)) {
      const office = this.previousOffices[this.currentType];
      const officeForm = <FormGroup>this.officeForms[office['type']];
      Object.keys(office).forEach(fieldName => {
        if (officeForm.get(fieldName) !== null) {
          if (fieldName === 'availableFrom' && office[fieldName]) {
            office[fieldName] = new Date((+office[fieldName] * 1000));
          } else if (fieldName === 'opensFrom') {
            this.validateTime(office[fieldName], 'closeTimeList');
          } else if (fieldName === 'closesAt') {
            this.validateTime(office[fieldName], 'openTimeList');
          }
          officeForm.get(fieldName).setValue(office[fieldName]);
        }
      });
    } else {
      this.errorsService.setErrors({message: 'Office not found'});
      this.router.navigate(['/mainportal']);
    }
  }

  updateLocation(locationData) {
    if (locationData.value.address !== this.addressElementRef.nativeElement.value) {
      locationData.get('address').setErrors({not_found: 'Address not found'});
    }
    this.needPostCodeUpdate = false;
    const value = locationData.value;
    if (!this.areasHolder.some(area => area.name === value['area'])) {
      value['area'] = this.areasHolder.find(area => (
        area.name.includes(value['area'])
      ));
      if (value['area']) {
        value['area'] = value['area'].slug;
      } else {
        this.formWorkspaceData.get('area').setErrors({invalid_area: true});
        this.suggestionsShow(true, this.areasHolder);
      }
    } else {
      value['area'] = this.getAreaSlug(value['area']);
    }
    if (this.checkFormErrors()) {
      return;
    }
    locationData.disable();

    // Remove empty nearBy[] if address field is untouched
    if(this.formWorkspaceData.controls.address.untouched) {
      delete value.nearby;
    }
    this.locationService
      .updateLocation(value, this.workspaceId)
      .subscribe(
        () => {
          if (this.bkList.length) {
            this.doOfficeProcceed();
          } else {
            this.router.navigate(['/mainportal']);
          }
          locationData.enable();
        },
        err => {
          console.log(err);
          locationData.enable();
          this.needPostCodeUpdate = true;
        }
      );
  }

  createOffice(event, officeData) {
    const value = officeData.value;
    officeData.disable();
    return this.locationService
      .createOffice(this.workspaceId, value)
      .subscribe(
        res => {
          officeData.value['id'] = res['id'];
          this.addPreviousOffice(value, this.currentType);
          this.nextOfficeProcceed();
          officeData.enable();
        },
        err => {
          console.log(err);
          officeData.enable();
        }
      );
  }

  updateOffice(officeData) {
    const value = officeData.value;
    const valueAvailableFrom = value.availableFrom instanceof moment ?
      value.availableFrom.unix() * 1000 :
      (value.availableFrom instanceof Date ? value.availableFrom.getTime() : value.availableFrom);
    const valueAvailableFromData = this.officeData.availableFrom instanceof moment ?
      this.officeData.availableFrom.unix() * 1000 :
      (this.officeData.availableFrom instanceof Date ? this.officeData.availableFrom.getTime() : this.officeData.availableFrom);
    officeData.disable();
    if (value.availableFrom &&
      this.officeData.availableFrom &&
      valueAvailableFrom === valueAvailableFromData) {
      delete value.availableFrom;
    }
    this.locationService
      .updateOffice(value, this.officeId)
      .subscribe(
        res => {
          if (this.officeCreated) {
            this.updating = false;
            value['id'] = this.officeId;
            this.addPreviousOffice(value, this.currentType);
            this.nextOfficeProcceed();
            this.imageList = [];
          } else {
            this.router.navigate(['/mainportal']);
          }
          officeData.enable();
        },
        err => {
          console.log(err);
          officeData.enable();
        }
      );
  }

  nextOfficeProcceed() {
    if (this.checkFormErrors()) {
      return;
    }
    this.workSpaceList[this.currentType]['saved'] = true;
    this.workSpaceList[this.currentType]['pointed'] = false;
    let path;
    const subUri = this.router.url.match(this.currentType + '/' + this.officeId) !== null ?
      this.currentType + '/' + this.officeId :
      this.currentType;
    const url = this.router.url.replace(subUri, '');
    for (const key of this.bkList) {
      if (this.workSpaceList.hasOwnProperty(key)) {
        if (!this.workSpaceList[key]['saved']) {
          if (this.workSpaceList[key]['state']) {
            if (this.universalStorage.getItem('prevOffices') !== null) {
              this.previousOffices = JSON.parse(this.universalStorage.getItem('prevOffices'));
              if (this.previousOffices[key] && this.previousOffices[key]['id']) {
                this.officeId = this.previousOffices[key]['id'];
                path = [url, key, this.officeId];
              } else {
                path = [url, key];
              }
            } else {
              path = [url, key];
            }
            this.workSpaceList[key]['pointed'] = true;
            this.currentType = key;
              this.universalStorage.removeItem('workSpaceList');
              this.universalStorage.setItem(
              'workSpaceList',
              JSON.stringify(this.workSpaceList)
            );
            this.router.navigate(path);
            return;
          }
          continue;
        }
      }
    }
      this.universalStorage.removeItem('workSpaceList');
      this.universalStorage.removeItem('bkList');
      this.universalStorage.removeItem('prevOffices');
    this.router.navigate(['/mainportal']);
  }

  procceedBack() {
    this.workSpaceList[this.currentType]['pointed'] = false;
    this.workSpaceList[this.currentType]['saved'] = false;
    const subUri = this.router.url.match(this.currentType + '/' + this.officeId) !== null ?
      this.currentType + '/' + this.officeId :
      this.currentType;
    const url = this.router.url.replace(subUri, '');
    for (const key of this.bkList.reverse()) {
      if (this.workSpaceList.hasOwnProperty(key)) {
        if (this.workSpaceList[key]['saved'] && this.currentType !== key) {
          if (this.workSpaceList[key]['state']) {
            this.previousOffices = JSON.parse(this.universalStorage.getItem('prevOffices'));
            this.officeId = this.previousOffices[key]['id'];
            this.workSpaceList[key]['pointed'] = true;
            this.workSpaceList[key]['saved'] = false;
            this.currentType = key;
              this.universalStorage.setItem(
              'workSpaceList',
              JSON.stringify(this.workSpaceList)
            );
            this.router.navigate([url, this.currentType, this.officeId]);
            return;
          }
          continue;
        }
      }
    }
  }

  async handleExit() {
    const officeState = JSON.parse(this.universalStorage.getItem('officeState'));
    this.newLocation = Boolean(this.universalStorage.getItem('new_location'));
    if (officeState) {
      if (officeState.images) {
        if (officeState.officeCreated) {
          officeState.images.forEach(async image => {
            await this.locationService.deleteOfficeImage(officeState.officeId, image).toPromise();
          });
          if (!this.newLocation) {
            await this.locationService.deleteOffice(officeState.officeId).toPromise();
          }
        } else {
          officeState.images.forEach(async image => {
            await this.locationService.deleteOfficeImage(officeState.officeId, image).toPromise();
          });
        }
      }
    }
    if (this.workspaceId && this.newLocation) {
        this.universalStorage.removeItem('active_location');
      await this.locationService.deleteLocation(this.workspaceId).toPromise();
    }
      this.universalStorage.removeItem('new_location');
    this.router.navigate(['/mainportal']);
  }

  facilityChange(event) {
    const facilitiesControl = this.officeForms[this.currentType].get('facilities');
    const values = [...facilitiesControl.value];
    if (!values.includes(event.val)) {
      values.push(event.val);
    } else {
      const index = values.findIndex(item => item === event.val);
      values.splice(index, 1);
    }
    facilitiesControl.setValue(values);
  }

  onFileChanged(event) {
    if (this.loading) {
      return false;
    }
    this.loading = true;
    this.locationService
      .createOffice(this.workspaceId, this.officeForms[this.currentType].value)
      .subscribe(
        res => {
          this.officeId = res['id'];
          this.locationService
            .uploadImage(this.officeId, event.target.files)
            .subscribe(
              resImages => {
                this.officeData = this.officeForms[this.currentType].value;
                this.imageList = resImages['images'];
                if (this.imageList.length) {
                  this.selectMainImage(this.imageList[0].id);
                }
                this.updating = true;
                this.formTitles.office = `${!this.updating ? 'new ' : ''}location`;
                this.officeCreated = true;
                event.target.value = '';
                this.officeForms[this.currentType].value.id = this.officeId;
                this.addPreviousOffice(
                  this.officeForms[this.currentType].value,
                  this.currentType,
                  {
                    officeCreated: true,
                    officeId: this.officeId,
                    images: resImages['images'].map(image => image.id)
                  }
                );
                this.loading = false;
              },
              err => {
                this.errorsService.setErrors(err);
                this.errorsService.showError();
                this.errorsService.clearErrors();
                this.loading = false;
                event.target.value = '';
                console.log(err);
              }
            );
        },
        err => {
          console.log(err);
          this.loading = false;
          event.target.value = '';
          if (this.checkFormErrors()) {
            return;
          }
        }
      );
  }

  updateImages(event) {
    this.loading = true;
    this.locationService
      .uploadImage(this.officeId, event.target.files)
      .subscribe(
        resImages => {
          this.addPreviousOffice(
            this.officeForms[this.currentType].value,
            this.currentType,
            {
              officeCreated: false,
              officeId: this.officeId,
              images: resImages['images']
                .filter(image => !!this.imageList.find(item => item.id !== image.id))
                .map(image => image.id)
            }
          );
          this.imageList = resImages['images'];
          if (this.imageList.length) {
            this.selectMainImage(this.imageList[0].id);
          }
          this.loading = false;
          event.target.value = '';
        },
        err => {
          this.errorsService.setErrors(err);
          this.errorsService.showError();
          this.errorsService.clearErrors();
          this.loading = false;
          event.target.value = '';
          console.log(err);
        }
      );
  }

  selectMainImage(imageId) {
    this.setMainImage(imageId);
    this.locationService.setMainImage(this.officeId, imageId).subscribe(
      res => {
        this.setMainImage(+imageId);
      },
      err => {
        console.log(err);
      }
    );
  }

  setMainImage(imageId) {
    const coverImageIndex = typeof imageId !== 'string' ? this.imageList.findIndex(image => +image.id === +imageId) :
      this.imageList.findIndex(image => image.url === imageId);
    const coverImage = this.imageList.splice(coverImageIndex, 1);
    this.imageFav = coverImage[0].id;
    this.imageList.unshift(coverImage[0]);
  }

  removeImage(imageId) {
    this.locationService.deleteOfficeImage(this.officeId, imageId).subscribe(
      res => {
        const imageIndex = this.imageList.findIndex(img => +img.id === +imageId);
        this.imageList.splice(imageIndex, 1);
        if (this.imageList.length) {
          this.selectMainImage(this.imageList[0].id);
        }
      },
      err => {
        this.errorsService.setErrors(err);
        this.errorsService.showError();
        this.errorsService.clearErrors();
        console.log(err);
      }
    );
  }

  setWorkingSpace(event) {
    this.workSpaceList[event['id']]['state'] = event['val'];
    /* block should be removed */
    if (event['val']) {
      this.bkList.push(event['id']);
    } else {
      this.bkList.splice(this.bkList.indexOf(event['id']), 1);
    }
      this.universalStorage.removeItem('bkList');
      this.universalStorage.setItem('bkList', JSON.stringify(this.bkList));
  }

  doOfficeProcceed() {
    const value = this.formWorkspaceData.value;
    this.needPostCodeUpdate = false;
    if (!this.areasHolder.some(area => area.name === value['area'])) {
      value['area'] = this.areasHolder.find(area => (
        area.name.includes(value['area'])
      ));
      if (value['area']) {
        value['area'] = value['area'].slug;
      } else {
        this.formWorkspaceData.get('area').setErrors({invalid_area: true});
        this.suggestionsShow(true, this.areasHolder);
      }
    } else {
      value['area'] = this.getAreaSlug(value['area']);
    }
    if (this.checkFormErrors()) {
      return;
    }
    this.formWorkspaceData.disable();
    window.scrollTo(0, 0);
    if (!this.workspaceId) {
      this.locationService
        .createLocation(value)
        .subscribe(
          res => {
              this.universalStorage.setItem('active_location', res['id']);
            this.workspaceId = res['id'];
            this.workSpaceList['office']['pointed'] = false;
            for (const key of this.bkList) {
              if (this.workSpaceList.hasOwnProperty(key)) {
                if (this.workSpaceList[key]['state']) {
                  this.workSpaceList[key]['pointed'] = true;
                  this.currentType = key;
                    this.universalStorage.removeItem('workSpaceList');
                    this.universalStorage.setItem(
                    'workSpaceList',
                    JSON.stringify(this.workSpaceList)
                  );
                  this.router.navigate([
                    this.router.url,
                    this.workspaceId,
                    this.currentType,
                  ]);
                  this.formWorkspaceData.enable();
                  return;
                }
                continue;
              }
            }
            this.router.navigate(['/mainportal']);
            this.formWorkspaceData.enable();
          },
          err => {
            console.log(err);
            this.formWorkspaceData.enable();
            this.needPostCodeUpdate = true;
          }
        );
    } else {
        this.universalStorage.setItem('active_location', this.workspaceId);
      this.workSpaceList['office']['pointed'] = false;
      for (const key of this.bkList) {
        if (this.workSpaceList.hasOwnProperty(key)) {
          if (this.workSpaceList[key]['state']) {
            this.workSpaceList[key]['pointed'] = true;
            this.currentType = key;
              this.universalStorage.removeItem('workSpaceList');
              this.universalStorage.setItem(
              'workSpaceList',
              JSON.stringify(this.workSpaceList)
            );
            this.router.navigate([
              this.router.url,
              this.currentType,
            ]);
            this.formWorkspaceData.enable();
            return;
          }
          continue;
        }
      }
      this.router.navigate(['/mainportal']);
      this.formWorkspaceData.enable();
    }
  }

  addPreviousOffice(office, type, state?) {
    if (!this.previousOffices[type]) {
      this.previousOffices[type] = {};
    }
    this.previousOffices[type] = Object.assign(this.previousOffices[type], office);
      this.universalStorage.setItem('prevOffices', JSON.stringify(this.previousOffices));
    if (state) {
      let officeState = this.universalStorage.checkItem('officeState') ? JSON.parse(this.universalStorage.getItem('officeState')) : false;
      if (!officeState) {
        officeState = state;
      } else {
        officeState = Object.assign(officeState, state);
      }
        this.universalStorage.setItem('officeState', JSON.stringify(officeState));
    }
  }

  changeHandler(event, officeData: FormGroup) {
    if (this.checkFormErrors()) {
      return;
    }
    if (this.updating) {
      window.scrollTo(0, 0);
      this.updateOffice(officeData);
    } else {
      window.scrollTo(0, 0);
      this.createOffice(event, officeData);
    }
  }

  initAutoComplete() {
    if (this.isBrowser) {
        const addressInput = this.addressElementRef.nativeElement;
        const autocomplete = new google.maps.places.Autocomplete(addressInput, {
            bounds: new google.maps.LatLngBounds(
                new google.maps.LatLng(51.332757, -0.475159),
                new google.maps.LatLng(51.680219, 0.191574)
            ),
            strictBounds: true,
            componentRestrictions: {
                country: 'GB'
            }
        });
        google.maps.event.addListener(autocomplete, 'place_changed', () => this.setAddressData(autocomplete));
    }
  }

  async setAddressData(autocomplete) {
      if (this.isBrowser) {
          const distance = new google.maps.DistanceMatrixService();
          const places = new google.maps.places.PlacesService(document.createElement('div'));
          const place = autocomplete.getPlace();
          const address = this.getFormattedAddress(place);
          const postCode = place.address_components.find(item => item.types.includes('postal_code'));
          const town = place.address_components.find(item => item.types.includes('postal_town'));

          this.addressElementRef.nativeElement.value = address;
          this.formWorkspaceData.get('address').setValue(address);
          if (!place.address_components.find(item => item.types.includes('street_number'))) {
              this.formWorkspaceData.get('address').setErrors({street_number: true}, {emitEvent: true});
              this.formWorkspaceData.get('address').markAsTouched();
          }

          const nearBy = [];
          places.nearbySearch({
              location: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
              radius: 2000,
              type: 'subway_station'
          }, (res, placeStatus) => {
              if (placeStatus === this.PlacesServiceStatus.OK) {
                  res.forEach(item => {
                      const place$: {
                          type?: string,
                          name?: string,
                          distance?: string,
                          duration?: string
                      } = {};
                      place$.name = item.name;
                      if (item.types.includes('subway_station')) {
                          place$.type = 'subway_station';
                      }
                      distance.getDistanceMatrix({
                          origins: [new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())],
                          destinations: [new google.maps.LatLng(item.geometry.location.lat(), item.geometry.location.lng())],
                          travelMode: google.maps.TravelMode.WALKING
                      }, (r, distanceStatus) => {
                          if (distanceStatus === this.DistanceMatrixStatus.OK) {
                              const row = r.rows.find(rowItem => 'elements' in rowItem);
                              const element = row.elements.find(elementItem => 'distance' in elementItem && 'duration' in elementItem);
                              place$.distance = element.distance.text;
                              place$.duration = element.duration.text;
                              if (parseFloat(place$.duration) <= 15 && !nearBy.some(elem => elem.name === place$.name)) {
                                  nearBy.push(place$);
                              }
                          }
                      });
                  });
              }
          });

          places.nearbySearch({
              location: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
              radius: 2000,
              type: 'transit_station'
          }, (res, placeStatus) => {
              if (placeStatus === this.PlacesServiceStatus.OK) {
                  res.forEach(item => {
                      const place$: {
                          type?: string,
                          name?: string,
                          distance?: string,
                          duration?: string
                      } = {};
                      place$.name = item.name;
                      if (item.types.includes('transit_station')) {
                          place$.type = 'subway_station';
                      }
                      distance.getDistanceMatrix({
                          origins: [new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())],
                          destinations: [new google.maps.LatLng(item.geometry.location.lat(), item.geometry.location.lng())],
                          travelMode: google.maps.TravelMode.WALKING
                      }, (r, distanceStatus) => {
                          if (distanceStatus === this.DistanceMatrixStatus.OK) {
                              const row = r.rows.find(rowItem => 'elements' in rowItem);
                              const element = row.elements.find(elementItem => 'distance' in elementItem && 'duration' in elementItem);
                              place$.distance = element.distance.text;
                              place$.duration = element.duration.text;
                              if (parseFloat(place$.duration) <= 15 && !nearBy.some(elem => elem.name === place$.name)) {
                                  nearBy.push(place$);
                              }
                          }
                      });
                  });
              }
          });

          places.nearbySearch({
              location: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
              radius: 2000,
              type: 'train_station'
          }, (res, placeStatus) => {
              if (placeStatus === this.PlacesServiceStatus.OK) {
                  res.forEach(item => {
                      const place$: {
                          type?: string,
                          name?: string,
                          distance?: string,
                          duration?: string
                      } = {};
                      place$.name = item.name;
                      if (item.types.includes('train_station')) {
                          place$.type = 'train_station';
                      }
                      distance.getDistanceMatrix({
                          origins: [new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())],
                          destinations: [new google.maps.LatLng(item.geometry.location.lat(), item.geometry.location.lng())],
                          travelMode: google.maps.TravelMode.WALKING
                      }, (r, distanceStatus) => {
                          if (distanceStatus === this.DistanceMatrixStatus.OK) {
                              const row = r.rows.find(rowItem => 'elements' in rowItem);
                              const element = row.elements.find(elementItem => 'distance' in elementItem && 'duration' in elementItem);
                              place$.distance = element.distance.text;
                              place$.duration = element.duration.text;
                              if (parseFloat(place$.duration) <= 15 && !nearBy.some(elem => elem.name === place$.name)) {
                                  nearBy.push(place$);
                              }
                          }
                      });
                  });
              }
          });

          places.nearbySearch({
              location: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
              radius: 2000,
              type: 'bus_station'
          }, (res, placeStatus) => {
              if (placeStatus === this.PlacesServiceStatus.OK) {
                  res.forEach(item => {
                      const place$: {
                          type?: string,
                          name?: string,
                          distance?: string,
                          duration?: string
                      } = {};
                      place$.name = item.name;
                      if (item.types.includes('bus_station')) {
                          place$.type = 'bus_station';
                      }
                      distance.getDistanceMatrix({
                          origins: [new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())],
                          destinations: [new google.maps.LatLng(item.geometry.location.lat(), item.geometry.location.lng())],
                          travelMode: google.maps.TravelMode.WALKING
                      }, (r, distanceStatus) => {
                          if (distanceStatus === this.DistanceMatrixStatus.OK) {
                              const row = r.rows.find(rowItem => 'elements' in rowItem);
                              const element = row.elements.find(elementItem => 'distance' in elementItem && 'duration' in elementItem);
                              place$.distance = element.distance.text;
                              place$.duration = element.duration.text;
                              if (parseFloat(place$.duration) <= 15 && !nearBy.some(elem => elem.name === place$.name)) {
                                  nearBy.push(place$);
                              }
                          }
                      });
                  });
              }
          });

          if (postCode) {
              const isValidPostcode = await this.postcodesService.validatePostCode(postCode.long_name);
              if (isValidPostcode) {
                  this.formWorkspaceData.get('postcode').setValue(postCode.long_name);
              } else {
                  this.suggestionsShow(true, this.areasHolder);
              }
          } else {
              this.suggestionsShow(true, this.areasHolder);
          }
          if (town) {
              this.formWorkspaceData.get('town').setValue(town.long_name);
          }
          this.formWorkspaceData.get('nearby').setValue(nearBy);
          this.formWorkspaceData.get('latitude').setValue(place.geometry.location.lat());
          this.formWorkspaceData.get('latitude').markAsDirty();
          this.formWorkspaceData.get('longitude').setValue(place.geometry.location.lng());
          this.formWorkspaceData.get('longitude').markAsDirty();
      }
  }

  checkFormErrors() {
    const form = this.officeForms[this.currentType] || this.formWorkspaceData;

    if (this.formWorkspaceData) {
        if (this.formWorkspaceData.get('address').touched && !this.formWorkspaceData.get('latitude').dirty && !this.formWorkspaceData.get('longitude').dirty) {
            this.formWorkspaceData.get('address').setErrors({not_choosed: 'Please, choose address from suggestions'}, {emitEvent: true});
            this.formWorkspaceData.get('address').markAsTouched();
            return true;
        }
    }

    if (form.invalid) {
      for (const controlName of Object.keys(form.controls)) {
        form.controls[controlName].markAsTouched();
      }
      this.errorsService.setErrors({message: 'Please, fill all fields first.'});
      this.errorsService.showError();
      this.errorsService.clearErrors();
      return true;
    } else if (this.currentType !== 'office' && !this.imageList || this.currentType !== 'office' && !this.imageList.length) {
      this.errorsService.setErrors({message: 'Please, upload at least one image.'});
      this.errorsService.showError();
      this.errorsService.clearErrors();
      return true;
    }
    return false;
  }

  validateTime(time, type: 'openTimeList' | 'closeTimeList') {
    if (time) {
      time = time && typeof time === 'string' ? time : time.value;
      if (time.match(/\d{2}:\d{2}/i)) {
        this[type].map(item => {
          if (type === 'closeTimeList') {
            item.valid = Number.parseInt(time) < Number.parseInt(item.value);
          } else {
            item.valid = Number.parseInt(time) > Number.parseInt(item.value);
          }
          return item;
        });
      }
    }
  }

  getFormattedAddress(place) {
    const address = [];
    place.address_components.forEach(item => {
      if (
        (item['types'].indexOf('street_number') > -1) ||
        (item['types'].indexOf('route') > -1) ||
        (item['types'].indexOf('postal_town') > -1) ||
        (item['types'].indexOf('administrative_area_level_2') > -1) ||
        (item['types'].indexOf('postal_code') > -1)
      ) {
        address.push(item['long_name']);
      }
    });
    const street = address.splice(0, 2);
    address.unshift(street.join(' '));
    return address.join(', ');
  }

  submitSuggestion(area) {
    this.formWorkspaceData.get('area').setValue(area);
    this.formWorkspaceData.get('area').updateValueAndValidity();
    this.suggestions = [];
  }

  handleSuggestions(suggestion) {
    return suggestion.name;
  }

  getAreaSlug(areaName) {
    return this.areasHolder.find(area => area.name === areaName).slug;
  }
}
