import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { LocationService } from '../../services/location.service';
import { ErrorsService } from '../../services/errors.service';
import { UploadLocationRoutes } from './upload-location.routing';
import { UploadLocationComponent } from './upload-location.component';
import { PostcodesService } from '../../services/postcodes.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { KmCheckboxModule } from '@shared/layouts/km-checkbox/km-checkbox.module';
import { FacilitiesComponent } from './facilities/facilities.component';


@NgModule({
  imports: [
      CommonModule,
      UploadLocationRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      FormsModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatProgressSpinnerModule,
      MatExpansionModule,
      MatNativeDateModule,
      MatDatepickerModule,
      MatSelectModule,
      MatSnackBarModule,
      PostSpaceBarModule,
      KmCheckboxModule
  ],
  declarations: [
      UploadLocationComponent,
      FacilitiesComponent
  ],
    providers: [
        LocationService,
        PostcodesService,
        ErrorsService
    ]
})
export class UploadLocationModule {}
