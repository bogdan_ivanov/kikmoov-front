import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LocationService } from '../../../services/location.service';
import { FormControl } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss'],
})
export class FacilitiesComponent implements OnInit {
  @Output()
  changed: EventEmitter<any> = new EventEmitter();
  @Input()
  checked: FormControl;
  @Input()
  userId;
  @Input()
  id;
  @Input()
  facilitiesList;

  private checkboxes;

  constructor(private locationService: LocationService, private router: Router) {
    this.router.events.subscribe((e) => {
      this.check(this.checkboxes, []);
    });
    this.locationService.getServiceFacilites().subscribe(
      res => {
        this.facilitiesList = res;
        setTimeout(() => {
          this.checkboxes = document.querySelectorAll('.facilities-col input');
          this.check(this.checkboxes, this.checked.value);
        }, 100);
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    this.checked.valueChanges.subscribe(facilities => {
      this.check(this.checkboxes, facilities);
    });
  }

  onFacilitiesChange(event, val) {
    this.changed.emit({ id: val, val: event });
  }

  check(checkboxes, facilities) {
    if (checkboxes) {
      checkboxes.forEach((checkbox: HTMLInputElement) => {
        checkbox.checked = facilities.includes(checkbox.value);
      });
    }
  }
}
