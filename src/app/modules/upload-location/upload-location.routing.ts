import { Routes, RouterModule } from '@angular/router';
import { UploadLocationComponent } from './upload-location.component';
import { AuthGuard } from '../../middleware/is-auth.guard';
import { SellerGuard } from '../../middleware/is-seller.guard';

const routes: Routes = [
  {
    path: '',
    component: UploadLocationComponent,
    canActivate: [AuthGuard, SellerGuard],
    data: {
      meta: {
        title: 'uploadLocation.title',
        description: 'uploadLocation.text',
        override: true,
      },
    },
  },
];

export const UploadLocationRoutes = RouterModule.forChild(routes);
