import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { MessageService } from '../../services/message.service';
import { ProfileService } from '../../services/profile.service';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyerChangePasswordComponent } from './buyer-change-password.component';
import { BuyerChangePasswordRoutes } from './buyer-change-password.routing';
import { ErrorsService } from '../../services/errors.service';

@NgModule({
  imports: [
      CommonModule,
      BuyerChangePasswordRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      MatCardModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      BuyerChangePasswordComponent
  ],
    providers: [
        ProfileService,
        MessageService,
        ErrorsService
    ]
})
export class BuyerChangePasswordModule {}
