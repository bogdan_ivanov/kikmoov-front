import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '../../services/profile.service';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';

@Component({
  selector: 'app-buyer-change-password',
  templateUrl: './buyer-change-password.component.html',
  styleUrls: ['./buyer-change-password.component.scss']
})
export class BuyerChangePasswordComponent implements OnInit {
  public passwordForm: FormGroup;

  constructor(private profileService: ProfileService,
              private messageService: MessageService,
              private errorService: ErrorsService,
              private fb: FormBuilder) {
    this.passwordForm = this.fb.group({
      oldPassword: [null, [Validators.required]],
      newPassword: [null, [Validators.required]],
      repeatNewPassword: [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  changePassword(form) {
    if (form.get('newPassword').value !== form.get('repeatNewPassword').value) {
      form.get('repeatNewPassword').setErrors({'not_match': 'Passwords not match'});
      return;
    }
    this.profileService.changePassword({
      oldPassword: form.get('oldPassword').value,
      newPassword: form.get('newPassword').value
    }).subscribe(res => {
      this.messageService.setMessage('Password was changed successfully');
      this.messageService.showMessage();
    }, err => {
      this.errorService.setErrors(err);
      this.errorService.showError();
      console.log(err);
    });
  }

}
