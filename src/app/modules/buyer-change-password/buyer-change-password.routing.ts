import { Routes, RouterModule } from '@angular/router';
import { BuyerChangePasswordComponent } from './buyer-change-password.component';
import { AuthGuard } from '../../middleware/is-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: BuyerChangePasswordComponent,
    canActivate: [AuthGuard],
    data: {
      meta: {
        title: 'profileChangePassword.title',
        description: 'profileChangePassword.text',
        override: true,
      },
    },
  },
];

export const BuyerChangePasswordRoutes = RouterModule.forChild(routes);
