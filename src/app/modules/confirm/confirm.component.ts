import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html'
})
export class ConfirmComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private messageService: MessageService,
    private errorService: ErrorsService
  ) {
    this.route.params.subscribe(params => {
      if ('pin' in params) {
        this.userService.confirmUser({token: params['pin']}).subscribe(
          res => {
            this.messageService.setMessage(res['message']);
            this.userService.doUserAuth(res);
            if (res['accountType'] === 'seller') {
              this.router.navigate(['/mainportal']);
            } else {
              this.router.navigate(['/']);
            }
          },
          err => {
            this.errorService.setErrors({message: 'Invalid token'});
            this.router.navigate(['/']);
          }
        );
      }
    });
  }

  ngOnInit() {
  }

}
