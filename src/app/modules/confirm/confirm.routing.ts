import { Routes, RouterModule } from '@angular/router';
import { ConfirmComponent } from './confirm.component';

const routes: Routes = [
  {
    path: '',
    component: ConfirmComponent,
    data: {
      meta: {
        title: 'confirm.title',
        description: 'confirm.text',
        override: true,
      },
    },
  },
];

export const ConfirmRoutes = RouterModule.forChild(routes);
