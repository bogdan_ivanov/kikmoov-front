import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { UserService } from '../../services/user.service';
import { ConfirmComponent } from './confirm.component';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';
import { ConfirmRoutes } from './confirm.routing';

@NgModule({
  imports: [
      CommonModule,
      ConfirmRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
  ],
  declarations: [
      ConfirmComponent
  ],
    providers: [
        UserService,
        MessageService,
        ErrorsService
    ]
})
export class ConfirmModule {}
