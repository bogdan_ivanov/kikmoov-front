import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { PwsLandingComponent } from './pws-landing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatSelectModule } from '@angular/material';
import { PwsLandingRoutes } from './pws-landing.routing';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  imports: [
      CommonModule,
      PwsLandingRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      FormsModule,
      ReactiveFormsModule,
      MatSelectModule,
      MatFormFieldModule,
      SlickCarouselModule
  ],
  declarations: [
      PwsLandingComponent,
  ]
})

export class PwsLandingModule {}
