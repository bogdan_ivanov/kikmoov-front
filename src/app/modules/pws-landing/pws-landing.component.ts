import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-pws-landing',
  templateUrl: './pws-landing.component.html',
  styleUrls: ['./pws-landing.component.scss']
})
export class PwsLandingComponent implements OnInit {
  public slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    speed: 1000,
    arrows: true,
    autoplaySpeed: 4000
  };

  public pwsSplash = {
    title: 'Pay with Skills',
    desc: 'Exchange your skills to work in a diverse range of exciting workspaces for free.'
  };

  public howItWorksColumns = [
    {
      icon: 'assets/img/pws-page/sign-up-icn.png',
      title: 'Sign up',
      desc: 'Sign up for a free office space by telling us what you skills you can offer',
    },
    {
      icon: 'assets/img/pws-page/move-in-icn.png',
      title: 'Move in',
      desc: 'We will connect you with a suitable workspace where you can expand your skills and grow your network',
    },
    {
      icon: 'assets/img/pws-page/work-icn.png',
      title: 'Work',
      desc: 'You split your time working with your host, and on your own projects',
    },
  ];

  public pwsTestimonial = [
    {
      photo: 'assets/img/pws-page/alex.png',
      desc: '“Pay with skills has been great for me, it’s the best thing in the world”',
      name: 'Alex',
      position: 'Cobidol',
    },
    {
      photo: 'assets/img/pws-page/timo.png',
      desc: '“Pay with skills has been great for me, it’s the best thing in the world”',
      name: 'Timo',
      position: 'Global Real Estate',
    },
    {
      photo: 'assets/img/pws-page/mustafa.png',
      desc: '“Pay with skills has been great for me, it’s the best thing in the world”',
      name: 'Mustafa',
      position: 'Digital Freelance',
    },
    {
      photo: 'assets/img/pws-page/rose.png',
      desc: '“Pay with skills has been great for me, it’s the best thing in the world”',
      name: 'Rose',
      position: 'UI/UX Designer',
    },
  ];

  public pwsBenefits = [
    {
      number: '1',
      desc: 'Surround yourself with other professionals and businesses',
    },
    {
      number: '2',
      desc: 'Work from a productive and collaborative environment',
    },
    {
      number: '3',
      desc: `Don't pay for an office, make huge savings every month`,
    },
    {
      number: '4',
      desc: 'Find new clients and grow your business',
    },
  ];

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) {}

  ngOnInit() {}

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }
}
