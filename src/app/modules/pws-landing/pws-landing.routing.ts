import { Routes, RouterModule } from '@angular/router';
import {PwsLandingComponent} from './pws-landing.component';

const routes: Routes = [
  {
    path: '',
    component: PwsLandingComponent,
  },
];

export const PwsLandingRoutes = RouterModule.forChild(routes);
