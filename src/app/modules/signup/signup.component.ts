import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from "angularx-social-login";
import {OauthService} from "../../services/oauth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  @Output()
  action: EventEmitter<any> = new EventEmitter();
  public errMsg = '';
  public success = false;
  public userType = '';

  constructor(private userServise: UserService,
              public fb: FormBuilder,
              private socialAuthService: AuthService,
              private oauthService: OauthService) {}

  public signupFormGroup = this.fb.group({
    firstname: [null, [Validators.required]],
    lastname: [null],
    email: [null, [Validators.required, Validators.email]],
    password: [null, [Validators.required, Validators.minLength(8)]],
    passwordCnfrm: [null, [Validators.required]]
  });

  ngOnInit() {}

  onDoLogin() {
    this.errMsg = '';
    if (this.signupFormGroup.status === 'VALID') {
      if (
        this.signupFormGroup.value.password ===
        this.signupFormGroup.value.passwordCnfrm
      ) {
        this.userServise.registerBuyer(this.signupFormGroup.value).subscribe(
          resp => {
            this.signupFormGroup.reset();
            this.success = true;
            setTimeout(() => {
              this.action.emit('close');
            }, 5000);
          },
          err => {
            this.errMsg = err.message;
          }
        );
      } else {
        this.errMsg = 'Confirmation password mismatch';
      }
    } else {
      this.showErrors(this.signupFormGroup);
    }
  }

  socialSignIn(e, socialPlatform: string) {
    e.preventDefault();
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService
      .signIn(socialPlatformProvider)
      .then(userData => {
        if (userData) {
          const provider =
            socialPlatformProvider.toLowerCase() === 'facebook'
              ? 'fb'
              : 'google';
          this.oauthService.doSocialAuth(userData, provider).subscribe(res => {
            if (res) {
              this.oauthService.authenticate(res, this.action);
            }
          });
        }
      })
      .catch(err => console.log('AuthError: ', err));
  }

  showErrors(form) {
    Object.keys(form.controls).forEach(controlName => {
      form.get(controlName).markAsTouched();
    });
    return;
  }
}
