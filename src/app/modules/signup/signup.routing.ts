import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup.component';

const routes: Routes = [
  {
    path: '',
    component: SignupComponent,
    data: {
      meta: {
        title: 'signup.title',
        description: 'signup.text',
        override: true,
      },
    },
  },
];

export const SignupRoutes = RouterModule.forChild(routes);
