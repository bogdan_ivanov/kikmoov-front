import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { SignupRoutes } from './signup.routing';
import { UserService } from '../../services/user.service';
import { AuthService } from 'angularx-social-login';
import { OauthService } from '../../services/oauth.service';
import { SignupComponent } from './signup.component';
import {MatCardModule, MatFormFieldModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      SignupRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule.forRoot(),
      MatCardModule,
      FormsModule,
      ReactiveFormsModule,
      MatFormFieldModule
  ],
  declarations: [
      SignupComponent
  ],
    providers: [
        UserService,
        AuthService,
        OauthService
    ]
})
export class SignupModule {}
