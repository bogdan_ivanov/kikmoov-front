import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { MessageService } from '../../services/message.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { PortalSidebarModule } from '@shared/layouts/portal-sidebar/portal-sidebar.module';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainportalNotificationsRoutes } from './mainportal-notifications.routing';
import { MainportalNotificationsComponent } from './mainportal-notifications.component';
import { NotificationService } from '../../services/notification.service';
import { KmCheckboxModule } from '@shared/layouts/km-checkbox/km-checkbox.module';

@NgModule({
  imports: [
      CommonModule,
      MainportalNotificationsRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule,
      PortalSidebarModule,
      PostSpaceBarModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule,
      KmCheckboxModule
  ],
  declarations: [
      MainportalNotificationsComponent
  ],
    providers: [
        NotificationService,
        MessageService
    ]
})
export class MainportalNotificationsModule {}
