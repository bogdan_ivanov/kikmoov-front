import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainportalNotificationsComponent } from './mainportal-notifications.component';

describe('MainportalNotificationsComponent', () => {
  let component: MainportalNotificationsComponent;
  let fixture: ComponentFixture<MainportalNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainportalNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainportalNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
