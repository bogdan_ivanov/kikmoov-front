import { Routes, RouterModule } from '@angular/router';
import { MainportalNotificationsComponent } from './mainportal-notifications.component';
import { SellerGuard } from '../../middleware/is-seller.guard';

const routes: Routes = [
  {
    path: '',
    component: MainportalNotificationsComponent,
    canActivate: [SellerGuard],
    data: {
      meta: {
        title: 'mainportalNotifications.title',
        description: 'mainportalNotifications.text',
        override: true,
      },
    },
  },
];

export const MainportalNotificationsRoutes = RouterModule.forChild(routes);
