import {Component, Inject, OnInit, Output, PLATFORM_ID} from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import * as moment from 'moment';
import { MessageService } from '../../services/message.service';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-mainportal-notifications',
  templateUrl: './mainportal-notifications.component.html',
  styleUrls: ['./mainportal-notifications.component.scss']
})
export class MainportalNotificationsComponent implements OnInit {
  public notificationCount$: Observable<number>;

  workspacelist;
  currentId;

  public notifications: any[] = [];
  private notificationCount: Observer<number>;
  private checkedNotifications = [];
  private disableActions = false;

  public isBrowser;

  constructor(private notificationService: NotificationService,
              private messageService: MessageService,
              @Inject(PLATFORM_ID) private platformId
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
      this.notificationCount$ = new Observable(observer => {
          this.notificationCount = observer;
      });
  }

  ngOnInit() {
    if (this.isBrowser) {
        this.notificationService.getInternalNotifications().subscribe(res => {
            this.notificationCount.next(res['notifications'].filter(item => item.status.includes('REQUESTED')).length);
            this.notifications = res['notifications'];
            this.notifications.map(notification => {
                notification.tableDate = moment.unix(notification.date)
                    .format('MMM D');
                notification.type = 'bookingId' in notification ? 'booking' : 'viewing';
                notification.notificationId = 'bookingId' in notification ? notification.bookingId : notification.viewingId;
                return notification;
            });
        });
    }
  }

  handleNotificationCheck(event) {
    const id = +event.id.replace('booking', '').replace('viewing', '');
    if (event.val) {
      this.checkedNotifications.push(id);
    } else {
      this.checkedNotifications = this.checkedNotifications.filter(item => item !== id);
    }
  }

  deleteNotifications() {
    if (!this.checkedNotifications.length || this.disableActions) {
      return;
    }
    this.disableActions = true;
    this.notificationService.deleteInternalNotifications(this.checkedNotifications).subscribe(res => {
      this.notifications = this.notifications.filter(item => !this.checkedNotifications.includes(item.id));
      this.messageService.showMessage('Notifications was deleted successfully');
      this.checkedNotifications = [];
      this.disableActions = false;
      this.notificationCount.next(this.notifications.filter(item => item.status.includes('REQUESTED')).length);
    }, err => {
      console.log(err);
      this.disableActions = false;
    });
  }

  acceptNotification(data) {
    if (this.disableActions) {
      return;
    }
    this.disableActions = true;
    this.notificationService.acceptNotification(data)
      .subscribe((res: any) => {
        this.notifications.map(notification => {
          if ('internalNotification' in res && res['internalNotification'].id === notification.id) {
            notification.message = res['internalNotification'].message;
            notification.status = `${notification.type.toUpperCase()}_${data.status.toUpperCase()}`;
          } else if ('message' in res) {
            this.messageService.showMessage(res['message']);
          }
          return notification;
        });
        this.disableActions = false;
        this.notificationCount.next(this.notifications.filter(item => item.status.includes('REQUESTED')).length);
      }, err => {
        console.log(err);
        this.disableActions = false;
      });
  }

}
