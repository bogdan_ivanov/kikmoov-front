import { Routes, RouterModule } from '@angular/router';
import { RecoverPasswordComponent } from './recover-password.component';

const routes: Routes = [
  {
    path: '',
    component: RecoverPasswordComponent,
    data: {
      meta: {
        title: 'recover.title',
        description: 'recover.text',
        override: true,
      },
    },
  },
];

export const RecoverPasswordRoutes = RouterModule.forChild(routes);
