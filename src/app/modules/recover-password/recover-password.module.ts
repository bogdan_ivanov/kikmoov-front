import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { UserService } from '../../services/user.service';
import { RecoverPasswordRoutes } from './recover-password.routing';
import { RecoverPasswordComponent } from './recover-password.component';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';
import { MatCardModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      RecoverPasswordRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      MatCardModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      RecoverPasswordComponent
  ],
    providers: [
        UserService,
        MessageService,
        ErrorsService
    ]
})
export class RecoverPasswordModule {}
