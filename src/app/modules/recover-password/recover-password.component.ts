import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MessageService } from './../../services/message.service';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorsService } from './../../services/errors.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  public errMsg = '';
  public pin = '';

  constructor(
    private userServise: UserService,
    private messageService: MessageService,
    private errorService: ErrorsService,
    public fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      if ('pin' in params) {
        this.pin = params['pin'];
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  public loginFormGroup = this.fb.group({
    password: [null, [Validators.required]]
  });

  ngOnInit() {}

  onDoPassChange(event) {
    this.errMsg = '';
    if (this.loginFormGroup.valid) {
      this.userServise.changePassword(
        this.loginFormGroup.value,
        this.pin
      ).subscribe(
        res => {
          this.messageService.setMessage(res['message']);
          this.messageService.showMessage();
          this.messageService.clearMessage();
          setTimeout(() => {
            this.router.navigate(['/']);
          });
        },
        err => {
          this.errorService.setErrors(err);
          this.errorService.showError();
          this.errorService.clearErrors();
        }
      );
    } else {
      this.errMsg = 'Password fields are blank';
    }
  }
}
