import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { UserService } from '../../services/user.service';
import {MatCardModule, MatFormFieldModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupsellerRoutes } from './signupseller.routing';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { SignUpSellerComponent } from './signupseller.component';
import { ForgotPasswordModule } from '@shared/layouts/forgot-password/forgot-password.module';
import {LogindialogModule} from '@shared/layouts/logindialog/logindialog.module';

@NgModule({
  imports: [
      CommonModule,
      SignupsellerRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      PostSpaceBarModule,
      ForgotPasswordModule,
      LogindialogModule,
      NgxSmartModalModule.forRoot(),
      MatCardModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule,
  ],
  declarations: [
      SignUpSellerComponent
  ],
    providers: [
        UserService
    ]
})
export class SignupsellerModule {}
