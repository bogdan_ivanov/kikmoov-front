import { Routes, RouterModule } from '@angular/router';
import { SignUpSellerComponent } from './signupseller.component';

const routes: Routes = [
  {
    path: '',
    component: SignUpSellerComponent,
    data: {
      meta: {
        title: 'signupSeller.title',
        description: 'signupSeller.text',
        override: true,
      },
    },
  },
];

export const SignupsellerRoutes = RouterModule.forChild(routes);
