import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signupseller',
  templateUrl: './signupseller.component.html',
  styleUrls: ['./signupseller.component.scss'],
})
export class SignUpSellerComponent implements OnInit {
  public errMsg = '';
  private phonePattern = '^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$';
  public signupFormGroup = this.fb.group({
    firstname: [null, [Validators.required]],
    lastname: [null],
    email: [null, [Validators.required, Validators.email]],
    password: [null, [Validators.required, Validators.minLength(8)]],
    rePassword: [null, [Validators.required]],
    phone: [null, [Validators.required, Validators.pattern(new RegExp(this.phonePattern, 'gm'))]],
  });

  constructor(
    private userServise: UserService,
    private router: Router,
    public fb: FormBuilder
  ) {}

  ngOnInit() {}

  signupSeller(form) {
    this.errMsg = '';
    if (form.valid) {
      if (
        form.value.password ===
        form.value.rePassword
      ) {
        this.userServise.setUserData(form.value);
        this.userServise.canSellerRegister(form.value.email).subscribe(
          can => {
            if (can) {
              this.router.navigate(['invoicing'], {
                queryParams: {
                  email: form.value.email
                },
              });
            } else {
              throw(can);
            }
          },
          err => {
            if (err.message == 'Email already exist') {
              this.router.navigate(['invoicing'], {
                queryParams: {
                  email: form.value['email'],
                },
              });
            }
            this.errMsg = `User with email ${form.value['email']} already exist`;
          }
        );
      } else {
        this.errMsg = 'Confirmation password mismatch';
      }
    } else {
      Object.keys(form['controls']).forEach(control => {
        form.get(control).markAsTouched();
      });
    }
  }
}
