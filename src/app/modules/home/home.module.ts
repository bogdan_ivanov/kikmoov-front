import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { HomeRoutes } from './home.routing';
import { HomeComponent } from './home.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { HomeTopPicksComponent } from './home-top-picks/home-top-picks.component';
import { HomeSearchComponent } from './home-search/home-search.component';
import { WorkspaceModule } from '@shared/layouts/workspace/workspace.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContentService } from '../../services/content.service';
import { BlogService } from '../../services/blog.service';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';
import { FilterService } from '../../services/filter.service';
import { LocationService } from '../../services/location.service';
import { HomePageResolver } from '../../resolvers/homePageResolver';

@NgModule({
  imports: [
      CommonModule,
      HomeRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule,
      SlickCarouselModule,
      WorkspaceModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      HomeComponent,
      HomeTopPicksComponent,
      HomeSearchComponent
  ],
    providers: [
        ContentService,
        BlogService,
        MessageService,
        ErrorsService,
        FilterService,
        LocationService,
        HomePageResolver
    ]
})
export class HomeModule {}
