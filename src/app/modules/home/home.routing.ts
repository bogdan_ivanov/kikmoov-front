import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { HomePageResolver } from '../../resolvers/homePageResolver';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      meta: {
        title: 'home.title',
        description: 'home.text',
        override: true,
      },
    },
    resolve: {
        data: HomePageResolver
    }
  },
];

export const HomeRoutes = RouterModule.forChild(routes);
