import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import { ErrorsService } from '../../services/errors.service';
import { MessageService } from '../../services/message.service';
import { ContentService } from '../../services/content.service';
import { BlogService } from '../../services/blog.service';
import { MetaService } from '@ngx-meta/core';
import { ActivatedRoute } from '@angular/router';
import {isPlatformBrowser} from '@angular/common';
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  afterChange(e) {
    const slide = document.querySelectorAll(`.home-page-testimonials div[data-slick-index="${e.currentSlide}"]`);
    const animContainer = document.getElementById('animation-block');

    if (slide[0].classList.contains('slick-active')) {
      animContainer.classList.add(`animation_${e.currentSlide}`);
    } else {
      animContainer.classList.remove('first-animation');
    }
  }

  beforeChange(e) {
    const slide = document.querySelectorAll(`.home-page-testimonials div[data-slick-index="${e.currentSlide}"]`);
    const animContainer = document.getElementById('animation-block');

    if (slide[0].classList.contains('slick-active')) {
      animContainer.classList.remove(`animation_${e.currentSlide}`);
    }
  }

  public slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    fade: true,
    arrows: false,
    autoplaySpeed: 4000,
  };
  public testimonialsSliderConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    speed: 0,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          adaptiveHeight: true,
          dots: true,
        }
      },
    ]
  };
  public newsPreview = {
    img: 'assets/img/baf.jpg',
    name: 'News room',
    title: 'Africa Is The Future',
    desc: 'It would be easy to underestimate the impact of global megatrends on Africa. After all, the continent\'s real estate markets have traditionally lingered behind developed and…',
  };
  public infoList = [
    {
      img: 'assets/img/office-guides.jpg',
      name: 'Office guides'
    },
    {
      img: 'assets/img/area-guides.jpg',
      name: 'Area guides'
    },
    {
      img: 'assets/img/market-rent-guides.jpg',
      name: 'Market rent guides'
    },
    {
      img: 'assets/img/case-study.jpg',
      name: 'Case studies'
    },
  ];
  public homePageTestimonial = [
    {
      photo: 'assets/img/homepage-testimonials/david.png',
      desc: '“We desperately needed to find a suitable work space. Using kikmoov we booked a co-working space in the city. Kikmoov helped me throughout the whole process, it was a great service. I have also used Kikmoov to book hourly spaces for the Paris team when they commute to London. I always save valuable time using kikmoov.”',
      name: 'David',
      position: 'Creme De La Creme',
    },
    {
      photo: 'assets/img/homepage-testimonials/adam.png',
      desc: '“Running a tech company means to scale up quickly when needed. Kikmoov has given us the flexibility to find and book short term space for part-time team members. Kikmoov’s real estate professionals have great market knowledge and are always here to help. If you want great flexible work space solutions look no further.”',
      name: 'Adam',
      position: 'Velocity Black',
    },
    {
      photo: 'assets/img/homepage-testimonials/elianna.png',
      desc: '“Working for time out, I am constantly on the go, working form different cities all over the world. Short term, flexible workspace is a must. Kikmoov has helped my team and I find great spaces in London. It is a simple, easy platform and the team is great!”',
      name: 'Elianna',
      position: 'Time Out',
    },
    {
      photo: 'assets/img/homepage-testimonials/jx.png',
      desc: '“Kikmoov was a lifesaver when I travelled to London from Singapore and needed a place for my team and I to work.”',
      name: 'J.X. Paulin',
      position: 'DBX',
    },
  ];
  public slides = [
    {
      img: 'assets/img/home-cover-1.jpg',
      title: 'Tech City',
      uri: '/blog/community-market/tech-city',
      description: 'Where innovation happens and where creators come to life.'
    },
    {
      img: 'assets/img/home-cover-2.jpg',
      title: 'The City Square Mile',
      uri: '/blog/community-market/city-square-mile',
      description: 'The hustle and bustle of the prestigious financial district.'
    },
    {
      img: 'assets/img/home-cover-3.jpg',
      title: 'East London',
      uri: '/blog/community-market/east-london',
      description: 'Gentrified London with growth and opportunity.'
    },
    {
      img: 'assets/img/home-cover-4.jpg',
      title: 'Midtown',
      uri: '/blog/community-market/midtown',
      description: 'Perfectly situated, moments from the West End and in close proximity to the City.'
    },
    {
      img: 'assets/img/home-cover-5.jpg',
      title: 'North London',
      uri: '/blog/community-market/north-london',
      description: 'A green part of London which have seen tremendous amount of growth in certain areas.'
    },
    {
      img: 'assets/img/home-cover-1.jpg',
      title: 'South London',
      uri: '/blog/community-market/south-london',
      description: 'Home to arguably the most iconic building in London, The Shard.'
    },
    {
      img: 'assets/img/home-cover-2.jpg',
      title: 'West London',
      uri: '/blog/community-market/west-london',
      description: 'A diverse area of London with every kind of business.'
    },
    {
      img: 'assets/img/home-cover-3.jpg',
      title: 'West End',
      uri: '/blog/community-market/west-end',
      description: 'The heart of London and a popular melting pot of offices and industries.'
    },
  ];
  public reviewSlides = [
    {
      img: 'assets/img/review-jx.png',
      author: 'J.X. Paulin',
      company: 'DBX',
      position: 'CEO',
      review:
        '“Kikmoov was a lifesaver when I travelled to London from Singapore and needed a place for my team and I to work.”',
    },
    {
      img: 'assets/img/review-david.png',
      author: 'David Odier',
      company: 'Creme de la creme',
      position: 'Director',
      review:
        '“We desperately needed to find a suitable work space. Using kikmoov we booked a co-working space in the city. Kikmoov helped me throughout the whole process, it was a great service. I have also used Kikmoov to book hourly spaces for the Paris team when they commute to London. I always save valuable time using kikmoov.”',
    },
    {
      img: 'assets/img/review-adam.png',
      author: 'Adam Williams',
      company: 'Velocity Black',
      position: 'Director',
      review:
        '“Running a tech company means to scale up quickly when needed. Kikmoov has given us the flexibility to find and book short term space for part-time team members. Kikmoov’s real estate professionals have great market knowledge and are always here to help. If you want great flexible work space solutions look no further.”',
    },
    {
      img: 'assets/img/review-elianna.png',
      author: 'Elianna Bar-El',
      company: 'Time Out',
      position: 'Editor',
      review:
        '“Working for time out, I am constantly on the go, working form different cities all over the world. Short term, flexible workspace is a must. Kikmoov has helped my team and I find great spaces in London. It is a simple, easy platform and the team is great!”',
    },
  ];
  public pthImgList = [
    {
      bg: '/assets/img/dbx.jpg',
      img: '/assets/img/ptn-dbx.svg',
    },
    {
      bg: '/assets/img/creme-de-la-creme.jpg',
      img: '/assets/img/ptn-creme-de-la-creme.svg',
    },
    {
      bg: '/assets/img/event.jpg',
      img: '/assets/img/ptn-timeout.svg',
    },
    {
      bg: '/assets/img/velocity-black.jpg',
      img: '/assets/img/ptn-velocity-black.svg',
    },
  ];
  public howItWorks = [
    {
      bg: '/assets/img/find-image.jpg',
      title: 'Find',
      description:
        'Find your perfect workspace. Search and compare kikmoov’s private offices, desks and meeting rooms.',
    },
    {
      bg: '/assets/img/book-image.jpg',
      title: 'Book',
      description:
        'Book viewings for flexible office spaces. Instantly book and pay for meeting rooms and hot desks on an hourly basis, all through our app and website.',
    },
    {
      bg: '/assets/img/create-image.jpg',
      title: 'Get to work',
      description:
        'Follow your passion and do what you love in an office you love. Kikmoov help with legals and paperwork.',
    },
  ];
  public articleSlideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true
        },
      },
    ],
  };
  public postSlideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    autoplay: true,
    adaptiveHeight: true,
    arrows: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: true
        },
      },
    ],
  };
  public testimonials = [];
  public blogArticles = [];
  public isBrowser;
  private intercomContainer;
  public blogCategories;
  public lastArticleInfo;
  public categories;
  public lastArticle;

  constructor(private errorsService: ErrorsService,
              private messageService: MessageService,
              private contentService: ContentService,
              private blogService: BlogService,
              @Inject('ORIGIN_URL') public baseUrl: string,
              private readonly meta: MetaService,
              private route: ActivatedRoute,
              @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.testimonials = this.route.snapshot.data.data.testimonials.testimonials;

    if (this.route.snapshot.data.data.blogArticles.articles) {
      const resultBlogArticles = this.route.snapshot.data.data.blogArticles;
      if (resultBlogArticles.articles.length > 4) {
        this.blogArticles = resultBlogArticles.articles.splice(resultBlogArticles.articles.length - 5, 4);
      } else {
        this.blogArticles = resultBlogArticles.articles;
      }
      this.blogArticles = this.blogArticles.map((article, index) => {
        if (this.slides[index]) {
          article.coverImageUrl = this.slides[index].img;
        }
        return article;
      });
    }
    this.blogCategories = this.blogService.getCategories();
    this.lastArticle = this.blogService.getLastArticle();
  }

  public getAbsoluteUrl(path: string) {
    return this.baseUrl + path;
  }

  ngOnInit() {
    // this.meta.setTitle('Home1 Page', true);
    // this.meta.setTag('title', 'Home dynamic Page');
    // this.meta.setTag('description', 'Home description');
    if (this.messageService.hasMessage()) {
      this.messageService.showMessage();
      this.messageService.clearMessage();
    }
    if (this.errorsService.hasErrors()) {
      this.errorsService.showError();
      this.errorsService.clearErrors();
    }

    forkJoin(
      this.blogCategories,
      this.lastArticle
    ).subscribe((data) => {
      this.categories = data[0]['categories'];
      this.lastArticleInfo = data[1];

      if (this.categories && this.lastArticleInfo) {
        this.categories.forEach((item, index) => {
          if (item.slug == this.lastArticleInfo.category_slug) {
            this.categories.splice(index, 1);
          }
        });
      }
    });
  }

  setBackgroundImage(src: string, catIndex: number = 0) {
    let image = document.createElement('img');
    image.src = src;

    return (!image.complete || image.height === 0) ? this.infoList[catIndex].img : src;
  }
}
