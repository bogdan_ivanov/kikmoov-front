import {AfterViewInit, Component, Inject, NgZone, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import { FilterService } from '../../../services/filter.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-home-search',
  templateUrl: './home-search.component.html',
  styleUrls: ['./home-search.component.scss']
})
export class HomeSearchComponent implements OnInit, AfterViewInit {
  @ViewChild('searchInput')
  searchInput;

  public durationTypes = [
    {
      name: 'Hourly',
      value: 'hourly',
    },
    {
      name: 'Monthly',
      value: 'monthly'
    }
  ];
  public duratonModel = {
    type: 'monthly',
    hourly: {
      offices: [
        {
          type: 'meeting-room',
          name: 'Meeting space'
        },
        {
          type: 'desk',
          name: 'Desk'
        },
      ]
    },
    monthly: {
      offices: [
        {
          type: 'desk',
          name: 'Desk'
        },
        {
          type: 'private-office',
          name: 'Private Office'
        }
      ]
    }
  };
  public officeModel = {type: null, name: null};
  public addressModel = '';
  public address = '';
  public station = '';
  public borough = '';
  public area = '';

  public suggestionList: {value: string, type: string}[] = [];

  private pendingRequest: Subscription;
  private isBrowser;

  constructor(private filterService: FilterService,
              private router: Router,
              @Inject(PLATFORM_ID) private platformId,
              @Inject('ORIGIN_URL') public baseUrl: string
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit() {
  }

  public getAbsoluteUrl(path: string) {
      return this.baseUrl + path;
  }

  ngAfterViewInit() {
    if (this.isBrowser) {
        this.searchInput.update
            .pipe(
                debounceTime(400),
                distinctUntilChanged()
            )
            .subscribe(this.getSuggestions.bind(this));
    }
  }

  applySearch() {
    const data = this.prepareFilters();
    this.router.navigate(['findaspace'], {queryParams: data});
  }

  getSuggestions(address) {
    if (!address) {
      this.suggestionList = [];
      return;
    }
    this['address$'] = address;
    this.pendingRequest = this.filterService.getSuggestions(address)
        .pipe(
            distinctUntilChanged()
        )
      .subscribe(res => {
        this.suggestionList = <{value: string, type: string}[]>res;
      });
  }

  submitSuggestion(address, type) {
    this.address = '';
    this.borough = '';
    this.area = '';
    this.station = '';
    this[type] = address;
    this.addressModel = address;
    this.suggestionList = [];
  }

  handleSuggestion(suggestion) {
    const type = `<span class="search-hint">${suggestion.type}</span>`;
    suggestion = suggestion.value;
    const addressMatch = suggestion.match(new RegExp(this.addressModel, 'ig'));
    return '<div class="left-col-wrap">' + suggestion.replace(
      addressMatch,
      `<strong>${addressMatch}</strong>`
    ) + '</div>' + type;
  }

  resolveSuggestion(suggestion) {
      return '<div class="d-flex">' + this.handleSuggestion(suggestion) + '</div>';
  }

  prepareFilters() {
    const result =  {
      duration: this.duratonModel.type,
      type: this.officeModel.type
    };
    if (this.address) {
      result['address'] = this.address;
    } else if (this.station) {
      result['station'] = this.station;
    } else if (this.borough) {
      result['borough'] = this.borough;
    } else if (this.area) {
      result['area'] = this.area;
    } else if (this.addressModel) {
        result['address'] = this.addressModel;
    }
    return result;
  }

  resetOffice() {
    this.officeModel = {type: null, name: null};
  }
}
