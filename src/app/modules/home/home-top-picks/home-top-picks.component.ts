import { Component, OnInit } from '@angular/core';
import { LocationService } from '../../../services/location.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-top-picks',
  templateUrl: './home-top-picks.component.html',
  styleUrls: ['./home-top-picks.component.scss']
})
export class HomeTopPicksComponent implements OnInit {
  public officeSlideConfig = {
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: true,
    infinite: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          arrows: false
        },
      },
    ],
  };
  public slides = [
    { img: 'assets/img/home-cover-1.jpg' },
    { img: 'assets/img/home-cover-2.jpg' },
    { img: 'assets/img/home-cover-3.jpg' },
    { img: 'assets/img/home-cover-4.jpg' },
  ];
  public workSpaceTypes = {
    'private-office': 'Private Office',
    'meeting-room': 'Meeting Space',
    'desk': 'Desk'
  };
  public deskTypeNames = {
    hourly_hot_desk: 'Hot Desk',
    monthly_hot_desk: 'Desk',
    monthly_fixed_desk: 'Fixed Desk'
  };
  public workSpaceList = [];

  constructor(private router: Router,
              private locationService: LocationService,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.workSpaceList = this.route.snapshot.data.data.topWorkspaces.results;
  }

}
