import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTopPicksComponent } from './home-top-picks.component';

describe('HomeTopPicksComponent', () => {
  let component: HomeTopPicksComponent;
  let fixture: ComponentFixture<HomeTopPicksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTopPicksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTopPicksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
