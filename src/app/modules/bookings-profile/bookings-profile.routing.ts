import { Routes, RouterModule } from '@angular/router';
import { BookingsProfileComponent } from './bookings-profile.component';

const routes: Routes = [
  {
    path: '',
    component: BookingsProfileComponent,
    data: {
      meta: {
        title: 'profileBookings.title',
        description: 'profileBookings.text',
        override: true,
      },
    },
  },
];

export const BookingsProfileRoutes = RouterModule.forChild(routes);
