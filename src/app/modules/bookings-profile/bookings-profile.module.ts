import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { MessageService } from '../../services/message.service';
import { ProfileService } from '../../services/profile.service';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorsService } from '../../services/errors.service';
import { BookingsProfileComponent } from './bookings-profile.component';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import { BookingsProfileRoutes } from './bookings-profile.routing';

@NgModule({
  imports: [
      CommonModule,
      BookingsProfileRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      MatCardModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule,
      NgxSmartModalModule.forRoot()
  ],
  declarations: [
      BookingsProfileComponent
  ],
    providers: [
        ProfileService,
        MessageService,
        ErrorsService,
        NgxSmartModalService
    ]
})
export class BookingsProfileModule {}
