import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as moment from 'moment';
import { ProfileService } from '../../services/profile.service';
import { Observable } from 'rxjs/Observable';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';

@Component({
  selector: 'app-bookings-profile',
  templateUrl: './bookings-profile.component.html',
  styleUrls: ['./bookings-profile.component.scss']
})
export class BookingsProfileComponent implements OnInit {
  public futureBookings = [];
  public pastBookings = [];
  public workSpaceTypes = {
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office',
    desk: 'Desk',
  };
  public deskTypes = {
    hourly_hot_desk: 'Hot Desk',
    monthly_hot_desk: 'Desk',
    monthly_fixed_desk: 'Fixed Desk',
    daily_hot_desk: 'Daily Desk'
  };
  public cancelingBooking;

  public loading = true;

  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private profileService: ProfileService,
    private messageService: MessageService,
    private errorsService: ErrorsService
  ) {
    moment.locale('en');
  }

  ngOnInit() {
    this.profileService.getBookingRequests().subscribe((res: any[]) => {
      res = res.map(item => {
        if (item.hasOwnProperty('viewing')) {
          item['booking'] = item['viewing'];
          item['booking']['viewing'] = true;
        } else {
          item['booking']['viewing'] = false;
        }
        const start = moment(item['booking'].startTime * 1000);
        const end = item['booking'].endTime ? moment(item['booking'].endTime * 1000) : null;
        // const formattedEnd = end && end.unix() !== start.unix() ? ` - ${end.format('HH:ssa')}` : '';
        const formattedEnd = end && end.unix() !== start.unix() ? ` - ${end.format('LT')}` : '';
        const formattedStart = (start && !item['booking']['viewing'] &&
                                item['workspace'].type !== 'private-office' &&
                                item['workspace'].deskType !== 'monthly_hot_desk' &&
                                item['workspace'].deskType !== 'monthly_fixed_desk') ||
                               (start && item['booking']['viewing']) ?
          // start.format(`HH:ssa$0,`).replace('$0', formattedEnd) : '';
          start.format(`LT`) + formattedEnd : '';
        item['booking']['date'] = formattedStart + start.format(' D MMMM YYYY');
        return item;
      });
      this.futureBookings = res.filter(item => item.booking.future && item.booking.status !== 'canceled');
      this.pastBookings = res.filter(item => !item.booking.future && item.booking.status !== 'canceled');
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
      this.errorsService.showError(err);
    });
  }

  bookingCancelModalOpen(booking) {
    this.cancelingBooking = booking;
    this.ngxSmartModalService.getModal('bookingCancelModal').open();
  }

  cancelBooking() {
    const cancelType = !this.cancelingBooking['booking'].viewing ? 'cancelBookingRequest' : 'cancelViewingRequest';
    (<Observable<any>>this.profileService[cancelType](this.cancelingBooking['booking'].id)).subscribe(
      res => {
        const type = this.cancelingBooking['booking'].viewing ? 'Viewing' : 'Booking';
        this.messageService.setMessage(`${type} was cancelled successfully`);
        this.messageService.showMessage();
        this.ngxSmartModalService.getModal('bookingCancelModal').close();
        this.futureBookings = this.futureBookings.filter(item => item.booking.id !== this.cancelingBooking['booking'].id);
      }, err => {
        console.log(err);
        this.errorsService.showError(err);
      }
    );
  }
}
