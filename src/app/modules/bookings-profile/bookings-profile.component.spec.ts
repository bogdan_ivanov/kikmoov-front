import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingsProfileComponent } from './bookings-profile.component';

describe('BookingsProfileComponent', () => {
  let component: BookingsProfileComponent;
  let fixture: ComponentFixture<BookingsProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingsProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
