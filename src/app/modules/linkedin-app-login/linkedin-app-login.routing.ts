import { LinkedinAppLoginComponent } from './linkedin-app-login.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: LinkedinAppLoginComponent,
    data: {
      meta: {
        title: 'linkedinApiLogin.title',
        description: 'linkedinApiLogin.text',
      },
    },
  },
];

export const LinkedinAppLoginRoutes = RouterModule.forChild(routes);
