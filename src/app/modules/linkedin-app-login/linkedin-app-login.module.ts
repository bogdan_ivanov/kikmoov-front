import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { LinkedinAppLoginRoutes } from './linkedin-app-login.routing';
import { LinkedinAppLoginComponent } from './linkedin-app-login.component';

@NgModule({
  imports: [CommonModule, LinkedinAppLoginRoutes, TranslateModule],
  declarations: [LinkedinAppLoginComponent],
})
export class LinkedinAppLoginModule {}
