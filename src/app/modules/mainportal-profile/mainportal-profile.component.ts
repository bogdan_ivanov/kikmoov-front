import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';
import { UserService } from '../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-mainportal-profile',
  templateUrl: './mainportal-profile.component.html',
  styleUrls: ['./mainportal-profile.component.scss']
})
export class MainportalProfileComponent implements OnInit {
  workspacelist;
  currentId;
  mode;
  loading = true;
  public profileForm;
  public changePasswordForm;
  private phonePattern = '^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$';
  constructor(private route: ActivatedRoute,
              private profileService: ProfileService,
              private messageService: MessageService,
              private errorService: ErrorsService,
              private userService: UserService,
              private fb: FormBuilder,
              private router: Router,
              public ngxSmartModalService: NgxSmartModalService) {
    // this.mode = this.route.snapshot.url.find(segment => segment.path === 'change-password') ? 'change-password' : 'profile';
    const currentUrl = this.router.routerState.snapshot.url;
    if (currentUrl === '/mainportal/profile/change-password') {
      this.mode = 'change-password';
    } else {
      this.mode = 'profile';
    }

    this.profileForm = this.fb.group({
      firstname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern(new RegExp(this.phonePattern, 'gm'))]],
      companyName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      optionalAddress: [''],
      town: ['', [Validators.required]],
      postcode: ['', [Validators.required]],
      invoiceEmail: ['', [Validators.required, Validators.email]]
    });
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      repeatNewPassword: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.profileService.getSellerDetails().subscribe(res => {
      this.profileForm.patchValue(res);
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

  submitProfile(form) {
    if (form.invalid) {
      for (const controlName of Object.keys(form.controls)) {
        form.controls[controlName].markAsTouched();
      }
      return;
    }
    this.profileService.updateSellerDetails(form.value).subscribe(res => {
      form.markAsPristine();
      this.messageService.showMessage(res['message']);
    }, err => {
      console.log(err);
    });
  }

  submitChangePassword(form) {
    if (form.invalid) {
      for (const controlName of Object.keys(form.controls)) {
        form.controls[controlName].markAsTouched();
      }
      return;
    }
    if (form.get('newPassword').value !== form.get('repeatNewPassword').value) {
      form.get('repeatNewPassword').setErrors({'not_match': 'Passwords not match'});
      return;
    }
    this.profileService.changePassword({
      oldPassword: form.get('oldPassword').value,
      newPassword: form.get('newPassword').value
    }).subscribe(res => {
      this.messageService.showMessage(res['message']);
    }, err => {
      this.errorService.setErrors(err);
      this.errorService.showError();
      console.log(err);
    });
  }

  deleteAccount()
  {
      this.profileService.deleteUser().subscribe(
          res => {
              console.log(res);
              this.userService.setLogoutAndRedirect();
          }, err => {
              console.log(err);
          }
      );
  }

  confirmDeleteProfileModal() {
    this.ngxSmartModalService.getModal('confirmDeleteProfileModal').open();
  }

  confirmDeleteProfileModalClose() {
    this.ngxSmartModalService.getModal('confirmDeleteProfileModal').close();
  }

}
