import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainportalProfileComponent } from './mainportal-profile.component';

describe('MainportalProfileComponent', () => {
  let component: MainportalProfileComponent;
  let fixture: ComponentFixture<MainportalProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainportalProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainportalProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
