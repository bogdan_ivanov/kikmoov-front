import { Routes, RouterModule } from '@angular/router';
import { MainportalProfileComponent } from './mainportal-profile.component';
import { SellerGuard } from '../../middleware/is-seller.guard';

const routes: Routes = [
  {
    path: '',
    component: MainportalProfileComponent,
    canActivate: [SellerGuard],
    data: {
      meta: {
        title: 'mainportalProfile.title',
        description: 'mainportalProfile.text',
        override: true,
      },
    },
  },
];

export const MainportalProfileRoutes = RouterModule.forChild(routes);
