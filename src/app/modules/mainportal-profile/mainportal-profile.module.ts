import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { ErrorsService } from '../../services/errors.service';
import { MessageService } from '../../services/message.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { MainportalProfileRoutes } from './mainportal-profile.routing';
import { MainportalProfileComponent } from './mainportal-profile.component';
import { ProfileService } from '../../services/profile.service';
import { PortalSidebarModule } from '@shared/layouts/portal-sidebar/portal-sidebar.module';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../services/user.service';

@NgModule({
  imports: [
      CommonModule,
      MainportalProfileRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule,
      PortalSidebarModule,
      PostSpaceBarModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      MainportalProfileComponent
  ],
    providers: [
        ProfileService,
        ErrorsService,
        MessageService,
        UserService
    ]
})
export class MainportalProfileModule {}
