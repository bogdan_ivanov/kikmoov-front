import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input()
  pagination$;

  _offset: number = 0;

  set offset(page: number) {

    this._offset = page > 1 ? (page * this.pagination$.pageSize - this.pagination$.pageSize) : 0;
  }

  get offset(): number {
    return this._offset;
  }

  @Output()
  onPageChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  setPage(page) {
    this.offset = page;
    this.onPageChange.emit({offset: this.offset, page});
  }

}
