import { NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { WorkspaceModule } from '@shared/layouts/workspace/workspace.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterService } from '../../services/filter.service';
import { LocationService } from '../../services/location.service';
import { FindaspaceRoutes } from './findaspace.routing';
import { FindaspaceComponent } from './findaspace.component';
import { FindaspaceFiltersComponent } from './findaspace-filters/findaspace-filters.component';
import {
    DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import { NguiMapModule } from '@ngui/map';
import { environment } from '../../../environments/environment';
import { PaginationComponent } from './pagination/pagination.component';
import { ProfileService } from '../../services/profile.service';
import { PaginationService } from '../../services/pagination.service';
import { MapSliderModule } from '@shared/layouts/map-slider/map-slider.module';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {Ng5SliderModule} from 'ng5-slider';

const CustomDateFormats = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
  imports: [
      CommonModule,
      FindaspaceRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      NgxSmartModalModule,
      SlickCarouselModule,
      WorkspaceModule,
      FormsModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatProgressSpinnerModule,
      MatExpansionModule,
      MatNativeDateModule,
      MatDatepickerModule,
      MatSelectModule,
      MatSnackBarModule,
      NguiMapModule.forRoot({
          apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.googleMapsKey
      }),
      WorkspaceModule,
      MapSliderModule,
      Ng5SliderModule
  ],
  declarations: [
      FindaspaceComponent,
      FindaspaceFiltersComponent,
      PaginationComponent
  ],
    providers: [
        LocationService,
        ProfileService,
        PaginationService,
        FilterService,
        CurrencyPipe,
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE]
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: CustomDateFormats
        },
    ]
})
export class FindaspaceModule {}
