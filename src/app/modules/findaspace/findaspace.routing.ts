import { Routes, RouterModule } from '@angular/router';
import { FindaspaceComponent } from './findaspace.component';

const routes: Routes = [
  {
    path: '',
    component: FindaspaceComponent,
    data: {
      meta: {
        title: 'findaspace.title',
        description: 'findaspace.text',
        override: true,
      },
    },
  },
];

export const FindaspaceRoutes = RouterModule.forChild(routes);
