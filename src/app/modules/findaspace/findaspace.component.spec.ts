import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindaspaceComponent } from './findaspace.component';

describe('FindaspaceComponent', () => {
  let component: FindaspaceComponent;
  let fixture: ComponentFixture<FindaspaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindaspaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindaspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
