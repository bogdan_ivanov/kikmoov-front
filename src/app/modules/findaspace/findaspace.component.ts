/// <reference types="@types/googlemaps" />
import {Component, Inject, OnInit, Output, PLATFORM_ID, ViewChild} from '@angular/core';
import { LocationService } from '../../services/location.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { ProfileService } from '../../services/profile.service';
import { PaginationService } from '../../services/pagination.service';
import {CurrencyPipe, isPlatformBrowser} from '@angular/common';
import { of } from 'rxjs/observable/of';
import {UniversalStorage} from '@shared/storage/universal.storage';

@Component({
  selector: 'app-findaspace',
  templateUrl: './findaspace.component.html',
  styleUrls: ['./findaspace.component.scss'],
})
export class FindaspaceComponent implements OnInit {
  public loading = true;
  public showChildSlider = false;
  public showMap = true;
  public sortValue: Observable<Object>;
  public sortObserver: Observer<Object>;
  public pagination: Observable<Object>;
  public paginationObserver: Observer<Object>;
  public workSpaces = [];
  public markers = [];
  public varMarks = [];
  public workSpaceCount = 0;
  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office',
  };
  public pagination$;

  public selectedDate = '';

  public currentPage = 1;

  public showDetails;

  // public mapCenter = new google.maps.LatLng(51.489786, -0.080362);
  public mapCenter;

  public googleMap;
  public mapInit;

  private minZoom = 13;

  // in px
  private minimumScreenDesktopSize = 767;

  public isBrowser;

  public sortValues = {
      'newest': 'date added: newest first',
      'oldest': 'date added: oldest first',
      'lowest': 'price: lowest first',
      'highest': 'price: highest first'
  };

@ViewChild('mapSlider') mapSlider;

    public mapSliderConfig = {
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        lazyLoad: 'progressive',
    };

    public deskTypeNames = {
        hourly_hot_desk: 'Hot Desk',
        monthly_hot_desk: 'Desk',
        monthly_fixed_desk: 'Fixed Desk'
    };

  constructor(
    private locationService: LocationService,
    private profileService: ProfileService,
    private paginationService: PaginationService,
    private currencyPipe: CurrencyPipe,
    private universalStorage: UniversalStorage,
    @Inject(PLATFORM_ID) private platformId,
    @Inject('ORIGIN_URL') public baseUrl: string
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.sortValue = new Observable<Object>(observer => {
      this.sortObserver = observer;
    });
    this.pagination = new Observable<Object>(observer => {
      this.paginationObserver = observer;
    });
    if (this.isBrowser) {
        // for mobile screens we should hide map
        if (window.screen.width <= this.minimumScreenDesktopSize) {
            this.showMap = false;
        }
        this.mapCenter = new google.maps.LatLng(51.489786, -0.080362);
    }
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
      if (this.universalStorage.getItem('map_state') !== null) {
          this.showMap = JSON.parse(this.universalStorage.getItem('map_state'));
      }
      if (this.isBrowser) {
          window.scrollTo(0, 0);
          document.addEventListener('mousedown', e => {
              const activeMarker = document.getElementById('details-' + this.showDetails);
              const target = <Element>e.target;
              if (activeMarker && !target.closest('.detail-location') && !target.closest('.map-marker')) {
                  this.showDetails = null;
              }
          });
      }
  }

  onMapReady(map) {
    this.googleMap = map;
    if (!this.mapInit) {
      let lastValidCenter = map.getCenter();
      let bounds = map.getBounds();
      google.maps.event.addListener(map, 'bounds_changed', function() {
        if (!bounds) {
          bounds = map.getBounds();
        }
      });
      google.maps.event.clearListeners(map, 'center_changed');
      google.maps.event.addListener(map, 'center_changed', function() {
        if (bounds.contains(map.getCenter())) {
          lastValidCenter = map.getCenter();
          return;
        }
        map.panTo(lastValidCenter);
      });
      this.mapInit = true;
    }
      this.setBounds();
  }

  setBounds() {
      const map = this.googleMap;
      const markers = this.markers;
      const bounds = new google.maps.LatLngBounds();
      for (let i = 0; i < markers.length; i++) {
          const myLatLng = new google.maps.LatLng(markers[i]['location'].latitude, markers[i]['location'].longitude);
          bounds.extend(myLatLng);
      }
      map.setCenter(bounds.getCenter());
      const center = bounds.getCenter();
      map.panTo(center);
      map.fitBounds(bounds);
      if (map.getZoom() > this.minZoom) {
          map.setZoom(this.minZoom);
      }
      map.setZoom(map.getZoom());
  }

  toggleMap() {
    this.showMap = !this.showMap;
    this.universalStorage.setItem('map_state', `${this.showMap}`);
  }

  handleSortSelect(value) {
    this.selectedDate = this.sortValues[value.value];
    this.sortObserver.next(value);
  }

  handleFilterResults(filterResults) {
    if (!this.pagination$ && filterResults.count || filterResults.pageReset) {
      this.pagination$ = this.paginationService.getPager(filterResults.count);
    }
    this.workSpaces = filterResults['results'];
    this.workSpaceCount = filterResults.count;
    if (this.workSpaces) {
      this.markers = [];
      this.workSpaces.forEach((workSpace, index) => {
          const existedIndex = this.markers.findIndex(
              marker => (
                  marker['location'].latitude === workSpace['location'].latitude &&
                  marker['location'].longitude === workSpace['location'].longitude
              )
          );

        if (existedIndex === -1) {
            const workspaces = [];
            workspaces.push(workSpace);
            const workSpaces = Object.assign({'location': workSpace['location']}, {'workspaces': workspaces});
            this.markers.push(workSpaces);
        } else {
            this.markers[existedIndex]['workspaces'].push(workSpace);
        }
      });
      if (this.mapInit) {
          this.setBounds();
      }
    }
    // console.log(this.markers);

    /*if (this.markers) {
      of(this.markers)
        .subscribe(
          data => {
            this.varMarks = data;
          }
        );
    }*/
  }

  handleMinMaxPrices(location) {
      const prices = [];
      location['workspaces'].forEach(function (item, i) {
          prices.push(item.workspace.price);
      });

      if (location['workspaces'].length > 1) {
          const max = Math.max.apply(Math, prices);
          const min = Math.min.apply(Math, prices);
          return this.currencyPipe.transform(min, 'GBP', 'symbol', '1.0-2') + ' - ' + this.currencyPipe.transform(max, 'GBP', 'symbol', '1.0-2');
      } else {
          return this.currencyPipe.transform(location.workspaces[0].workspace.price, 'GBP', 'symbol', '1.0-2');
      }

  }

  handleLoading(loading: boolean) {
    this.loading = loading;
  }

  setShowSlider(showChildSlider: boolean) {
    this.showChildSlider = showChildSlider;
  }

  handlePageChange(event) {
    this.pagination$ = this.paginationService.getPager(this.workSpaceCount, event.page);
    this.paginationObserver.next(event.offset);
  }

  handleWorkSpaceHover(location, event) {
    const position = location.latitude + ';' + location.longitude;
    const marker = document.querySelector("[data-position='" + position + "']");

    if (marker) {
      if (event === 'in') {
        marker.classList.add('hover');
      } else {
        marker.classList.remove('hover');
      }
    }
  }

  openInfoWindow(space) {
    if (this.googleMap) {
      const latLng = new google.maps.LatLng(
        space['location'].latitude,
        space['location'].longitude
      );
      this.googleMap.panTo(latLng);
    }
    setTimeout(() => {
      this.showDetails = space['location'].id;
      this.handleMinMaxPrices(space);
    }, 500);
  }
}
