import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindaspaceFiltersComponent } from './findaspace-filters.component';

describe('FindaspaceFiltersComponent', () => {
  let component: FindaspaceFiltersComponent;
  let fixture: ComponentFixture<FindaspaceFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindaspaceFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindaspaceFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
