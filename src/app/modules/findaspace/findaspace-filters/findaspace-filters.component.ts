import { LocationService } from './../../../services/location.service';
import {
    AfterContentChecked,
    Component, ElementRef,
    EventEmitter,
    Inject,
    Input,
    OnDestroy,
    OnInit,
    Output,
    PLATFORM_ID,
    ViewChild
} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import { FilterService } from '../../../services/filter.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';
import {AfterContentInit} from '@angular/core/src/metadata/lifecycle_hooks';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';
import {LabelType, Options} from 'ng5-slider';

@Component({
  selector: 'app-findaspace-filters',
  templateUrl: './findaspace-filters.component.html',
  styleUrls: ['./findaspace-filters.component.scss'],
})
export class FindaspaceFiltersComponent implements OnInit, OnDestroy, AfterContentInit, AfterContentChecked {
  @Input()
  sortValue: Observable<{ type: string, value: string }>;
  @Input()
  pagination: Observable<number>;
  @Output()
  atFilter = new EventEmitter();
  @Output()
  loading = new EventEmitter();
  @Output()
  onSortValueClear = new EventEmitter();

  @ViewChild('searchInput')
  searchInput;

  public date: Date;
  public showFiltersMobile = false;
  public facilitiesList: any[] = [];
  public suggestionList: Suggestion[] = [];
  public durations = [
    {name: 'Monthly', value: 'monthly'},
    {name: 'Hourly', value: 'hourly'}
  ];
  public types = [
    {name: 'Private Office', value: 'private-office'},
    {name: 'Desk', value: 'desk'},
    {name: 'Meeting', value: 'meeting-room'}
  ];

  public typeNames = {
    'private-office': 'Private Office',
    'desk': 'Desk',
    'meeting-room': 'Meeting'
  };

  public filterNames = {
    capacity: null,
    minPrice: null,
    maxPrice: null
  };

  public widthDateField = 130;
  @ViewChild('invisibleText') invTextER: ElementRef;
  public stringDateField: string = '';
  public minWidthDateField = 130;

  public address$ = '';

  public filtersForm = this.fb.group({
    duration: ['monthly'],
    type: [null],
    address: [''],
    borough: [''],
    area: [''],
    station: [''],
    size: [''],
    capacity: [0],
    minPrice: [''],
    maxPrice: [''],
    quantity: [0],
    facilities: [[]],
    price: [''],
    availableFrom: [''],
    date: [''],
    start: [null]
  });

  public suggestionSubmitted = false;
  public isPageChanged = false;
  public anytimeButton;

  private pendingRequest: Subscription;

  private currentFilters: {};

  private getRouterSubscription: Subscription;

  public isBrowser;

    public stepValues = {
        'monthly_private-office': 500,
        'monthly_desk': 100,
        'hourly_desk': 10,
        'hourly_meeting-room': 10
    };

    public minValue: number = 0;
    public maxValue: number = 100000;
    public minRangeValue: number = 0;
    public maxRangeValue: number = 100000;
    public step: number = 500;
    // value: number = 50;

    options: Options = {
        floor: this.minValue,
        ceil: this.maxValue,
        draggableRange: true,
        step: this.step,
        customValueToPosition: (val: number, minVal: number, maxVal: number): number => {
            val = Math.sqrt(val);
            minVal = Math.sqrt(minVal);
            maxVal = Math.sqrt(maxVal);
            const range: number = maxVal - minVal;
            return (val - minVal) / range;
        },
        customPositionToValue: (percent: number, minVal: number, maxVal: number): number => {
            minVal = Math.sqrt(minVal);
            maxVal = Math.sqrt(maxVal);
            const value: number = percent * (maxVal - minVal) + minVal;
            return Math.pow(value, 2);
        },
        translate: (value: number, label: LabelType): string => {
            if (label === LabelType.Floor && this.minRangeValue !== this.minValue) {
                return '';
            }
            if (label === LabelType.Ceil && this.maxRangeValue !== this.maxValue) {
                return '';
            }
            if (label === LabelType.High || label === LabelType.Low) {
                return '£' + value;
            }
            return '';
        }
    };

  constructor(
    private fb: FormBuilder,
    private filterService: FilterService,
    private locationService: LocationService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId,
    @Inject('ORIGIN_URL') public baseUrl: string
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.locationService.getServiceFacilites().subscribe((facilities: any[]) => {
      this.facilitiesList = facilities;
    });

    this.filtersForm.get('duration').valueChanges.subscribe(duration => {
        const type = this.filtersForm.get('type').value;
      if (duration === 'hourly') {
          this.changePriveRangeBorders(0, 1000);
          this.maxRangeValue = 1000;
          this.minRangeValue = 0;
          this.maxValue = 1000;
          this.minValue = 0;
          const minPrice = this.minRangeValue;
          const maxPrice = this.maxRangeValue;
          this.step = this.stepValues['hourly_desk'];
          this.changeStepRange(this.step);
          if (minPrice > 1000 && maxPrice >= 1000) {
              this.changePriveRangeBorders(0, 1000);
          } else if (minPrice < 1000 && maxPrice >= 1000) {
              this.changePriveRangeBorders(minPrice, 1000);
          }
      } else if (duration === 'monthly') {
          this.changePriveRangeBorders(0, 100000);
          this.maxRangeValue = 100000;
          this.minRangeValue = 0;
          this.maxValue = 100000;
          this.minValue = 0;
          const minPrice = this.minRangeValue;
          const maxPrice = this.maxRangeValue;
          if (type && type === 'private-office') {
              this.step = this.stepValues['monthly_private-office'];
          } else {
              this.step = this.stepValues['monthly_desk'];
          }
          this.changeStepRange(this.step);
          if (minPrice === 0 && maxPrice === 1000) {
              this.changePriveRangeBorders(0, 100000);
          } else if (maxPrice === 1000) {
              this.changePriveRangeBorders(minPrice, 100000);
          }
      }
    });
    this.filtersForm.get('type').valueChanges.subscribe(type => {
      const duration = this.filtersForm.get('duration').value;
      if (duration && type) {
          const stepValueType = duration + '_' + type;
          this.step = this.stepValues[stepValueType];
          if (duration === 'hourly') {
              this.maxRangeValue = 1000;
              this.minRangeValue = 0;
          } else {
              this.maxRangeValue = 100000;
              this.minRangeValue = 0;
          }
          this.changePriveRangeBorders(this.minRangeValue, this.maxRangeValue);
      } else {
          this.step = this.stepValues['hourly_desk'];
      }
      this.changeStepRange(this.step);
    });
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
        this.searchInput.update
            .pipe(
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(address => {
                if (address === '') {
                    this.filtersForm.get('address').reset('');
                    this.filtersForm.get('borough').reset('');
                    this.filtersForm.get('area').reset('');
                    this.filtersForm.get('station').reset('');
                    this.setFilter();
                    return;
                }
                if (!this.suggestionSubmitted) {
                    this.getSuggestions(address);
                }
                this.suggestionSubmitted = false;
            });
        this.sortValue.subscribe(res => {
            if (!this.filtersForm.pristine) {
                if (res.type === 'price') {
                    this.filtersForm.get('date').reset('');
                } else {
                    this.filtersForm.get('price').reset('');
                }
                this.filtersForm.get(res.type).setValue(res.value);
                this.applyFilter();
                this.setQueryParams(this.currentFilters);
                this.getData(this.currentFilters);
            } else {
                const sorting = {};
                sorting[res.type] = res.value;
                this.applyFilter(sorting);
                this.setQueryParams(this.currentFilters);
                this.getData(this.currentFilters);
            }
        });
        this.pagination.subscribe(offset => {
            this.filtersForm.get('start').setValue(offset);
            this.isPageChanged = true;
            this.setFilter();
        });
        this.getRouterSubscription = this.route.queryParams.subscribe(params => {
            if ('duration' in params) {
                const paramsCopy = {...params};
                if ('availableFrom' in paramsCopy) {
                    paramsCopy['availableFrom'] = moment(+paramsCopy['availableFrom']);
                }
                if ('facilities' in paramsCopy) {
                    paramsCopy['facilities'] = JSON.parse(paramsCopy['facilities']);
                    paramsCopy['facilities'].forEach(facility => {
                        this.handleFacilitiesCheck({
                            target: {
                                value: facility,
                                checked: true
                            }
                        });
                    });
                }
                if ('address' in paramsCopy) this.address$ = paramsCopy['address'];
                if ('borough' in paramsCopy) this.address$ = paramsCopy['borough'];
                if ('area' in paramsCopy) this.address$ = paramsCopy['area'];
                if ('station' in paramsCopy) this.address$ = paramsCopy['station'];
                this.filtersForm.patchValue(paramsCopy);
                if ('availableFrom' in paramsCopy) {
                    this.handleDataChanged(moment(+params['availableFrom']).format('MMMM D, YYYY'));
                }
                this.suggestionSubmitted = true;
                if ('capacity' in paramsCopy) {
                    this.filterNames.capacity = paramsCopy['capacity'];
                }
                if ('minPrice' in paramsCopy) {
                    this.filterNames.minPrice = paramsCopy['minPrice'];
                    this.minRangeValue = paramsCopy['minPrice'];
                }
                if ('maxPrice' in paramsCopy) {
                    this.filterNames.maxPrice = paramsCopy['maxPrice'];
                    this.maxRangeValue = paramsCopy['maxPrice'];
                }

                this.getData(paramsCopy);
            } else {
                this.setFilter();
            }
        });
        document.body.addEventListener('click', (e) => {
            const target = <HTMLElement>e.target;
            if (e && !target.closest('.filter-col')) {
                const menuItems = document.querySelectorAll('.filter-col');
                menuItems.forEach(menuItem => {
                    if (menuItem) {
                        menuItem.classList.remove('active');
                    }
                });
                return;
            }
        });
    // }
  }

  changePriveRangeBorders(min, max) {
      // this.options = Object.assign({}, this.options, {floor: min, ceil: max});
      const newOptions: Options = Object.assign({}, this.options);
      newOptions.floor = min;
      newOptions.ceil = max;
      this.options = newOptions;
  }

  changeCurrentRangeBorders(min, max) {
      // this.options = Object.assign({}, this.options, {floor: min, ceil: max});
      const newOptions: Options = Object.assign({}, this.options);
      newOptions.floor = min;
      newOptions.ceil = max;
      this.options = newOptions;
  }

  changeStepRange(step) {
      const newOptions: Options = Object.assign({}, this.options);
      newOptions.step = step;
      this.options = newOptions;
  }

  ngOnDestroy() {
    if (this.isBrowser) {
        this.getRouterSubscription.unsubscribe();
    }
  }

  ngAfterContentInit() {
    if (this.isBrowser) {
        this.getRouterSubscription.unsubscribe();
    }
  }

    ngAfterContentChecked() {
        // console.log(this.minRangeValue, this.maxRangeValue);
    }

  getData(params) {
      this.filterService.filter(params).subscribe(
          res => {
              /*res['results'] = res['results'].map(item => {
                  if (item['workspace'].images.length > 1) {
                      const mainImageIndex = item['workspace'].images
                          .findIndex(image => image.url === item['workspace'].coverImageUrl);
                      const mainImage = item['workspace'].images.splice(mainImageIndex, 1);
                      item['workspace'].images = [...mainImage, ...item['workspace'].images];
                  }
                  return item;
              });*/
              res['pageReset'] = !params['start'];
              this.atFilter.emit(res);
              this.loading.emit(false);
              this.isPageChanged = false;
          },
          err => {
              console.log(err);
              this.atFilter.emit({results: []});
              this.loading.emit(false);
          }
      );
  }

  setFilter() {
      this.loading.emit(true);
      this.applyFilter();
      this.setQueryParams(this.currentFilters);
      this.getData(this.currentFilters);
  }

  handleDataChanged(ev) {

      this.stringDateField = ev;
      setTimeout ( () =>{
      if (this.invTextER.nativeElement.offsetWidth > this.minWidthDateField - 45) {
          this.widthDateField = this.invTextER.nativeElement.offsetWidth + 45;
      } else {
          this.widthDateField = this.minWidthDateField;
      }
      }, 0);
  }

  applyFilter(allFilters?) {
    if (!this.isPageChanged) {
      this.filtersForm.get('start').reset();
    }
    this.checkType();
    this.atFilter.emit([]);
    this.loading.emit(true);
    let facilities;
    let filters = {...this.filtersForm.value};
    if (typeof this.filtersForm.get('facilities').value === 'string') {
      facilities = JSON.parse(this.filtersForm.get('facilities').value);
    } else {
      facilities = this.filtersForm.get('facilities').value;
    }
    if (!this.filtersForm.get('minPrice').touched && +this.minRangeValue === +this.filtersForm.get('minPrice').value) {
      // delete filters['minPrice'];
    }
    if (!this.filtersForm.get('maxPrice').touched && +this.maxRangeValue === +this.filtersForm.get('maxPrice').value) {
      delete filters['maxPrice'];
    }
    if (allFilters && typeof allFilters === 'object' && Object.keys(allFilters).length) {
        // allFilters['duration'] = 'monthly';
        if ('price' in allFilters && 'date' in filters) {
            delete filters['date'];
        } else if ('date' in allFilters && 'price' in filters) {
            delete filters['price'];
        }
        filters = Object.assign(filters, allFilters);
    } else if (allFilters && typeof allFilters === 'object') {
        // allFilters['duration'] = 'monthly';
        filters = Object.assign(filters, allFilters);
    } else {
        filters = allFilters || filters;
    }

    if (!this.filtersForm.get('address').value &&
        !this.filtersForm.get('borough').value &&
        !this.filtersForm.get('area').value &&
        !this.filtersForm.get('station').value) {
      filters['address'] = this.address$;
    }
    this.suggestionList = [];
    this.filterNames.capacity = filters.capacity;
    this.filterNames.minPrice = this.minRangeValue || filters.minPrice;
    this.filterNames.maxPrice = this.maxRangeValue || filters.maxPrice;
    filters.minPrice = this.minRangeValue || filters.minPrice;
    filters.maxPrice = this.maxRangeValue || filters.maxPrice;
    this.closeFilters();
    this.currentFilters = filters;
    if (allFilters) {
        this.filtersForm.get('date').reset('');
        this.filtersForm.get('price').reset('');
    }
  }

  getSuggestions(address) {
    if (!address) {
      this.suggestionList = [];
      return;
    }
    this['address$'] = address;
    this.pendingRequest = this.filterService.getSuggestions(address)
      .pipe(distinctUntilChanged())
      .subscribe(res => {
        this.suggestionList = <{value: string, type: string}[]>res;
      });
  }

  submitSuggestion(address, type) {
    this.suggestionSubmitted = true;
    this.filtersForm.get('address').reset('');
    this.filtersForm.get('borough').reset('');
    this.filtersForm.get('area').reset('');
    this.filtersForm.get('station').reset('');
    this.filtersForm.get(type).setValue(address);
    this.address$ = address;
    this.setFilter();
    this.suggestionList = [];
  }

  handleSuggestion(suggestion) {
    suggestion = suggestion.value;
    const addressValue = this.address$;
    const addressMatch = suggestion.match(new RegExp(addressValue, 'ig'));
    return suggestion.replace(addressMatch, `<strong>${addressMatch}</strong>`);
  }

  resolveSuggestion(suggestion) {
      return '<div class="left-col-wrap">' + this.handleSuggestion(suggestion) + '</div>';
  }

  handleCapacityChange(capacity) {
    this.filtersForm.get('capacity').setValue(+capacity);
  }

  handleIncreaseCapacity() {
      if (+this.filtersForm.get('capacity').value + 1 < 5000) {
          this.filtersForm.get('capacity').setValue(+this.filtersForm.get('capacity').value + 1);
      } else {
          this.filtersForm.get('capacity').setValue(5000);
      }
  }

  handleDecreaseCapacity() {
      if (+this.filtersForm.get('capacity').value - 1 > 0) {
          this.filtersForm.get('capacity').setValue(+this.filtersForm.get('capacity').value - 1);
      } else {
          this.filtersForm.get('capacity').setValue(0);
      }
  }

  setQueryParams(filters) {
    const queryParams = {};
    Object.keys(filters).forEach(key => {
      if (filters[key]) {
        queryParams[key] = filters[key];
      }
      if (filters[key] instanceof Array) {
        queryParams[key] = JSON.stringify(filters[key]);
      }
      if (filters[key] instanceof Date) {
        filters[key] = this.convertToUtc(filters[key]);
        queryParams[key] = (filters[key].getTime()).toFixed();
      }
    });
    this.router.navigate(
      [],
      {
        queryParams,
        replaceUrl: true
      }
    );
  }

  showFilter(e?, selector?, pickerInstance?) {
    if (selector) {
      setTimeout(() => {
        const datepickerPopup = document.querySelector(selector);
        if (!this.anytimeButton) {
          this.anytimeButton = document.createElement('button');
          this.anytimeButton.classList.add('btn-site');
          this.anytimeButton.classList.add('anytime_button');
          this.anytimeButton.innerText = 'Anytime';
          this.anytimeButton.onclick = () => {
            this.clearFilters('availableFrom');
            pickerInstance.close();
          };
        }
        datepickerPopup.appendChild(this.anytimeButton);
      }, 200);
    }
    if (e && e.target.closest('.filter-dropdown')) {
      return;
    }
    let item;
    const menuItems = document.querySelectorAll('.filter-col');
    if (e) {
      item = e.target.closest('.filter-col');
    }
    menuItems.forEach(menuItem => {
      if (e) {
        if (menuItem !== item) {
          menuItem.classList.remove('active');
        } else {
          item.classList.toggle('active');
        }
      } else {
        menuItem.classList.remove('active');
      }
    });
  }

  closeFilters() {
    const menuItems = document.querySelectorAll('.filter-col');
    menuItems.forEach(menuItem => {
      if (menuItem) {
        menuItem.classList.remove('active');
      }
    });
  }

  handleFacilitiesCheck(event) {
    const facilities = <FormControl>this.filtersForm.get('facilities');
    let value;
    if (event.target.checked) {
      facilities.value.push(event.target.value);
      return;
    } else {
      value = facilities.value.filter(item => item !== event.target.value);
    }
    facilities.setValue(value);
    return;
  }

  handleSearchSubmit(event) {
    if (event.keyCode === 13) {
      this.filtersForm.get('borough').reset('');
      this.filtersForm.get('area').reset('');
      this.filtersForm.get('station').reset('');
      this.setFilter();
        /*if (this.filtersForm.get('address').value !== this.address$) {
            this.filtersForm.get('address').reset('');
            this.filtersForm.get('address').setValue(this.address$);
            this.setFilter();
        }*/
    }
  }

  getPriceRange(values, handle) {
    if (this.filtersForm) {
      if (handle === 0) {
        this.filtersForm.get('minPrice').setValue(values[handle]);
      } else if (handle === 1) {
        this.filtersForm.get('maxPrice').setValue(values[handle]);
      }
    }
  }

  /*changePriceSlideStep(values, handle) {
    if (this.filtersForm.get('duration').value === 'hourly') {
      this.priceSlider.noUiSlider.updateOptions({
        step: 10
      });
    } else if (this.filtersForm.get('duration').value === 'monthly' && +values[0] <= 1000) {
      this.priceSlider.noUiSlider.updateOptions({
        step: 50
      });
    } else if (this.filtersForm.get('duration').value === 'monthly' && +values[0] > 1000) {
      this.priceSlider.noUiSlider.updateOptions({
        step: 100
      });
    }
  }*/

  clearFilters(filter?) {
    const defaultValues = {
      duration: 'monthly',
      type: null,
      facilities: [],
      minPrice: this.minValue,
      maxPrice: this.maxValue,
      availableFrom: '',
      price: '',
      date: '',
      capacity: ''
    };
    if (!filter) {
      this.filtersForm.reset(defaultValues);
        this.minRangeValue = this.minValue;
        this.maxRangeValue = this.maxValue;
      this.changePriveRangeBorders(this.minValue, this.maxValue);
      this.onSortValueClear.emit();
      this.address$ = '';
      this.filtersForm.get('address').reset('');
      this.filtersForm.get('borough').reset('');
      this.filtersForm.get('area').reset('');
      this.filtersForm.get('station').reset('');
      this.applyFilter({});
      delete this.currentFilters['maxPrice'];
      delete this.currentFilters['minPrice'];
      this.setQueryParams(this.currentFilters);
      this.getData(this.currentFilters);
    } else {
      if (filter === 'capacity') {
        this.filtersForm.get(filter).reset(defaultValues[filter]);
        this.setFilter();
        return;
      }
      if (filter === 'minPrice') {
          this.minRangeValue = this.minValue;
          this.maxRangeValue = this.maxValue;
          this.filtersForm.get('minPrice').reset('');
          this.filtersForm.get('maxPrice').reset('');
          delete this.currentFilters['maxPrice'];
          delete this.currentFilters['minPrice'];
          this.changePriveRangeBorders(this.minValue, this.maxValue);
        this.setFilter();
        return;
      }
      if (filter === 'availableFrom') {
        this.filtersForm.get(filter).reset(defaultValues[filter]);
        this.setFilter();
        return;
      }
      this.filtersForm.get(filter).reset(defaultValues[filter]);
    }
    const facilities = <NodeListOf<HTMLInputElement>>document.querySelectorAll('input[name="facilities"]');
    facilities.forEach(facility => facility.checked = false);
  }

  getFacilityName(slug) {
    const facility = this.facilitiesList.find(item => item.slug === slug);
    return facility ? facility.name : '';
  }

  checkType() {
    if (this.filtersForm.get('duration').value === 'monthly' &&
        this.filtersForm.get('type').value === 'meeting-room' ||
        this.filtersForm.get('duration').value === 'hourly' &&
        this.filtersForm.get('type').value === 'private-office') {
      this.filtersForm.get('type').setValue(null);
    }
  }

  isArray(value) {
    return Array.isArray(value);
  }

  validateCapacity(e) {
    if (e.target.value.length > 4) {
      e.target.value = e.target.value.slice(0,4);
    }
    if (e.target.value > 5000) {
      e.target.value = 5000;
    }
    if (e.target.value < 0) {
      e.target.value = 0;
    }
  }

  convertToUtc(date, time?: number): Date | number {
    if (date instanceof moment) {
        date = moment(date).toDate();
    }
    if (time) {
      const startDate = time;
      return new Date(startDate).getTime();
    } else {
        date.setHours(0, 0);
      const startDate = date.getTime();
      return new Date(startDate);
    }
  }
}

interface Suggestion {
  value: string;
  type: string;
}
