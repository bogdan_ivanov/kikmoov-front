import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { LocationService } from '../../../services/location.service';
import * as moment from 'moment';

@Component({
  selector: 'app-bookings-table',
  templateUrl: './bookings-table.component.html',
  styleUrls: ['./bookings-table.component.scss']
})
export class BookingsTableComponent implements OnInit {
  private workSpaceId;
  public workSpace;
  public bookingList = [];

  constructor(public ngxSmartModalService: NgxSmartModalService, private locationService: LocationService) {
  }

  ngOnInit() {
    const bookingModal = this.ngxSmartModalService.get('bookingModal');
    bookingModal.onOpen.subscribe(modal => {
      this.bookingList = [];
      this.workSpaceId = modal['workSpaceId'];
      this.workSpace = modal['workSpace'];
      this.locationService.getBookingList(this.workSpaceId).subscribe(res => {
        this.bookingList = res.map(booking => {
          const startTime = moment((booking.startTime * 1000)).format('HH:mma');
          const endTime = moment((booking.endTime * 1000)).format('HH:mma');
          return {
            date: moment((booking.startTime * 1000)).format('DD/MM/YYYY'),
            time: `${startTime} - ${endTime}`,
            meetingName: booking.meetingName || '',
            contract: `${booking.name}\n${booking.email}\n${booking.phone}`
          };
        });
      });
    });
  }

}
