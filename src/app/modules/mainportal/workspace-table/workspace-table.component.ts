import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { LocationService } from '../../../services/location.service';
import { MessageService } from '../../../services/message.service';
import {removeSummaryDuplicates} from '@angular/compiler';

@Component({
  selector: 'app-workspace-table',
  templateUrl: './workspace-table.component.html',
  styleUrls: ['./workspace-table.component.scss'],
})
export class WorkspaceTableComponent implements OnInit {
  @Input()
  headData: string[];
  @Input()
  bodyData;
  @Output()
  handleEdit = new EventEmitter<any>();
  @Output()
  onUpdateStatus = new EventEmitter<any>();

  public unavailable = 'unavailable';
  public available = 'active';

  public deletingOfficeId;

  constructor(public ngxSmartModalService: NgxSmartModalService,
              private locationService: LocationService,
              private messageService: MessageService) {}

  ngOnInit() {
  }

  showBookings(workSpace, location) {
    const bookingModal = this.ngxSmartModalService.getModal('bookingModal');
    bookingModal['workSpaceId'] = workSpace.id;
    bookingModal['workSpace'] = workSpace;
    bookingModal['location'] = location;
    bookingModal.open();
  }

  confirmDeleteSpace({originalData}) {
    this.deletingOfficeId = originalData.id;
    setTimeout(() => {
      this.ngxSmartModalService.getModal('confirmDelete').open();
    }, 10);
  }

  confirmDeleting() {
    this.locationService.deleteOffice(this.deletingOfficeId).subscribe(
      res => {
        this.messageService.setMessage('Space was deleted successfully');
        this.messageService.showMessage();
        this.bodyData = this.bodyData.filter(item => item[item.length - 1].originalData.id !== this.deletingOfficeId);
        this.ngxSmartModalService.getModal('confirmDelete').close();
      }, err => {
        console.log(err);
      }
    );
  }

  handleEditClick(event, data) {
    event.preventDefault();
    if ('originalData' in data) {
      this.handleEdit.emit(data.originalData);
    }
  }

  handleUpdateStatus(event, data, status) {
    this.locationService.changeStatus(data.originalData.id, status).subscribe(
        res => {
            this.messageService.setMessage(res['message']);
            this.messageService.showMessage();
            if (res['status']) {
                this.onUpdateStatus.emit(
                    {
                        'id': data.originalData.id,
                        'status': res['status'],
                        'type': data.originalData.type,
                        'deskType': data.originalData.deskType || null
                    }
                );
            }
        }, err => {
            console.log(err);
        }
    );
  }
}
