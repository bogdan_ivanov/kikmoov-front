import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { MainportalComponent } from './mainportal.component';
import { LocationService } from '../../services/location.service';
import { ErrorsService } from '../../services/errors.service';
import { MessageService } from '../../services/message.service';
import { MainportalRoutes } from './mainportal.routing';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { WorkspaceTableComponent } from './workspace-table/workspace-table.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { BookingsTableComponent } from './bookings-table/bookings-table.component';
import { PortalLocationModule } from '@shared/layouts/portal-location/portal-location.module';
import { PortalSidebarModule } from '@shared/layouts/portal-sidebar/portal-sidebar.module';


@NgModule({
  imports: [
      CommonModule,
      MainportalRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      PostSpaceBarModule,
      NgxSmartModalModule,
      PortalLocationModule,
      PortalSidebarModule
  ],
  declarations: [
      MainportalComponent,
      WorkspaceTableComponent,
      BookingsTableComponent
  ],
    providers: [
        LocationService,
        ErrorsService,
        MessageService
    ]
})
export class MainportalModule {}
