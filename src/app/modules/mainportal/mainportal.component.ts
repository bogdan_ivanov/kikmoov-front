import { Component, OnDestroy, OnInit } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { UserService } from '../../services/user.service';
import { ErrorsService } from '../../services/errors.service';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniversalStorage } from '@shared/storage/universal.storage';

@Component({
  selector: 'app-mainportal',
  templateUrl: './mainportal.component.html',
  styleUrls: ['./mainportal.component.scss'],
})
export class MainportalComponent implements OnInit, OnDestroy {
  public privateOfficeHead = [
    'Quantity',
    'Size (sq ft)',
    'Price (monthly)',
    'Status',
    'Bookings',
  ];
  public privateOfficeData = [];

  public meetingSpaceHead = [
    'Quantity',
    'Amount (ppl)',
    'Price (hourly)',
    'Status',
    'Bookings',
  ];
  public meetingSpaceData = [];

  public desksHead = [
    'Quantity',
    'Type',
    'Price (monthly)',
    'Status',
    'Bookings',
  ];
  public desksData = [];

  public desksHourlyHead = [
    'Quantity',
    'Type',
    'Price (hourly)',
    'Status',
    'Bookings',
  ];
  public desksHourlyData = [];
  public workspacelist;
  public currentId;
  public currentLocation;

  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office',
  };
  public descTypes = {
    monthly_hot_desk: 'Hot desk',
    hourly_hot_desk: 'Hot desk',
    monthly_fixed_desk: 'Fixed desk',
    daily_hot_desk: 'Daily desk'
  };

  private getLocationSubscription: Subscription;

  constructor(
    private locationService: LocationService,
    private router: Router,
    public errorsService: ErrorsService,
    public messageService: MessageService,
    private universalStorage: UniversalStorage
  ) {
    if (this.errorsService.hasErrors()) {
      this.errorsService.showError();
      this.errorsService.clearErrors();
    }
    if (this.messageService.hasMessage()) {
      this.messageService.showMessage();
      this.messageService.clearMessage();
    }
    this.getLocationSubscription = this.locationService.getLocations().subscribe(
      res => {
        this.workspacelist = res['locations'];
        if (this.universalStorage.getItem('active_location') !== null) {
          this.currentLocation = this.workspacelist.find(
            item => item['id'] === +this.universalStorage.getItem('active_location')
          );
          this.currentId = this.currentLocation['id'];
        } else {
          this.currentId =
            this.workspacelist.length && this.workspacelist[0]
              ? this.workspacelist[0]['id']
              : null;
          this.currentLocation =
            this.workspacelist.length && this.workspacelist[0]
              ? this.workspacelist[0]
              : null;
        }
        this.initPrivateOfficeTable(this.currentId);
      },
      err => {
        this.errorsService.setErrors(err);
        this.errorsService.showError();
        this.errorsService.clearErrors();
      }
    );
    this.universalStorage.removeItem('workSpaceList');
    this.universalStorage.removeItem('bkList');
    this.universalStorage.removeItem('prevOffices');
    this.universalStorage.removeItem('officeState');
    this.universalStorage.removeItem('new_location');
  }

  initPrivateOfficeTable(id) {
    this.claerTables();
    this.workspacelist.map(ws => {
      ws['desks'].map(desk => {
        if (!('typeRent' in desk)) {
          desk.typeRent = this.descTypes[desk.type];
          desk.deskType = desk.type;
        }
        desk.type = 'desk';
        return desk;
      });
      ws['privateOffices'].map(
        privateOffice => (privateOffice.type = 'private-office')
      );
      ws['meetingRooms'].map(meeting => (meeting.type = 'meeting-room'));
      ws.offices = [].concat(
        ws['desks'],
        ws['privateOffices'],
        ws['meetingRooms']
      );
      return ws;
    });
    this.workspacelist.forEach(element => {
      if (element['id'] == id) {
        element.offices.forEach(office => {
          if (office['type'] == 'desk') {
            if (office.deskType === 'hourly_hot_desk' || office.deskType === 'daily_hot_desk') {
              this.addDataToHourlyDesktable(office, element);
            }
            if (office.deskType.includes('monthly')) {
              this.addDataToDesktable(office, element);
            }
          } else if (office['type'] == 'private-office') {
            this.addDataToPrivatetable(office, element);
          } else if (office['type'] == 'meeting-room') {
            this.addDataToMeetingtable(office, element);
          }
        });
      }
    });
  }

  claerTables() {
    this.desksData = [];
    this.desksHourlyData = [];
    this.meetingSpaceData = [];
    this.privateOfficeData = [];
  }

  addDataToDesktable(deskData, location) {
    if (deskData['status'] === 'active') {
      deskData['status'] = 'available';
    }

    this.desksData.push([
      {data: deskData['quantity']},
      {data: deskData['typeRent']},
      {
        data: deskData['price'],
        title: 'price'
      },
      {data: deskData['status'], cellStyle: deskData['status']},
      {data: (+deskData['bookings'] + +deskData['viewing']).toString(), bookings: true},
      {originalData: deskData, location},
    ]);
  }

  addDataToHourlyDesktable(deskData, location) {
    if (deskData['status'] === 'active') {
      deskData['status'] = 'available';
    }

    this.desksHourlyData.push([
      {data: deskData['quantity']},
      {data: deskData['typeRent']},
      {
        data: deskData['price'],
        title: 'price'
      },
      {data: deskData['status'], cellStyle: deskData['status']},
      {data: (+deskData['bookings'] + +deskData['viewing']).toString(), bookings: true},
      {originalData: deskData, location},
    ]);
  }

  addDataToMeetingtable(meetingData, location) {
    if (meetingData['status'] === 'active') {
      meetingData['status'] = 'available';
    }
    this.meetingSpaceData.push([
      { data: meetingData['quantity'] },
      { data: meetingData['capacity'] },
      {
        data: meetingData['price'],
        title: 'price'
      },
      { data: meetingData['status'], cellStyle: meetingData['status'] },
      { data: (+meetingData['bookings'] + +meetingData['viewing']).toString(), bookings: true },
      { originalData: meetingData, location },
    ]);
  }

  addDataToPrivatetable(privateData, location) {
    if (privateData['status'] === 'active') {
      privateData['status'] = 'available';
    }
    this.privateOfficeData.push([
      { data: privateData['quantity'] },
      { data: privateData['size'] },
      {
        data: privateData['price'],
        title: 'price'
      },
      { data: privateData['status'], cellStyle: privateData['status'] },
      { data: (+privateData['bookings'] + +privateData['viewing']).toString(), bookings: true },
      { originalData: privateData, location },
    ]);
  }

  onWorkspaceSelect(event) {
    this.currentId = event;
    this.currentLocation =
      this.workspacelist.find(item => +item.id === +event) || null;
    this.initPrivateOfficeTable(event);
  }

  handleEdit(data) {
    this.router.navigate([
      '/upload-location',
      this.currentId,
      data['type'],
      data['id'],
    ]);
  }

  handleDelete(locationId) {
    this.workspacelist = this.workspacelist.filter(location => location.id !== locationId);
    if (this.workspacelist.length) {
      this.onWorkspaceSelect(this.workspacelist[0].id);
    } else {
      this.initPrivateOfficeTable(locationId);
    }
  }

  handleUpdateStatus(data) {
    let bodyData;
    if (data.type === 'private-office') {
      bodyData = this.privateOfficeData;
    } else if (data.type === 'meeting-room') {
      bodyData = this.meetingSpaceData;
    } else if (data.type === 'desk' && (data.deskType === 'hourly_hot_desk' || data.deskType === 'daily_hot_desk')) {
      bodyData = this.desksHourlyData;
    } else {
      bodyData = this.desksData;
    }
    const index = bodyData.findIndex(item => item[item.length - 1].originalData.id === data.id);
      bodyData[index].forEach(item => {
      if ('cellStyle' in item) {
        const status = data.status === 'active' ? 'available' : data.status;
        item.data = status;
        item.cellStyle = status;
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.getLocationSubscription.unsubscribe();
  }
}
