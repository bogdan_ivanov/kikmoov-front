import { Routes, RouterModule } from '@angular/router';
import { MainportalComponent } from './mainportal.component';
import { AuthGuard } from '../../middleware/is-auth.guard';
import { SellerGuard } from '../../middleware/is-seller.guard';

const routes: Routes = [
  {
    path: '',
    component: MainportalComponent,
    canActivate: [AuthGuard, SellerGuard],
    data: {
      meta: {
        title: 'mainportal.title',
        description: 'mainportal.text',
        override: true,
      },
    },
  },
];

export const MainportalRoutes = RouterModule.forChild(routes);
