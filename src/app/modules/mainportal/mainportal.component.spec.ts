import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainportalComponent } from './mainportal.component';

describe('MainportalComponent', () => {
  let component: MainportalComponent;
  let fixture: ComponentFixture<MainportalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainportalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainportalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
