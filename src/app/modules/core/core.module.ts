import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthServiceConfig } from 'angularx-social-login';
import { getAuthServiceConfigs } from '../../configs/auth-service-config';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { HttpRefreshTokenInterceptor } from '../../middleware/http-refresh-token-interceptor';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { HttpErrInterceptor } from '../../middleware/http-err-interceptor';
import { HttpHeaderInterceptor } from '../../middleware/http-header-interceptor';
import { BuyerGuard } from '../../middleware/is-buyer.guard';
import { SellerGuard } from '../../middleware/is-seller.guard';
import { LoginGuard } from '../../middleware/login.guard';
import { AuthGuard } from '../../middleware/is-auth.guard';

const CustomDateFormats = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
  imports: [
      CommonModule,
      HttpClientModule
  ],
  declarations: [],
    providers: [
        AuthGuard,
        LoginGuard,
        SellerGuard,
        BuyerGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpHeaderInterceptor,
            multi: true,
        },/*
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpRefreshTokenInterceptor,
            multi: true,
        },*/
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrInterceptor,
            multi: true,
        },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE]
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: CustomDateFormats
        },
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
        },
    ]
})
export class CoreModule {}
