import { Routes, RouterModule } from '@angular/router';
import { BuyerProfileComponent } from './buyer-profile.component';
import {AuthGuard} from '../../middleware/is-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: BuyerProfileComponent,
    canActivate: [AuthGuard],
    data: {
      meta: {
        title: 'profile.title',
        description: 'profile.text',
        override: true,
      },
    },
  },
];

export const BuyerProfileRoutes = RouterModule.forChild(routes);
