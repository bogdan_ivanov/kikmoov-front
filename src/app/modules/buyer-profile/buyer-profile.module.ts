import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { MessageService } from '../../services/message.service';
import { BuyerProfileRoutes } from './buyer-profile.routing';
import { BuyerProfileComponent } from './buyer-profile.component';
import { ProfileService } from '../../services/profile.service';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      BuyerProfileRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule,
      MatCardModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      BuyerProfileComponent
  ],
    providers: [
        ProfileService,
        MessageService
    ]
})
export class BuyerProfileModule {}
