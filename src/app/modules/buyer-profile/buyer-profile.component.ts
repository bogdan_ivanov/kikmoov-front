import {AfterViewInit, Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MessageService } from '../../services/message.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-buyer-profile',
  templateUrl: './buyer-profile.component.html',
  styleUrls: ['./buyer-profile.component.scss']
})
export class BuyerProfileComponent implements OnInit, AfterViewInit {
  public profileForm: FormGroup;
  public userName$: Observable<{firstname: string, lastname: string}>;
  public userNameObserver: Observer<{firstname: string, lastname: string}>;
  private phonePattern = '^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$';
  public isBrowser;

  constructor(
      private profileService: ProfileService,
      private fb: FormBuilder,
      private messageService: MessageService,
      @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);

    this.profileForm = this.fb.group({
      firstname: [null],
      lastname: [null],
      email: [{value: null, disabled: true}],
      phone: [null, [Validators.pattern(new RegExp(this.phonePattern, 'gm'))]],
      companyName: [null]
    });
    this.userName$ = new Observable(observer => {
      this.userNameObserver = observer;
    });
  }

  ngOnInit() {
  }

    ngAfterViewInit() {
        this.profileService.getUserDetails().subscribe(
            res => {
                this.profileForm.patchValue(res);
                if (this.userNameObserver) {
                    this.userNameObserver.next({
                        firstname: res['firstname'],
                        lastname: res['lastname']
                    });
                }
            },
            err => {
                console.log(err);
            }
        );
    }

  saveProfile(form) {
    this.profileService.updateUserDetails(form.value).subscribe(res => {
      form.markAsPristine();
      if (this.userNameObserver) {
        this.userNameObserver.next({
          firstname: form.value['firstname'],
          lastname: form.value['lastname']
        });
      }
      this.messageService.setMessage('Profile was updated successfully');
      this.messageService.showMessage();
    }, err => {
      console.log(err);
    });
  }

}
