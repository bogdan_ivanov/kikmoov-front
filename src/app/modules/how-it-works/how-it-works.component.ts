import {Component, Inject, OnInit} from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ContactService } from '../../services/contact.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from '../../services/message.service';
import { ErrorsService } from '../../services/errors.service';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.scss']
})
export class HowItWorksComponent implements OnInit {

  public faqContent = [
    {
      title: 'Can I rent a private office?',
      description: 'Yes, of course. Through kikmoov you can filter through thousands of flexible office spaces. You can arrange viewings at any time through our platform. Feel free to call, email or simply chat with us live online if you have any questions or require further information.'
    },
    {
      title: 'I only need one desk, can I rent one?',
      description: 'Yes, through kikmoov, you are able to the markets monthly hot desks and dedicated desks available. These are great flexible options if you are a freelancer or an entrepreneur on a budget. Book your viewings and see the spaces available for yourselves.'
    },
    {
      title: 'Can I book a desk by the hour?',
      description: 'Yes, it is 100% possible with Kikmoov. We give you the flexible option of choosing both hourly and monthly spaces. Simply use our platform and search for all hourly desk options then book and pay directly through our website – it’s that easy!'
    },
    {
      title: 'Can I book hourly meeting room spaces?',
      description: 'Absolutely, here at kikmoov we understand the importance of a suitable meeting space! Scroll through market options. Find, book and pay directly through our platform. If you have any questions, feel free to get in touch with us.'
    },
    {
      title: 'What should I look for in flexible workspace?',
      description: 'The perfect work space needs to feel like a home away from home. Having a great workspace will increase work productivity. Browse the kikmoov website to find your perfect workplace.'
    },
    {
      title: 'What is included in a serviced office?',
      description: 'Serviced offices are not your traditional leased office space. Serviced offices and co-working providers will include utilities, cleaning services, a front of house community officer and many other amenities such as coffee and beer. However, this will differ from provider to provider depending on their mission statement and work model.'
    },
    {
      title: 'How much space do I need in my office?',
      description: 'The traditional office space size per person is traditionally 100 square feet per person; many years ago, it was even 150 square feet per person. Now companies have become more cost efficient and require less space due to technology.'
    },
    {
      title: 'How can I schedule a viewing with Kikmoov?',
      description: 'It’s very easy; once you have found an office you like simply click the ‘schedule a viewing’ button. A pop up will appear requesting the best suitable times and dates for you. We recommend viewing multiple options at a time to understand market availability.We get back to you as fast as possible, within 24 hours. Our team work on your behalf and arrange everything for you.'
    },
    {
      title: 'Are there any hidden costs at Kikmoov? ',
      description: 'No, there are no hidden costs. To search and find a flexible office space is absolutely free! We will never charge you for this service. The only costs which you will incur will be once you have moved into your chosen office to pay the office provider.'
    },
    {
      title: 'Do you only have co-working spaces on your platform?',
      description: 'No, we have private offices, dedicated desks, hot desks, hourly desks and hourly meeting room spaces. Use the dedicated filters to find your suitable office type.'
    }
  ];

  public enquiryForm: FormGroup;
  private phonePattern = '^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$';

  private checkedAreas = [];

  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    public contactService: ContactService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private errorService: ErrorsService,
    @Inject('ORIGIN_URL') public baseUrl: string
  ) {
    this.enquiryForm = this.fb.group({
      area: [[], [Validators.required]],
      fullName: [null, [Validators.required]],
      email: [null, [Validators.required]],
      numberOfPeople: [null, [Validators.required]],
      companyName: [null, [Validators.required]],
      phone: [null, Validators.pattern(new RegExp(this.phonePattern, 'gm'))],
      message: [null, []],
    });
  }

  ngOnInit() {
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  onSubmit(form) {
    this.contactService.sendLeasehold(form.value).subscribe(res => {
      form.reset();
      this.resetCheckboxes();
      this.ngxSmartModalService.getModal('afterSendMessageHiw').open();
    }, err => {
      this.errorService.setFieldErrors(err, form);
    });
  }

  resetCheckboxes() {
    const checkboxes = document.querySelectorAll('.checkbox-form-wrap input');
    if (checkboxes) {
      checkboxes.forEach(checkbox => checkbox['checked'] = false);
    }
  }

  handleAreaCheck(event) {
    const value = event.target.value;
    const isChecked = event.target.checked;
    if (isChecked) {
      this.checkedAreas.push(value);
    } else {
      this.checkedAreas = this.checkedAreas.filter(item => item !== value);
    }
    this.enquiryForm.get('area').setValue(this.checkedAreas);
  }


}
