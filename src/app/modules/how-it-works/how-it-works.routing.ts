import { Routes, RouterModule } from '@angular/router';
import { HowItWorksComponent } from './how-it-works.component';

const routes: Routes = [
  {
    path: '',
    component: HowItWorksComponent,
    data: {
      meta: {
        title: 'howItWorks.title',
        description: 'howItWorks.text',
        override: true,
      },
    },
  },
];

export const HowItWorksRoutes = RouterModule.forChild(routes);
