import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TermsRoutes } from './terms.routing';
import { TermsComponent } from './terms.component';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';


@NgModule({
  imports: [
      CommonModule,
      TermsRoutes,
      TranslateModule,
      KikmoovFooterModule,
      NavmenuModule
  ],
  declarations: [TermsComponent],
})
export class TermsModule {}
