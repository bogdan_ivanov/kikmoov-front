import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { isPlatformBrowser } from '@angular/common';
import {forEach} from '@angular/router/src/utils/collection';
import {REQUEST} from '@nguniversal/express-engine/tokens';

@Injectable()
export class UniversalStorage implements Storage {
  [index: number]: string;
  [key: string]: any;
  length: number;
  cookies: any;
  private isBrowser;

  constructor(
      private cookieService: CookieService,
      @Inject(PLATFORM_ID) private platformId,
      @Inject(REQUEST) public request: any
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
      if (!this.isBrowser) {
          let _cookies = this.request.cookies || {};
          this.cookies = this.request.cookies || {};
      }
  }

  public clear(): void {
    if (this.isBrowser) {
        localStorage.clear();
    } else {
        this.cookieService.removeAll();
    }
  }

  public getItem(key: string): string {
      if (this.isBrowser) {
          return localStorage.getItem(key);
      } else {
          return this.cookies[key];
      }
  }

  public getAllCookies() {
    return this.cookieService.getAll();
  }

  public key(index: number): string {
    if (this.isBrowser) {
      const
        values = [],
        keys = Object.keys(localStorage);
      let i = keys.length;

      while (i--) {
        values.push(localStorage.getItem(keys[i]));
      }

      return values[index];
    } else {
      return this.cookieService.getAll().propertyIsEnumerable[index];
    }
  }

  public removeItem(key: string): void {
    if (this.isBrowser) {
      localStorage.removeItem(key);
    } else {
      this.cookieService.remove(key);
    }
  }

  public removeByArrayOfKeys(keys: Array<string>): void {
    keys.map((key) => {
      this.cookieService.remove(key);
    });
  }

  public setItem(key: string, data: string): void {
    if (this.isBrowser) {
      localStorage.setItem(key, data);
    this.cookieService.put(key, data);
    } else {
        this.cookieService.put(key, data);
    }
  }

  public checkItem(key: string): boolean {
    if (this.isBrowser) {
      return !!localStorage.getItem(key);
    } else {
        return !!this.cookieService.get(key);
    }
  }

/*  public getCookieService(): CookieService {
    return this.cookieService;
  }*/
}
