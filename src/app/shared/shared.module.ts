import { ModuleWithProviders, NgModule } from '@angular/core';

import { TransferHttpModule } from '@gorniv/ngx-transfer-http';

import { LayoutsModule } from './layouts/layouts.module';
import { SharedMetaModule } from './shared-meta';
import { KikmoovFooterModule } from '@shared/layouts/kikmoov-footer/kikmoov-footer.module';
import { NavmenuModule } from '@shared/layouts/navmenu/navmenu.module';
import { WorkspaceModule } from '@shared/layouts/workspace/workspace.module';

@NgModule({
  exports: [
      LayoutsModule,
      SharedMetaModule,
      TransferHttpModule,
      KikmoovFooterModule,
      NavmenuModule,
      WorkspaceModule
  ],
  providers: [],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: SharedModule };
  }
}
