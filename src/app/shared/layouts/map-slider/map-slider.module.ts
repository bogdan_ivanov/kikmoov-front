import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { MapSliderComponent } from '@shared/layouts/map-slider/map-slider.component';
import { SliderWorkspaceModule } from '@shared/layouts/slider-workspace/slider-workspace.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      SliderWorkspaceModule
  ],
  declarations: [
      MapSliderComponent
  ],
    exports: [
        MapSliderComponent
    ]
})
export class MapSliderModule {}
