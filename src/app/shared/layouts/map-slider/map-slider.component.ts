import {Component, OnInit, Input, Inject} from '@angular/core';

@Component({
    selector: 'app-slider',
    templateUrl: './map-slider.component.html',
    styleUrls: ['./map-slider.component.scss']})

export class MapSliderComponent implements OnInit {

  @Input() slideList: string[];
  width: number = 280;
  activeIndex: number = 0;
  loading: boolean = true;

  constructor(@Inject('ORIGIN_URL') public baseUrl: string) {  }

  ngOnInit() {
    setTimeout(() => {
      this.loading = false;
    }, 6000);
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  slideLeft($event) {
      $event.stopPropagation();
      $event.preventDefault();
    if (this.activeIndex >= (this.slideList.length - 1)) {
      this.activeIndex = 0;
    } else {
      this.activeIndex++;
    }
  }

  slideRight($event) {
      $event.stopPropagation();
      $event.preventDefault();
    if (this.activeIndex == 0) {
      this.activeIndex = this.slideList.length - 1;
    } else {
      this.activeIndex--;
    }
  }
}
