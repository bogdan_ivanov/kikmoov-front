import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KmCheckboxComponent } from './km-checkbox.component';

describe('KmCheckboxComponent', () => {
  let component: KmCheckboxComponent;
  let fixture: ComponentFixture<KmCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KmCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KmCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
