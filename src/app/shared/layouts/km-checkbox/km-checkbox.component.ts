import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-km-checkbox',
  templateUrl: './km-checkbox.component.html',
  styleUrls: ['./km-checkbox.component.sass']
})
export class KmCheckboxComponent implements OnInit {
  @Input()
  elemetId;
  @Output()
  changed: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onChange(val, event) {
    this.changed.emit({ id: val, val: event });
  }
}
