import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ForgotPasswordComponent } from '@shared/layouts/forgot-password/forgot-password.component';
import { MatCardModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      MatCardModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      ForgotPasswordComponent
  ],
    exports: [
        ForgotPasswordComponent
    ]
})
export class ForgotPasswordModule {}
