import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import { MessageService } from './../../../services/message.service';
import { ErrorsService } from './../../../services/errors.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  @Output()
  action: EventEmitter<any> = new EventEmitter();
  public forgetFormGroup = this.fb.group({
    email: [null, [Validators.required]],
  });
  public success = false;
  public errMsg = '';

  constructor(
    private userServise: UserService,
    public fb: FormBuilder,
    private messageService: MessageService,
    private errorService: ErrorsService
  ) {}

  ngOnInit() {}

  onDoRecover() {
    if (this.forgetFormGroup.valid) {
      this.userServise.forgetPassword(this.forgetFormGroup.value).subscribe(
        resp => {
          this.messageService.setMessage(resp['message']);
          this.messageService.showMessage();
          this.messageService.clearMessage();
          this.action.emit('close');
        },
        err => {
          this.errorService.setErrors(err);
          this.errorService.showError();
          this.errorService.clearErrors();
        }
      );
    } else {
      this.success = false;
    }
  }
}
