import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PortalSidebarComponent } from '@shared/layouts/portal-sidebar/portal-sidebar.component';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { UserService } from '../../../services/user.service';
import { LocationService } from '../../../services/location.service';
import { NotificationService } from '../../../services/notification.service';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      NgxSmartModalModule,
      MatExpansionModule
  ],
  declarations: [
      PortalSidebarComponent
  ],
    exports: [
        PortalSidebarComponent
    ],
    providers: [
        LocationService,
        NgxSmartModalService,
        NotificationService,
        UserService
    ]
})
export class PortalSidebarModule {}
