import {Component, OnInit, Input, Output, EventEmitter, AfterViewInit, Inject, PLATFORM_ID} from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { UserService } from '../../../services/user.service';
import { LocationService } from '../../../services/location.service';
import { NotificationService } from '../../../services/notification.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {UniversalStorage} from '@shared/storage/universal.storage';
import {Subscription} from 'rxjs/Subscription';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-portal-sidebar',
  templateUrl: './portal-sidebar.component.html',
  styleUrls: ['./portal-sidebar.component.scss'],
})
export class PortalSidebarComponent implements OnInit, AfterViewInit {
  @Input()
  currentWorkSpaceId: number;
  @Input()
  set workspaceList(workspaceList: any) {
    this._workspaceList = workspaceList;
    if (workspaceList && workspaceList.length) {
      if (!this.currentWorkSpaceId) {
        this.currentWorkSpaceId = workspaceList[0]['id'];
      }
      if (this.universalStorage.getItem('active_location') !== null) {
        this.activeId = +this.universalStorage.getItem('active_location');
      } else {
        this.activeId = this.currentWorkSpaceId;
      }
    }
  }
  @Input()
  public notificationsCount$: Observable<number>;

  public notificationsCount = 0;
  public _workspaceList = [];

  subscription: Subscription;

  @Output()
  clicked: EventEmitter<any> = new EventEmitter();
  public activeId: any;

  public isBrowser;

  constructor(
    private locationService: LocationService,
    public ngxSmartModalService: NgxSmartModalService,
    private notificationService: NotificationService,
    private userService: UserService,
    private router: Router,
    private universalStorage: UniversalStorage,
    @Inject(PLATFORM_ID) private platformId
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit() {
    if (!this._workspaceList && this.router.url !== '/mainportal') {
      this.locationService.getLocations().subscribe(res => {
        this._workspaceList = res['locations'];
      });
    }
  }

  ngAfterViewInit() {
    if (this.isBrowser) {
        if (this.notificationsCount$) {
            this.subscription = this.notificationsCount$.subscribe(count => {
                this.notificationsCount = count;
            });
        } else {
            this.notificationService.getInternalNotifications().subscribe(res => {
                res['notifications'] = res['notifications'].filter((item) => item.status.includes('REQUESTED'));
                this.notificationsCount = res['notifications'].length;
            });
        }
    }
  }

  onWorkspaceSelect(id) {
    this.universalStorage.setItem('active_location', id);
    this.activeId = id;
    this.clicked.emit(id);
  }

  signOutModal() {
    this.ngxSmartModalService.getModal('confirmModal').open();
  }

  signOutModalClose() {
    this.ngxSmartModalService.getModal('confirmModal').close();
  }

  signOut(e) {
    e.preventDefault();
    this.userService.setLogoutAndRedirect();
  }
}
