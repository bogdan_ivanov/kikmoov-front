import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalLocationComponent } from './portal-location.component';

describe('PortalLocationComponent', () => {
  let component: PortalLocationComponent;
  let fixture: ComponentFixture<PortalLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
