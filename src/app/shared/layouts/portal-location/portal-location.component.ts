import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { LocationService } from '../../../services/location.service';
import { UserService } from '../../../services/user.service';
import { MessageService } from '../../../services/message.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { UniversalStorage } from '@shared/storage/universal.storage';

@Component({
  selector: 'app-portal-location',
  templateUrl: './portal-location.component.html',
  styleUrls: ['./portal-location.component.scss']
})
export class PortalLocationComponent implements OnInit {
  @Input()
  set workspaceList(workspaceList: any) {
    this.workspaceData = workspaceList;
    this.refreshTableData();
  }
  @Input()
  set workspaceId(workspaceId: any) {
    this._workspaceId = workspaceId;
    this.refreshTableData();
  }
  @Output()
  onWorkSpaceDelete = new EventEmitter();
  public _workspaceId;
  public curWorkspace;
  private workspaceData;

  constructor(
    private locationService: LocationService,
    private userService: UserService,
    private messageService: MessageService,
    public ngxSmartModalService: NgxSmartModalService,
    private universalStorage: UniversalStorage
  ) {}

  refreshTableData() {
    if (this.workspaceData != undefined && this._workspaceId != undefined) {
      try {
        this.workspaceData.forEach(element => {
          if (element['id'] == this._workspaceId) {
            this.curWorkspace = element;
            throw {};
          }
        });
      } catch (e) {}
    }
  }

  ngOnInit() {}

  confirmDeleting() {
    this.locationService.deleteLocation(this._workspaceId).subscribe(
      res => {
        this.messageService.setMessage('Location has been deleted successfully');
        this.messageService.showMessage();
        this.universalStorage.removeItem('active_location');
        this.onWorkSpaceDelete.emit(this._workspaceId);
        this.workspaceData = this.workspaceData.filter(location => location.id !== this._workspaceId);
        if (this.workspaceData.length) {
          this.curWorkspace = this.workspaceData[0];
        } else {
          this.curWorkspace = undefined;
          this._workspaceId = undefined;
        }
        this.refreshTableData();
        this.ngxSmartModalService.getModal('confirmLocationDelete').close();
      }, err => {
        console.log(err);
      }
    );
  }
}
