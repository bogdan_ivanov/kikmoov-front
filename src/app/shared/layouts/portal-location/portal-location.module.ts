import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { UserService } from '../../../services/user.service';
import { LocationService } from '../../../services/location.service';
import { NotificationService } from '../../../services/notification.service';
import { PortalLocationComponent } from '@shared/layouts/portal-location/portal-location.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      NgxSmartModalModule
  ],
  declarations: [
      PortalLocationComponent
  ],
    exports: [
        PortalLocationComponent
    ],
    providers: [
        LocationService,
        NgxSmartModalService,
        NotificationService,
        UserService
    ]
})
export class PortalLocationModule {}
