import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { KikmoovFooterComponent } from './kikmoov-footer.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { UserService } from '../../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

library.add(fas, far, fab);


@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule, FontAwesomeModule],
  declarations: [
      KikmoovFooterComponent
  ],
    exports: [
        KikmoovFooterComponent,
        FontAwesomeModule
    ],
    providers: [
        UserService,
        NgxSmartModalService
    ]
})
export class KikmoovFooterModule {}
