import {Component, Inject} from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { UserService } from '../../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-kikmoov-footer',
  templateUrl: './kikmoov-footer.component.html',
    styleUrls: ['./kikmoov-footer.component.scss']
})
export class KikmoovFooterComponent  {
    public year = (new Date()).getFullYear();

    constructor(
        @Inject('ORIGIN_URL') public baseUrl: string,
        private viewportScroller: ViewportScroller,
        public userService: UserService,
        public ngxSmartModalService: NgxSmartModalService
    ) { }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

    scrollToTop(anchor) {
        this.viewportScroller.scrollToAnchor(anchor);
    }

    login() {
        this.ngxSmartModalService.getModal('authModal')['showDlg'] = 'login';
        this.ngxSmartModalService.getModal('authModal').open();
    }

}
