import {Component, OnInit, Input, Inject, PLATFORM_ID} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-post-space-bar',
  templateUrl: './post-space-bar.component.html',
  styleUrls: ['./post-space-bar.component.scss']
})
export class PostSpaceBarComponent implements OnInit {
  @Input()
  disableHomePage = false;

  showMenu;

  public isUploading = false;
    private isBrowser;

  constructor(
      @Inject('ORIGIN_URL') public baseUrl: string,
      private router: Router,
      @Inject(PLATFORM_ID) private platformId
  ) {
        this.isBrowser = isPlatformBrowser(platformId);
      if (this.isBrowser) {
          this.isUploading = this.router.url.includes('upload-location');
      }
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
      if (this.isBrowser) {
          this.router.events.subscribe(e => {
              if (e instanceof NavigationEnd) {
                  if (e.url && e.url.includes('upload-location') ||
                      e.urlAfterRedirects && e.urlAfterRedirects.includes('upload-location')) {
                      this.isUploading = true;
                  } else {
                      this.isUploading = false;
                  }
              }
          });
      }
  }
}
