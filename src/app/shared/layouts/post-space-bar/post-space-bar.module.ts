import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {PostSpaceBarComponent} from '@shared/layouts/post-space-bar/post-space-bar.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule
    ],
    declarations: [
        PostSpaceBarComponent
    ],
    exports: [
        PostSpaceBarComponent
    ]
})
export class PostSpaceBarModule {}
