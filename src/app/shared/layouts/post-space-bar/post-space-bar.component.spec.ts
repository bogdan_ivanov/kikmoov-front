import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSpaceBarComponent } from './post-space-bar.component';

describe('PostSpaceBarComponent', () => {
  let component: PostSpaceBarComponent;
  let fixture: ComponentFixture<PostSpaceBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSpaceBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSpaceBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
