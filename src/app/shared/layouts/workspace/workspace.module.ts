import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { WorkspaceComponent } from './workspace.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      SlickCarouselModule
  ],
  declarations: [
      WorkspaceComponent
  ],
    exports: [
        WorkspaceComponent
    ]
})
export class WorkspaceModule {}
