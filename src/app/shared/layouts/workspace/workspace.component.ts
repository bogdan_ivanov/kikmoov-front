import {
    AfterViewInit,
    Component,
    EventEmitter, Inject,
    Input,
    OnInit,
    Output, PLATFORM_ID,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ProfileService } from '../../../services/profile.service';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent implements OnInit, AfterViewInit {
  @Input()
  workSpace;

  @Input()
  classes = '';

  @Output()
  onWorkSpaceLike = new EventEmitter();

  @Output()
  onShowSlider = new EventEmitter();

  @ViewChild('slider') slider;

  public fsSliderConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true
  };
  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office',
  };

  public deskTypeNames = {
    hourly_hot_desk: 'Desk',
    monthly_hot_desk: 'Hot Desk',
    monthly_fixed_desk: 'Fixed Desk',
    daily_hot_desk: 'Daily Desk'
  };

  public meetingRoomTypeNames = {
    hourly_meeting_room: 'Hourly Meeting Space',
    daily_meeting_room: 'Daily Meeting Space'
  };

  private excludeNavigateList = [
    'topticks-info-wrapper-like',
    'slide',
    'slick-prev slick-arrow',
    'slick-next slick-arrow',
    'fa fa-heart-o',
    'fa selected fa-heart',
    'fa fa-heart',
    'fs-slider'
  ];

    public isBrowser;

  constructor(private router: Router,
              private userService: UserService,
              private profileService: ProfileService,
              public ngxSmartModalService: NgxSmartModalService,
              @Inject(PLATFORM_ID) private platformId,
              @Inject('ORIGIN_URL') public baseUrl: string
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
  }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
    if (this.workSpace && this.workSpace['workspace'].type === 'desk') {
      this.prepareDesk();
    }
  }

  ngAfterViewInit(): void {
    if (this.isBrowser) {
        this.onShowSlider.emit(false);
        setTimeout(() => {
            this.slider.unslick();
            this.slider.initSlick();
            this.onShowSlider.emit(true);
        }, 500);
    }
  }

  openDetails(event, workSpaceId, relatedId?) {
    if (this.excludeNavigateList.includes(event.target.className)) {
      return;
    } else {
      this.excludeNavigateList.forEach(item => {
        if (event.target.closest(item)) {
          return;
        }
      });
    }
    const related = relatedId ? {queryParams: {relatedId}} : {};
    // const url = this.router.createUrlTree(['/indepth', workSpaceId], related).toString();
    // window.open(url, '_blank');
    this.router.navigate(['/indepth', workSpaceId], related);
  }

  prepareDesk() {
    const workSpace = {...this.workSpace['workspace']};
    if (workSpace.deskType !== 'hourly_hot_desk') {
      const hourlyHotDesk = this.workSpace['related'].find(item => item.deskType === 'hourly_hot_desk');
      if (hourlyHotDesk) {
        this.workSpace['workspace'] = hourlyHotDesk;
        this.workSpace['related'] = this.workSpace['related'].filter(item => item.deskType !== 'hourly_hot_desk');
        this.workSpace['related'].push(workSpace);
      } else {
        const monthlyHotDesk = this.workSpace['related'].find(item => item.deskType === 'monthly_hot_desk');
        if (monthlyHotDesk) {
            this.workSpace['workspace'] = monthlyHotDesk;
            this.workSpace['related'] = this.workSpace['related'].filter(item => item.deskType !== 'monthly_hot_desk');
            this.workSpace['related'].push(workSpace);
        }
      }
    }
    if (this.workSpace['related'].length > 1 && this.workSpace['related'][0].deskType !== 'monthly_hot_desk') {
      this.workSpace['related'].reverse();
    }
  }

  handleLikeWorkSpace(workSpace) {
    if (!this.userService.isLoggedin()) {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return;
    } else if (
      this.userService.isLoggedin() &&
      this.userService.getUserType() === 'seller') {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['message'] = true;
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return;
    }
    const likeState = !workSpace['workspace'].isLiked;
    workSpace['workspace'].isLiked = likeState;
    this.profileService.likeWorkSpace(workSpace['workspace'].id).subscribe(res => {
      workSpace['workspace'].isLiked = likeState;
      this.onWorkSpaceLike.emit(workSpace['workspace']);
    });
  }

  trackByFn(index, item) {
    return index; // or item.id
  }

}
