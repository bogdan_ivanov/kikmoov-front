import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SignupComponent } from '@shared/layouts/signup/signup.component';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule,
      MatCardModule
  ],
  declarations: [
      SignupComponent
  ],
    exports: [
        SignupComponent
    ]
})
export class SignupModule {}
