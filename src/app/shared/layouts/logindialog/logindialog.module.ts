import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { LogindialogComponent } from '@shared/layouts/logindialog/logindialog.component';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatToolbarModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorsService } from '../../../services/errors.service';
import { ProfileService } from '../../../services/profile.service';
import { OauthService } from '../../../services/oauth.service';
import { AuthServiceConfig } from 'angularx-social-login';
import { getAuthServiceConfigs } from '../../../configs/auth-service-config';
import {ForgotPasswordModule} from '@shared/layouts/forgot-password/forgot-password.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      PostSpaceBarModule,
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatProgressSpinnerModule,
      MatExpansionModule,
      MatNativeDateModule,
      MatDatepickerModule,
      MatSelectModule,
      MatSnackBarModule,
      FormsModule,
      ReactiveFormsModule,
      ForgotPasswordModule
  ],
  declarations: [
      LogindialogComponent
  ],
  exports: [
      LogindialogComponent
  ],
  providers: [
      ErrorsService,
      ProfileService,
      OauthService,
      {
          provide: AuthServiceConfig,
          useFactory: getAuthServiceConfigs
      },
  ]
})
export class LogindialogModule {}
