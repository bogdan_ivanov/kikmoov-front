import {Component, OnInit, Input, Output, EventEmitter, OnDestroy, Inject} from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { environment } from '../../../environments/environment';
import { ErrorsService } from '../../../services/errors.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';
import { OauthService } from '../../../services/oauth.service';
import { UniversalStorage } from '@shared/storage/universal.storage';

@Component({
  selector: 'app-logindialog',
  templateUrl: './logindialog.component.html',
  styleUrls: ['./logindialog.component.scss']
})
export class LogindialogComponent implements OnInit, OnDestroy {
  public apiUrl = 'http://kikmoov.local';
  // public apiUrl = environment.apiUrl;
  public linkedInOauthLink = '';
  public errMsg = '';
  public buyerMessage = null;
  public loading = false;
  @Input()
  linkedInCode = '';
  @Input()
  modal = false;
  @Input()
  forgotPasswordModal = true;
  @Output()
  action: EventEmitter<any> = new EventEmitter();

  private returnUri;

  constructor(
    private userService: UserService,
    private errorsService: ErrorsService,
    public fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService,
    private socialAuthService: AuthService,
    private oauthService: OauthService,
    @Inject('ORIGIN_URL') public baseUrl: string,
    private universalStorage: UniversalStorage
  ) {}

  public loginFormGroup = this.fb.group({
    email: [
        null,
        [
            Validators.minLength(1),
            Validators.required,
            Validators.email
        ]
    ],
    password: [
        null,
        [
            Validators.minLength(8),
            Validators.required
        ]
    ]
  });

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

  ngOnInit() {
    if (this.errorsService.hasErrors()) {
      this.errorsService.showError();
      this.errorsService.clearErrors();
    }
    this.linkedInOauthLink = this.oauthService.getLinkedInAuthLink();
    if (this.modal) {
      this.setMessage();
      this.ngxSmartModalService.get('authModal').onAnyCloseEvent.subscribe(() => {
        delete this.ngxSmartModalService.get('authModal')['guest_login_as_seller'];
        this.buyerMessage = null;
      });
      this.ngxSmartModalService.get('authModal').onOpen.subscribe(() => {
        this.setMessage();
      });
    }
    if (this.userService.isLoggedin()) {
      this.setMessage();
    }
    this.route.queryParams.subscribe(params => {
      if ('returnUri' in params) {
        this.returnUri = params['returnUri'];
      }
    })
  }

  ngOnDestroy() {
    this.buyerMessage = null;
  }

  setMessage(isSeller?: boolean, isBuyer?: boolean) {
    if ((this.userService.isLoggedin() && this.userService.getUserType() === 'buyer') ||
        (isBuyer)) {
      this.buyerMessage = [
        'This email is not linked to a Post a Space account.',
        'If you wish to post a space, please create an account with another email address.'
      ];
    }
    if ((this.userService.isLoggedin() &&
      this.userService.getUserType() === 'seller' &&
      this.ngxSmartModalService.get('authModal')['message']) ||
      (isSeller)) {
      this.buyerMessage = [
        'This email is not linked to a Find a Space account.',
        'If you wish to find a space, please create an account with another email address.'
      ];
      delete this.ngxSmartModalService.get('authModal')['message'];
    }
  }

  onDoLogin(event) {
    this.errMsg = '';
    if (this.loginFormGroup.status === 'VALID') {
      this.userService.doLogin(this.loginFormGroup.value).subscribe(
        res => {
          if (res['client_id'] && res['client_secret']) {
            this.userService
              .getAccessToken(res, this.loginFormGroup.value)
              .subscribe(
                result => {
                  result['accountType'] = res['accountType'];
                  result['client_id'] = res['client_id'];
                  result['client_secret'] = res['client_secret'];
                  if (this.modal) {
                    if (this.ngxSmartModalService.get('authModal')['guest_login_as_seller'] && result['accountType'] === 'seller') {
                      this.setMessage(true);
                      return;
                    } else if (this.ngxSmartModalService.get('authModal')['guest_login_as_seller'] && result['accountType'] === 'buyer') {
                      this.userService.doUserAuth(result);
                      this.universalStorage.setItem(
                        'email',
                        this.loginFormGroup.value.email
                      );
                      this.action.emit({ action: 'close', loggedIn: true });
                      return;
                    }
                  } else if (!this.modal && result['accountType'] === 'buyer') {
                    this.setMessage(false, true);
                    return;
                  }
                  this.userService.doUserAuth(result);
                  this.universalStorage.setItem(
                    'email',
                    this.loginFormGroup.value.email
                  );

                  if (result['accountType'] === 'seller') {
                    if (this.returnUri) {
                      this.router.navigate([this.returnUri]);
                    } else {
                      this.router.navigate(['/mainportal']);
                    }
                  } else {
                    this.action.emit({ action: 'close', loggedIn: true });
                  }
                },
                err => {
                  console.log(err);
                  this.errMsg = err.message;
                }
              );
          }
        },
        err => {
          console.log(err);
          this.errMsg = err.message;
        }
      );
    } else {
      if (
        'email' in this.loginFormGroup.controls.email.errors &&
        !('required' in this.loginFormGroup.controls.email.errors)
      ) {
        this.errMsg = 'Email is incorrect';
      } else {
        this.errMsg = 'Email or password fields are blank';
      }
    }
  }

  socialSignIn(e, socialPlatform: string) {
    e.preventDefault();
    this.loading = true;
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService
      .signIn(socialPlatformProvider)
      .then(userData => {
        if (userData) {
          const provider =
            socialPlatformProvider.toLowerCase() === 'facebook'
              ? 'fb'
              : 'google';
          this.oauthService.doSocialAuth(userData, provider).subscribe(res => {
            if (res) {
              this.oauthService.authenticate(res, this.action, {
                guest_login_as_seller: this.ngxSmartModalService.get('authModal')['guest_login_as_seller'],
                setMessage: this.setMessage.bind(this),
                login: this
              });
            }
          }, err => {
            console.log('Error', err);
            this.loading = false;
            this.errorsService.showError(err);
          });
        }
      })
      .catch(err => {
        console.log('AuthError: ', err);
        this.loading = false;
      });
  }

  openForgotPasswordDialog() {
    if (this.modal) {
      this.action.emit('recover');
    } else {
      this.ngxSmartModalService.getModal('recover').open();
    }
  }
}
