import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {UniversalStorage} from '@shared/storage/universal.storage';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit {
  public confirmed = false;
  public isBrowser;

  constructor(private universalStorage: UniversalStorage, @Inject(PLATFORM_ID) private platformId) {
    this.isBrowser = isPlatformBrowser(platformId);

    if (this.isBrowser) {
      if (localStorage.getItem('confirmed_cookies')) {
        this.confirmed = true;
      }
    }
  }

  ngOnInit() {
  }

  confirm() {
    if (this.isBrowser) {
      localStorage.setItem('confirmed_cookies', '1');
    }
    this.confirmed = true;
  }
}
