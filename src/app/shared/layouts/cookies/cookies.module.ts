import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CookiesComponent } from '@shared/layouts/cookies/cookies.component';


@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule],
  declarations: [
      CookiesComponent
  ],
    exports: [
        CookiesComponent
    ]
})
export class CookiesModule {}
