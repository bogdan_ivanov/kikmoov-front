import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ProfileService } from '../../../services/profile.service';

@Component({
  selector: 'app-slider-workspace',
  templateUrl: './slider-workspace.component.html',
  styleUrls: ['./slider-workspace.component.scss']
})
export class SliderWorkspaceComponent implements OnInit {
  @Input()
  workSpace;

  @Input()
  classes = '';

  @Output()
  onWorkSpaceLike = new EventEmitter();

  @ViewChild('slider') slider;

  public workSpaceTypes = {
    desk: 'Desk',
    'meeting-room': 'Meeting Space',
    'private-office': 'Private Office',
  };

  public deskTypeNames = {
    hourly_hot_desk: 'Desk',
    monthly_hot_desk: 'Hot Desk',
    monthly_fixed_desk: 'Fixed Desk',
    daily_hot_desk: 'Daily Desk'
  };

  public meetingRoomTypeNames = {
      hourly_meeting_room: 'Hourly Meeting Space',
      daily_meeting_room: 'Daily Meeting Space'
  };

  private excludeNavigateList = [
    'topticks-info-wrapper-like',
    'slide',
    'slick-prev slick-arrow',
    'slick-next slick-arrow',
    'fa fa-heart-o',
    'fa selected fa-heart',
    'fa fa-heart',
    'fs-slider'
  ];

  constructor(private router: Router,
              private userService: UserService,
              private profileService: ProfileService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    if (this.workSpace && this.workSpace['workspace'].type === 'desk') {
      this.prepareDesk();
    }
  }

  openDetails(event, workSpaceId, relatedId?) {
    if (this.excludeNavigateList.includes(event.target.className)) {
      return;
    } else {
      this.excludeNavigateList.forEach(item => {
        if (event.target.closest(item)) {
          return;
        }
      });
    }
    const related = relatedId ? {queryParams: {relatedId}} : {};
    this.router.navigate(['/indepth', workSpaceId], related);
  }

  prepareDesk() {
    const workSpace = {...this.workSpace['workspace']};
    if (workSpace.deskType !== 'hourly_hot_desk') {
      const hourlyHotDesk = this.workSpace['related'].find(item => item.deskType === 'hourly_hot_desk');
      if (hourlyHotDesk) {
        this.workSpace['workspace'] = hourlyHotDesk;
        this.workSpace['related'] = this.workSpace['related'].filter(item => item.deskType !== 'hourly_hot_desk');
        this.workSpace['related'].push(workSpace);
      }
    }
    if (this.workSpace['related'].length > 1 && this.workSpace['related'][0].deskType !== 'monthly_hot_desk') {
      this.workSpace['related'].reverse();
    }
  }

  handleLikeWorkSpace(workSpace) {
    if (!this.userService.isLoggedin()) {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return;
    } else if (
      this.userService.isLoggedin() &&
      this.userService.getUserType() === 'seller') {
      const authModal = this.ngxSmartModalService.getModal('authModal');
      authModal['showDlg'] = 'login';
      authModal['message'] = true;
      authModal['guest_login_as_seller'] = true;
      authModal.open();
      return;
    }
    const likeState = !workSpace['workspace'].isLiked;
    workSpace['workspace'].isLiked = likeState;
    this.profileService.likeWorkSpace(workSpace['workspace'].id).subscribe(res => {
      workSpace['workspace'].isLiked = likeState;
      this.onWorkSpaceLike.emit(workSpace['workspace']);
    });
  }

}
