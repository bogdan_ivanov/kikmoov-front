import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderWorkspaceComponent } from './workspace.component';

describe('WorkspaceComponent', () => {
  let component: SliderWorkspaceComponent;
  let fixture: ComponentFixture<SliderWorkspaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderWorkspaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
