import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { NavmenuComponent } from '@shared/layouts/navmenu/navmenu.component';
import { PostSpaceBarModule } from '@shared/layouts/post-space-bar/post-space-bar.module';
import { LogindialogModule } from '@shared/layouts/logindialog/logindialog.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { UserService } from '../../../services/user.service';
import { ErrorsService } from '../../../services/errors.service';
import { ProfileService } from '../../../services/profile.service';
import { OauthService } from '../../../services/oauth.service';
import { AuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { getAuthServiceConfigs } from '../../../configs/auth-service-config';
import { ForgotPasswordModule } from '@shared/layouts/forgot-password/forgot-password.module';
import { SignupModule } from '@shared/layouts/signup/signup.module';
import { MatCardModule } from '@angular/material';
import { ClickoutsideDirective } from '../../../derective/clickoutside.directive';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      PostSpaceBarModule,
      LogindialogModule,
      NgxSmartModalModule.forRoot(),
      SocialLoginModule,
      ForgotPasswordModule,
      SignupModule,
      MatCardModule
  ],
  declarations: [
      NavmenuComponent,
      ClickoutsideDirective
  ],
    exports: [
        NavmenuComponent
    ],
    providers: [
        UserService,
        ErrorsService,
        ProfileService,
        OauthService,
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
        },
    ],
})
export class NavmenuModule {}
