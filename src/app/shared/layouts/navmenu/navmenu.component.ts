import { Component, Inject, Input, OnInit, PLATFORM_ID, Renderer } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { OauthService } from '../../../services/oauth.service';
import { ProfileService } from '../../../services/profile.service';
import { Observable } from 'rxjs/Observable';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements OnInit {

    @Input()
    filled: boolean;
    @Input()
    scrollable = true;
    @Input()
    userName: Observable<{firstname: string, lastname: string}>;

    public scrolled = false;
    public showMenu = false;
    public showDropdown = false;
    public isLoggedIn = false;
    public linkedInCode = '';
    public accountType;
    public userDetails;
    private isBrowser;

    constructor(
        private renderer: Renderer,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private profileService: ProfileService,
        public ngxSmartModalService: NgxSmartModalService,
        private oauthService: OauthService,
        @Inject('ORIGIN_URL') public baseUrl: string,
        @Inject(PLATFORM_ID) private platformId
    ) {
        this.isBrowser = isPlatformBrowser(platformId);
        if (this.isBrowser) {
            this.route.queryParams.subscribe(params => {
                window.scrollTo(0, 0);
                if ('showlogin' in params) {
                    this.ngxSmartModalService.getModal('authModal')['showDlg'] = 'login';
                }
                if ('code' in params) {
                    this.oauthService
                        .doLinkedInAuth({code: params['code']})
                        .subscribe(res => {
                            if (res) {
                                this.oauthService.authenticate(res, this.signinHadler.bind(this));
                            }
                        });
                }
            });
        }
        this.isLoggedIn = this.userService.isLoggedin();

        this.accountType = this.userService.getUserType();

        if (!this.userService.getUserData() && this.isLoggedIn) {
            this.profileService.getUserDetails().subscribe(
                res => {
                    this.userService.setUserData(res);
                }
            );
        }

        this.userService.userSubject.subscribe(userData => {
            this.userDetails = userData;
        });

    }

    public getAbsoluteUrl(path: string) {
        return this.baseUrl + path;
    }

    ngOnInit() {
        this.renderer.listenGlobal('document', 'scroll', evt => {
            if ('path' in evt) {
                this.scrolled = (evt.path[1].pageYOffset != 0);
            } else {
                this.scrolled = (evt.pageY != 0);
            }
            if (this.showMenu && this.scrolled) {
                this.showMenu = false;
            }
        });
        this.renderer.listenGlobal('document', 'click', evt => {
            if (this.showMenu && evt.target &&
                !evt.target.closest('.show-mobile-menu') &&
                !evt.target.closest('.toggle-menu-button')) {
                this.showMenu = false;
            }
        });
        if (this.userName) {
            this.userName.subscribe(user => {
                if (this.userDetails) {
                    this.userDetails.firstname = user.firstname;
                    this.userDetails.lastname = user.lastname;
                }
            });
        }
    }

    showLoginDlg() {
        this.ngxSmartModalService.getModal('authModal').open();
        this.ngxSmartModalService.getModal('authModal')['showDlg'] = 'login';
    }

    signinHadler(event) {
        if (event === 'close') {
            this.ngxSmartModalService.getModal('authModal').close();
        } else if (event.action === 'close' && event.loggedIn) {
            this.ngxSmartModalService.getModal('authModal').close();
            this.isLoggedIn = event.loggedIn;
            this.profileService.getUserDetails().subscribe(
                res => {
                    this.userService.setUserData(res);
                }
            );
        }

        this.accountType = this.userService.getUserType();
        this.ngxSmartModalService.getModal('authModal')['showDlg'] = event;
    }

    signOutModal() {
        this.ngxSmartModalService.getModal('confirmModal').open();
    }

    signOutModalClose() {
        this.ngxSmartModalService.getModal('confirmModal').close();
    }

    tryToLogut() {
        this.isLoggedIn = false;
        this.userService.signOut();
        this.ngxSmartModalService.getModal('confirmModal').close();
    }
}
