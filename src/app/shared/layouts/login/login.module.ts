import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import {PostSpaceBarModule} from '@shared/layouts/post-space-bar/post-space-bar.module';
import {LoginComponent} from '@shared/layouts/login/login.component';
import { LogindialogModule } from '@shared/layouts/logindialog/logindialog.module';
import {MatCardModule} from '@angular/material';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      PostSpaceBarModule,
      LogindialogModule,
      MatCardModule
  ],
  declarations: [
      LoginComponent
  ],
    exports: [
        LoginComponent
    ]
})
export class LoginModule {}
