import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public confirm = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userServise: UserService
  ) {
    this.route.params.subscribe(params => {
      if ('key' in params) {
        this.confirm = true;
        this.confirmUserByKey(params['key']);
      } else if ('token' in params) {
        this.verifyToken(params['token']);
      }
    });
  }

  verifyToken(token) {
    this.userServise.doSocialAuth(token).subscribe(res => {
      this.userServise.doUserAuth(res);
      this.router.navigate(['/mainportal']);
    }, err => {
      console.log(err);
    });
  }

  confirmUserByKey(key) {
    this.userServise.confirmUser(key).subscribe(
      res => {
        if (res['type'] == 'buyer') {
          this.router.navigate(['/'], {
            queryParams: {
              showlogin: true
            }
          });
        } else {
          setTimeout(() => {
            this.confirm = false;
          }, 2000);
        }
      },
      err => {
        //todo: add error message
      }
    );
  }

  ngOnInit() {}
}
