import { NgModule } from '@angular/core';
import { MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

export function metaFactory(translate: TranslateService): MetaLoader {
  return new MetaStaticLoader({
    callback: (key: string): Observable<string | Object> => translate.get(key),
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: ' | ',
    applicationName: 'Kikmoov',
    defaults: {
      title: 'Kikmoov: Find Your Perfect Office Space To Rent',
      description: 'Looking for a workspace? We help freelancers, start-ups, scale ups and established companies, search and book coworking spaces, private offices, serviced office space and meeting rooms. Book viewings and workspaces directly on Kikmoov.',
      'og:site_name': 'Kikmoov',
      'og:type': 'website',
      'og:locale': 'en-US',
      'og:locale:alternate': [
        { code: 'en', name: 'English', culture: 'en-US' },
      ]
        .map((lang: any) => lang.culture)
        .toString(),
    },
  });
}

@NgModule({
  imports: [
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: metaFactory,
      deps: [TranslateService],
    }),
  ],
})
export class SharedMetaModule {}
