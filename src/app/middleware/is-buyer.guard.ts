import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../services/user.service';

@Injectable()
export class BuyerGuard implements CanActivate {
  constructor(private router: Router, private user: UserService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.user.getUserType() === 'buyer' || !this.user.isLoggedin()) {
      return true;
    } else {
      this.user.setLogoutAndRedirect();
      return false;
    }
  }
}
