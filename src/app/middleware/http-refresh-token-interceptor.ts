import {Inject, Injectable, Injector, PLATFORM_ID} from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import {UserService} from '../services/user.service';
import {finalize} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class HttpRefreshTokenInterceptor implements HttpInterceptor {
  readonly http: HttpClient;
  readonly auth: UserService;
  private refreshInProgress: boolean;
  private requests: CallerRequest[] = [];
  public isBrowser;

  constructor(private injector: Injector, @Inject(PLATFORM_ID) private platformId) {
    if (!this.http) {
      this.http = injector.get(HttpClient);
      this.auth = injector.get(UserService);
    }
      this.isBrowser = isPlatformBrowser(platformId);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.includes('api/')) {
      return next.handle(req);
    }

    return new Observable<HttpEvent<any>>(
      subscriber => {
        const originalRequestSubscription =
          next
            .handle(req)
            .subscribe(
              response => {
                  subscriber.next(response);
                },
                err => {
                    if (this.isBrowser) {
                        if ((err.status === 401 && err.error === 'access_denied') || (err.status === 401 && err.error === 'invalid_grant' && this.auth.isLoggedin())) {
                            console.log('401');
                            this.handleUnauthorizedError(subscriber, req);
                        } else {
                            console.log('errrrrrrr1');
                            subscriber.error(err);
                        }
                    }
                },
              () => {
                subscriber.complete();
              });
        return () => {
          originalRequestSubscription.unsubscribe();
        };
    });
  }

  private handleUnauthorizedError(subscriber: Subscriber<any>, request: HttpRequest<any>) {
    return this.auth.setLogoutAndRedirect();
    this.requests.push({subscriber, failedRequest: request});

    if (!this.refreshInProgress) {
      this.refreshInProgress = true;
      this.auth.refreshToken()
        .subscribe(
          (res) => {
            return this.auth.setLogoutAndRedirect();
          },
          (error) => {
            return this.auth.setLogoutAndRedirect(error);
          }
        )
        .add(() => {
          this.refreshInProgress = false;
        });
    }
  }

  private repeatFailedRequests(authHeader) {
    console.log('repeatFailedRequests');
    this.requests.forEach((c) => {
      const requestWithNewToken = c.failedRequest.clone({
        headers: c.failedRequest.headers.set('Authorization', `Bearer ${authHeader}`)
      });
      this.repeatRequest(requestWithNewToken, c.subscriber);
    });
    this.requests = [];
  }

  private repeatRequest(requestWithNewToken: HttpRequest<any>, subscriber: Subscriber<any>) {
    console.log('repeatRequest');
    this.http.request(requestWithNewToken).subscribe((res) => {
        subscriber.next(res);
      },
      err => {
        subscriber.error(err);
      },
      () => {
        subscriber.complete();
      });
  }
}

interface CallerRequest {
  subscriber: Subscriber<any>;
  failedRequest: HttpRequest<any>;
}
