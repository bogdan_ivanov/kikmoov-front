import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot, CanLoad, Route
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class SellerGuard implements CanActivate {
  constructor(private router: Router, private user: UserService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.user.getUserType() === 'seller') {
      return true;
    } else {
      this.router.navigate(['/signin/seller'], {queryParams: {
          returnUri: state.url
        }});
      return false;
    }
  }
}
