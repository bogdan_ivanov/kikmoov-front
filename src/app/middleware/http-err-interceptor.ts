import { ErrorsService } from '../services/errors.service';
import {Inject, Injectable, Injector, PLATFORM_ID} from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { UserService } from '../services/user.service';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class HttpErrInterceptor implements HttpInterceptor {

  public isBrowser;

  constructor(
    private router: Router,
    private user: UserService,
    private errors: ErrorsService,
    @Inject(PLATFORM_ID) private platformId
  ) {
      this.isBrowser = isPlatformBrowser(platformId);
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .retry(1)
      .map(resp => {
        return resp;
      })
      .catch(err => {
        if (
          ((err.status === 403 && this.user.isLoggedin()) || (err.status === 401 && this.user.isLoggedin()))
        ) {
          this.user.setLogoutAndRedirect(err.error);
          return Observable.throw(this.errors.getErrors());
        } else {
          console.log('errr ', err);
          return Observable.throw({...err.error, status: err.status});
        }
      });
  }
}
