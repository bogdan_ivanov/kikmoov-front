import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../services/user.service';

import 'rxjs/add/operator/do';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpHeaderInterceptor implements HttpInterceptor {
  constructor(private user: UserService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.user.isLoggedin() && req.url.includes(environment.apiUrl)) {
      const request = req.clone({
        headers: req.headers.set(
          'Authorization',
          `Bearer ${this.user.getToken()}`
        ),
      });
      return next.handle(request);
    } else {
      return next.handle(req);
    }
  }
}
