import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ContentService } from '../services/content.service';
import { BlogService } from '../services/blog.service';
import { LocationService } from '../services/location.service';

@Injectable()
export class HomePageResolver implements Resolve<any> {

    constructor(
        private http: HttpClient,
        private contentService: ContentService,
        private blogService: BlogService,
        private locationService: LocationService
    ) { }

    resolve() {
        return  Observable.forkJoin ([
            this.contentService.getTestimonials(),
            this.blogService.getCategory('news-room'),
            this.locationService.getTopWorkSpaces()
        ])
            .map(results => ({
                testimonials: results[0],
                blogArticles : results[1],
                topWorkspaces : results[2]
            }))
            .catch(error => {
                return Observable.throw(error);
            });

    }
}