import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as moment from 'moment-timezone';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class FilterService {
  private apiUrl = environment.apiUrl;
  private searchUri = 'api/search';
  private suggestionsUri = 'api/suggestion';
  private priceRangeUrl = 'api/price-range';

  private sesstionToken;
  // private sesstionToken = new google.maps.places.AutocompleteSessionToken();
  private isBrowser;
  public date: Date;

  constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId) {
      this.isBrowser = isPlatformBrowser(platformId);
      if (this.isBrowser) {
          this.sesstionToken = new google.maps.places.AutocompleteSessionToken();
      }
  }

  filter(data) {
    data = this.serializeData(data);
    return this.http.request('GET', `${this.apiUrl}${this.searchUri}`, {params: data});
  }

  getSuggestions(address) {
    const sessionToken = this.sesstionToken['Pf'];
    return this.http.get(
      `${this.apiUrl}${this.suggestionsUri}?address=${encodeURIComponent(address)}&sessiontoken=${sessionToken}`
    );
  }

  serializeData(data) {
    if ('facilities' in data && typeof data['facilities'] === 'string') {
      data['facilities'] = JSON.parse(data['facilities']);
      data['facilities'].forEach((item, key) => {
        data[`facilities[${key}]`] = item;
      });
      delete data['facilities'];
    } else if ('facilities' in data && Array.isArray(data['facilities'])) {
      data['facilities'].forEach((item, key) => {
        data[`facilities[${key}]`] = item;
      });
      delete data['facilities'];
    }

    if (data['availableFrom'] && data['availableFrom'] instanceof Date) {
      data['availableFrom'] = this.convertToUtc(data['availableFrom']);
      data['availableFrom'] = moment(data['availableFrom'].getTime()).tz('Europe/London');
      data['availableFrom'] = data['availableFrom'].unix();
    }
    if (data['availableFrom'] && data['availableFrom'] instanceof moment) {
      data['availableFrom'] = this.convertToUtc(data['availableFrom']);
      data['availableFrom'] = moment(data['availableFrom'].getTime()).unix();
    }
    Object.keys(data).forEach((key) => {
      if (!data[key]) {
        delete data[key];
      }
    });
    return new HttpParams({fromObject: data});
  }

  getPriceRange() {
    return this.http.get(this.apiUrl + this.priceRangeUrl);
  }

  convertToUtc(date, time?: number): Date | number {
        if (date instanceof moment) {
            date = moment(date).toDate();
            // this.date = new Date(date.toString());
            // this.date = date.toDate();
        }
        if (time) {
            const tzOffset = Math.abs(new Date().getTimezoneOffset());
            let startDate = time;
            startDate = (startDate - (tzOffset * 60000));
            return new Date(startDate).getTime();
        } else {
            date.setHours(0, 0);
            const tzOffset = Math.abs(date.getTimezoneOffset());
            let startDate = date.getTime();
            startDate = (startDate + (tzOffset * 60000));
            return new Date(startDate);
        }
  }
}
