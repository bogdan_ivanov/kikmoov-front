import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {
  private apiUrl = environment.apiUrl;
  public getInternalNotificationsUri = 'api/seller/internal-notifications';
  public deleteInternalNotificationsUri = 'api/seller/internal-notifications';
  public acceptNotificationUri = 'api/seller/workspace/$id/$type-status';

  constructor(private http: HttpClient) { }

  getInternalNotifications() {
    return this.http.get(this.apiUrl + this.getInternalNotificationsUri);
  }

  deleteInternalNotifications(ids) {
    return this.http.request(
      'DELETE',
      this.apiUrl + this.deleteInternalNotificationsUri,
      {
        body: {ids}
      }
    );
  }

  acceptNotification({id, type, workspaceId, status}) {
    return this.http.patch(this.apiUrl + this.acceptNotificationUri
      .replace('$id', workspaceId)
      .replace('$type', type), {
      [type + 'Id']: id,
      status
    });
  }

}
