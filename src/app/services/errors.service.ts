import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable()
export class ErrorsService {
  public error: { message?: string } = {};
  constructor(private snackBar: MatSnackBar) {}

  hasErrors() {
    return Object.keys(this.error).length;
  }

  setErrors(error: { message?: string }) {
    this.error = { ...error };
    return this;
  }

  getErrors() {
    return this.error;
  }

  clearErrors() {
    this.error = {};
  }

  showError(error?) {
    if (error) this.setErrors(error);
    this.snackBar.open(this.error.message, 'OK', {
      duration: 5000,
      panelClass: 'error-snackbar',
    });
    this.clearErrors();
  }

  setFieldErrors(err, form: FormGroup) {
    if (err.hasOwnProperty('errors')) {
      err['errors'].forEach(error => {
        form.get(error['attribute']).markAsTouched();
        form.get(error['attribute']).setErrors({[`${error['attribute']}-error`]: error['details']});
      });
    }
  }
}
