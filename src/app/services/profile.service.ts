import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProfileService {
  private apiUrl = environment.apiUrl;

  private getSellerDetailsUri = 'api/seller/profile';
  private updateSellerDetailsUri = 'api/seller/profile';

  private getUserDetailsUri = 'api/v1/user';
  private updateUserDetailsUri = 'api/v1/user';
  private deleteUserUri = 'api/v1/user';
  private changePasswordUri = 'api/v1/user/password';

  private getSavedWorkSpacesUri = 'api/v1/user/liked';

  private getBookingRequestsUri = 'api/v1/user/requests';
  private cancelBookingRequestUri = 'api/v1/user/booking/$id/cancel';
  private cancelViewingRequestUri = 'api/v1/user/viewing/$id/cancel';

  private likeWorkSpaceUri = 'api/v1/user/like/$id';

  constructor(private http: HttpClient) {
  }

  getSellerDetails() {
    return this.http.get(this.apiUrl + this.getSellerDetailsUri);
  }

  updateSellerDetails(userDetails) {
    return this.http.put(this.apiUrl + this.updateSellerDetailsUri, userDetails);
  }

  getUserDetails() {
    return this.http.get(this.apiUrl + this.getUserDetailsUri);
  }

  deleteUser() {
    return this.http.delete(this.apiUrl + this.deleteUserUri);
  }

  updateUserDetails(userDetails) {
    return this.http.put(this.apiUrl + this.updateUserDetailsUri, userDetails);
  }

  changePassword(data) {
    return this.http.patch(this.apiUrl + this.changePasswordUri, data);
  }

  getSavedWorkSpaces() {
    return this.http.get(this.apiUrl + this.getSavedWorkSpacesUri, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  getBookingRequests() {
    return this.http.get(this.apiUrl + this.getBookingRequestsUri);
  }

  cancelBookingRequest(id) {
    return this.http.patch(this.apiUrl + this.cancelBookingRequestUri.replace('$id', id), {});
  }

  cancelViewingRequest(id) {
    return this.http.patch(this.apiUrl + this.cancelViewingRequestUri.replace('$id', id), {});
  }

  likeWorkSpace(id) {
    return this.http.post(this.apiUrl + this.likeWorkSpaceUri.replace('$id', id), {});
  }

}
