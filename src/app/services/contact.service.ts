import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactService {
  private apiUrl = environment.apiUrl;
  private contactUri = 'api/contact-us';
  private leasholdUri = 'api/leasehold-enquiry';

  constructor(private http: HttpClient) { }

  sendMessage(data) {
    return this.http.post(this.apiUrl + this.contactUri, data);
  }

  sendLeasehold(data) {
    return this.http.post(this.apiUrl + this.leasholdUri, data);
  }

}
