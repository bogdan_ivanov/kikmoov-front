import { ErrorsService } from './errors.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { UniversalStorage } from '@shared/storage/universal.storage';

@Injectable()
export class OauthService {
  private redirectLink = window.location.origin;
  private linkedInApiUrl = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${
    environment.oauth.linkedIn.CLIENT_ID
  }&redirect_uri=${this.redirectLink}&state=9876524352&scope=r_liteprofile%20r_emailaddress`;

  private apiUrl = environment.apiUrl;
  private getAccessTokenUri = 'oauth/v2/token';
  private socialAuthUri = 'api/$provider/check';
  private linkedInAuthUri = 'api/linkedin/check';

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private errorService: ErrorsService,
    private universalStorage: UniversalStorage
  ) {}

  getLinkedInAuthLink() {
    return this.linkedInApiUrl;
  }

  doSocialAuth(
    data: {
      authToken: string;
      email: string;
      firstName: string;
      lastName: string;
    },
    provider: string
  ) {
    return this.http.post(
      this.apiUrl + this.socialAuthUri.replace('$provider', provider),
      data,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  doLinkedInAuth(data: { code: string }) {
    return this.http.post(this.apiUrl + this.linkedInAuthUri, data, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  getAccessToken(data) {
    return this.http.get(
      `${this.apiUrl}${this.getAccessTokenUri}?username=${
        data.email
      }&password=${data.socialToken}&grant_type=password&client_id=${
        data.client_id
      }&client_secret=${data.client_secret}`,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  authenticate(clientData, action?, options?) {
    if (clientData['client_id'] && clientData['client_secret']) {
      this.getAccessToken(clientData).subscribe(
        result => {
          result['accountType'] = clientData['accountType'];
          result['client_id'] = clientData['client_id'];
          result['client_secret'] = clientData['client_secret'];
          if (options && options.guest_login_as_seller && result['accountType'] === 'seller') {
            options.setMessage(true);
            options.login.loading = false;
            return;
          } else if (options && options.guest_login_as_seller && result['accountType'] === 'buyer') {
            this.userService.doUserAuth(result);
            this.universalStorage.setItem('email', clientData.email);
            if (action && typeof action !== 'function') {
              action.emit({ action: 'close', loggedIn: true });
            } else if (action && typeof action === 'function') {
              action({ action: 'close', loggedIn: true });
            }
            options.login.loading = false;
            return;
          }
          this.userService.doUserAuth(result);
          this.universalStorage.setItem('email', clientData.email);
          if (result['accountType'] === 'seller') this.router.navigate(['/mainportal']);
          if (action && typeof action !== 'function') {
            action.emit({ action: 'close', loggedIn: true });
          } else if (action && typeof action === 'function') {
            action({ action: 'close', loggedIn: true });
          }
        },
        err => {
          console.log(err);
          this.errorService.setErrors(err);
          this.errorService.showError();
          this.errorService.clearErrors();
        }
      );
    }
  }
}
