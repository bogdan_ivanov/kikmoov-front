import {Observable, EMPTY, BehaviorSubject} from 'rxjs';
import {Injectable, Inject, PLATFORM_ID, Optional} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/do';

import {environment} from '../../environments/environment';
import {ErrorsService} from './errors.service';
import {UniversalStorage} from '@shared/storage/universal.storage';
import {isPlatformBrowser} from '@angular/common';
// import {RESPONSE} from '@nguniversal/express-engine/tokens';

// import { Response as expressResponse } from 'express';
// import { RESPONSE } from '@nguniversal/express-engine/tokens';

@Injectable()
export class UserService {
  private apiUrl = environment.apiUrl;
  private frontUrl = environment.host;
  private loginUri = 'api/login';
  private forgotPassUri = 'api/forgot-password';
  private changePassUri = 'api/password/';
  private checkRegistration = 'api/check-email';
  private registrationSeller = 'api/registration/seller';
  private registrationBuyer = 'api/registration/buyer';
  private confirmAcc = 'api/confirm/$token';
  private loginByToken = 'api/user/oauth2/verify';
  private getAccessTokenByClientData = 'oauth/v2/token';

  public loginStatusSubj = new BehaviorSubject(null);
  public userSubject = new BehaviorSubject(null);

  public userData: {
    email: string;
    firstname?: string;
    lastname?: string;
    phone?: string;
    password?: string;
  };

  private isLoggedIn = false;
  // private isLoggedIn;

  private isBrowser;

  constructor(
    private router: Router,
    private http: HttpClient,
    private errorService: ErrorsService,
    @Inject(PLATFORM_ID) private platformId: Object,
    private universalStorage: UniversalStorage,
    // @Optional() @Inject(RESPONSE) private response: expressResponse
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  doLogin(data: Object) {
    const body = {email: data['email'], password: data['password']};
    return this.http.post(this.apiUrl + this.loginUri, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  doSocialAuth(token: string) {
    const body = {accessToken: token};
    return this.http.post(this.apiUrl + this.loginByToken, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  doRefreshToken(params: {
    refresh_token: string;
    client_id: string;
    client_secret: string;
  }) {
    return this.http.get(
      `${this.apiUrl}${this.getAccessTokenByClientData}?refresh_token=${
        params.refresh_token
        }&grant_type=refresh_token&client_id=${params.client_id}&client_secret=${
        params.client_secret
        }`
    );
  }

  confirmUser(data: { token: string }) {
    return this.http.get(this.apiUrl + this.confirmAcc.replace('$token', data['token']), {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  registerBuyer(data: Object) {
    const body = {
      firstname: data['firstname'],
      lastname: data['lastname'],
      email: data['email'],
      password: data['password'],
      accountType: 'buyer',
    };
    return this.http.post(this.apiUrl + this.registrationBuyer, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  canSellerRegister(email: string) {
    const body = {
      email,
    };
    return this.http.post(this.apiUrl + this.checkRegistration, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  registerSeller(user: Object, invoice: Object) {
    const body = {
      firstname: user['firstname'],
      lastname: user['lastname'],
      email: user['email'],
      password: user['password'],
      phone: user['phone'],
      accountType: 'seller',
      companyName: invoice['companyName'],
      town: invoice['town'],
      address: invoice['address'],
      optionalAddress: invoice['optionalAddress'],
      invoiceEmail: invoice['invoiceEmail'],
      postcode: invoice['postcode'],
    };
    return this.http.post(this.apiUrl + this.registrationSeller, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  refreshToken(params?: {
    refresh_token: string;
    client_id: string;
    client_secret: string;
  }) {
    if (!params) {
      params = this.getClientData();
    }
    if (!false) {
      return EMPTY;
    }

    return this.doRefreshToken(params)
      .do(
        res => {
          if (res['access_token'] && res['refresh_token']) {
            const expires_at =
              +(Date.now() / 1000).toFixed(0) + res['expires_in'];
            this.universalStorage.setItem('access_token', res['access_token']);
            this.universalStorage.setItem('expires_in', expires_at);
            this.universalStorage.setItem('refresh_token', res['refresh_token']);
            this.universalStorage.setItem('client_id', params.client_id);
            this.universalStorage.setItem('client_secret', params.client_secret);
          }
        },
        err => {
          throw new err;
        }
      );
  }

  forgetPassword(data: Object) {
    const body = {email: data['email']};

    return this.http.post(this.apiUrl + this.forgotPassUri, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  changePassword(data: Object, pin: string) {
    const body = {password: data['password']};
    return this.http.post(this.apiUrl + this.changePassUri + pin, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  getAccessToken(client_data, credentials) {
    return this.http.get(
      `${this.apiUrl}${this.getAccessTokenByClientData}?username=${
        credentials.email
        }&password=${credentials.password}&grant_type=password&client_id=${
        client_data.client_id
        }&client_secret=${client_data.client_secret}`,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      }
    );
  }

  signOut() {
    if (this.isBrowser) {
      const cookies = localStorage.getItem('confirmed_cookies');
      localStorage.clear();
      //
      this.universalStorage.clear();
      this.universalStorage.removeByArrayOfKeys([
          'access_token', 'expires_in', 'refresh_token', 'client_id', 'client_secret', 'userData'
      ]);
      this.userData = undefined;
      this.userSubject.next(null);
      if (cookies) {
        localStorage.setItem('confirmed_cookies', cookies);
      }
      if (this.router.url.match(/profile/ig)) {
        this.router.navigate(['/']);
      }
    } else {
      const cookies = this.universalStorage.getItem('confirmed_cookies');
      this.universalStorage.clear();
      this.userData = undefined;
      this.userSubject.next(null);
      if (cookies) {
        this.universalStorage.setItem('confirmed_cookies', cookies);
      }
      if (this.router.url.match(/profile/ig)) {
        this.router.navigate(['/']);
      }
    }
  }

  setLogoutAndRedirect(error?): Promise<boolean> {
    const redirectUrl = '/';

    if (this.isBrowser) {
      localStorage.clear();
      this.userData = undefined;
      this.userSubject.next(null);
      this.loginStatusSubj.next(null);
      this.universalStorage.removeByArrayOfKeys([
          'access_token', 'expires_in', 'refresh_token', 'client_id', 'client_secret', 'userData'
      ]);
    } else {
      this.userData = undefined;
      this.userSubject.next(null);
      this.loginStatusSubj.next(null);
      this.universalStorage.removeByArrayOfKeys([
        'access_token', 'expires_in', 'refresh_token', 'client_id', 'client_secret', 'userData'
      ]);
    }

    if (error) {
      if (error.message && error.message.length > 0 ) {
          this.errorService.setErrors({
              message: error.message,
          });
      } else {
          this.errorService.setErrors({
              message: 'You were logged out, please, log in again',
          });
          return this.router.navigate([redirectUrl]);
      }
    }

    this.router.navigate([redirectUrl]);
  }

  doUserAuth(authData) {
    this.loginStatusSubj.next({
      type: authData.accountType,
      client_id: authData.client_id,
    });
    const expires_in = +(Date.now() / 1000).toFixed(0) + authData.expires_in;
    this.universalStorage.setItem('access_token', authData.access_token);
    this.universalStorage.setItem('expires_in', expires_in);
    this.universalStorage.setItem('refresh_token', authData.refresh_token);
    this.universalStorage.setItem('accountType', authData.accountType);
    this.universalStorage.setItem('client_id', authData.client_id);
    this.universalStorage.setItem('client_secret', authData.client_secret);
  }

  getToken(): string {
    if (this.isBrowser) {
      return localStorage.getItem('access_token');
    } else {
      return this.universalStorage.getItem('access_token');
    }
  }

  getUserType() {
    if (this.isBrowser) {
      return localStorage.getItem('accountType');
    } else {
      return this.universalStorage.getItem('accountType');
    }
  }

  public isNeedRefreshToken() {
    if (this.isBrowser) {
      const expires_in = localStorage.getItem('expires_in');
      if (!expires_in) {
        return false;
      }
      const expiresAt = JSON.parse(expires_in);
      return ((new Date()).getTime() / 1000) > (expiresAt - 60);
    } else {
      const expires_in = this.universalStorage.checkItem('expires_in') ? this.universalStorage.getItem('expires_in') : false;
      if (!expires_in) {
        return false;
      }
      const expiresAt = JSON.parse(expires_in);
      return ((new Date()).getTime() / 1000) > (expiresAt - 60);
    }
  }

  isLoggedin() {
    // console.log('isLoggedin');
    // if (this.isBrowser) {
      const access_token = this.universalStorage.getItem('access_token');
      const refresh_token = this.universalStorage.getItem('refresh_token');
      const client_data = this.getClientData();
      this.isLoggedIn =
        client_data !== null &&
        access_token !== null &&
        refresh_token !== null;
      return this.isLoggedIn;
    // } else {
    //   const access_token = this.universalStorage.checkItem('access_token') ? this.universalStorage.getItem('access_token') : null;
    //   const refresh_token = this.universalStorage.checkItem('refresh_token') ? this.universalStorage.getItem('refresh_token') : null;
    //   const client_data = this.getClientData();
    //
    //   // this.isLoggedIn =
    //   //   client_data !== null &&
    //   //   access_token !== null &&
    //   //   refresh_token !== null;
    //
    //   console.log(this.universalStorage.getAllCookies());
    //   this.isLoggedIn = this.universalStorage.checkItem("auth");
    //
    //   return this.isLoggedIn;
    // }
  }

  loginStatus(): Observable<Object> {
    return this.loginStatusSubj.asObservable();
  }

  setUserData(data) {
    data['fullname'] = (data.firstname ? data.firstname + ' ' : '') +
      (data.lastname ? data.lastname : '');
    if (this.isBrowser) {
      localStorage.setItem('userData', JSON.stringify(data));
    } else {
      this.universalStorage.setItem('userData', JSON.stringify(data));
    }
    this.userSubject.next(data);
    this.userData = data;
  }

  getUserData() {
    if (this.isBrowser) {
      if (localStorage.getItem('userData')) {
        this.userData = JSON.parse(localStorage.getItem('userData'));
      }
    } else {
      if (!this.userData && this.universalStorage.checkItem('userData')) {
        this.userData = JSON.parse(this.universalStorage.getItem('userData'));
      }
    }
    this.userSubject.next(this.userData);
    return this.userData;
  }

  clearUserData() {
    this.userData = {email: null};
  }

  private getClientData() {
    if (this.isBrowser) {
      if (
        localStorage.getItem('client_id') !== null &&
        localStorage.getItem('client_secret') !== null &&
        localStorage.getItem('refresh_token') !== null
      ) {
        return {
          client_id: localStorage.getItem('client_id'),
          client_secret: localStorage.getItem('client_secret'),
          refresh_token: localStorage.getItem('refresh_token'),
        };
      }
    } else {
      if (
        this.universalStorage.checkItem('client_id') &&
        this.universalStorage.checkItem('client_secret') &&
        this.universalStorage.checkItem('refresh_token')
      ) {
        return {
          client_id: this.universalStorage.getItem('client_id'),
          client_secret: this.universalStorage.getItem('client_secret'),
          refresh_token: this.universalStorage.getItem('refresh_token'),
        };
      }
    }

    return null;
  }
}
