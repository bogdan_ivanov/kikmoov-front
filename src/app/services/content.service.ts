import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/index';

@Injectable()
export class ContentService {
  private apiUrl = environment.apiUrl;
  private getTestimonialsUri = 'api/testimonials';
  private getProudToWorkUri = 'api/proud-to-work';

  constructor(private http: HttpClient) { }

  getTestimonials() {
    return this.http.get(this.apiUrl + this.getTestimonialsUri).pipe(catchError(error => of(error)));
  }

  getProudToWork() {
    return this.http.get(this.apiUrl + this.getProudToWorkUri);
  }

}
