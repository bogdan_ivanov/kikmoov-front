import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  public message = '';
  constructor(private snackBar: MatSnackBar) {}

  hasMessage() {
    return this.message.length > 0;
  }

  setMessage(message: string) {
    this.message = message;
  }

  getMessage() {
    return this.message;
  }

  clearMessage() {
    this.message = '';
  }

  showMessage(message?) {
    message = message || this.message;
    this.snackBar.open(message, 'OK', {
      duration: 5000,
      panelClass: ['matSuccess']
    });
    this.clearMessage();
  }
}
