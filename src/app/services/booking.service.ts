import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BookingService {
  private apiUrl = environment.apiUrl;
  private addViewingUri = 'api/v1/workspace/$id/viewing';
  private addBookingUri = 'api/v1/workspace/$id/booking';
  private inviteAttendeeBookingUri = 'api/v1/user/booking/$id/invite-attendee';
  private addBookingRequestUri = 'api/v1/workspace/$id/booking-request';
  private checkBookingAvailableUri = 'api/workspace/$id/booking-available';
  private addToCalendarUri = 'export/$id/booking';

  constructor(private http: HttpClient) { }

  addViewing(workSpaceId, data) {
    const body = {
      startTime: data['startTime'],
      phone: data['phone'],
      email: data['email']
    };
    body['startTime'] = <any>this.convertToUtc(null, (body['startTime'] * 1000)) / 1000;
    return this.http.post(this.apiUrl + this.addViewingUri.replace('$id', workSpaceId), body);
  }

  addBooking(workSpaceId, data, token) {
    const body = {
      startTime: data['startTime'],
      endTime: data['endTime'],
      phone: data['phone'],
      email: data['email'],
      name: data['name'] || 'desk',
      paymentToken: token,
      billingAddress: data['address'],
      city: data['city'],
      postCode: data['postcode']
    };
    body['startTime'] = <any>this.convertToUtc(null, (body['startTime'] * 1000)) / 1000;
    body['endTime'] = <any>this.convertToUtc(null, (body['endTime'] * 1000)) / 1000;
    return this.http.post(this.apiUrl + this.addBookingUri.replace('$id', workSpaceId), body);
  }

  inviteAttendeeBooking(workSpaceId, data) {
    const body = {
      emails: data
    };
    return this.http.post(this.apiUrl + this.inviteAttendeeBookingUri.replace('$id', workSpaceId), body);
  }

  addToCalendar(workspaceId) {
      return this.apiUrl + this.addToCalendarUri.replace('$id', workspaceId);
  }

  addBookingRequest(workSpaceId, data) {
    if (data['startDate'] instanceof Date) {
      const tzOffset = Math.abs(data['startDate'].getTimezoneOffset());
      data['startDate'] = data['startDate'].getTime();
      data['startDate'] = data['startDate'] + (tzOffset * 60000);
    }
    let body;
    if (data['units']) {
        body = {
            startTime: +(data['startDate'] / 1000).toFixed(0),
            phone: data['phone'],
            email: data['email'],
            name: data['name'],
            information: data['information'],
            duration: data['duration'],
            tenantType: data['tenantType'],
            units: data['units']
        };
    } else {
        body = {
            startTime: +(data['startDate'] / 1000).toFixed(0),
            phone: data['phone'],
            email: data['email'],
            name: data['name'],
            information: data['information'],
            duration: data['duration'],
            tenantType: data['tenantType']
        };
    }
    return this.http.post(this.apiUrl + this.addBookingRequestUri.replace('$id', workSpaceId), body);
  }

  checkBookingAvailable(workSpaceId, data) {
    return this.http.post(this.apiUrl + this.checkBookingAvailableUri.replace('$id', workSpaceId), data);
  }

  convertToUtc(date: Date, time?: number): Date | number {
    if (time) {
      // const tzOffset = Math.abs(new Date().getTimezoneOffset());
      const startDate = time;
      // startDate = (startDate + (tzOffset * 60000));
      return new Date(startDate).getTime();
    } else {
      // const tzOffset = Math.abs(date.getTimezoneOffset());
      const startDate = date.getTime();
      // startDate = (startDate - (tzOffset * 60000));
      return new Date(startDate)
    }
  }

}
