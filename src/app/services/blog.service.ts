import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { TransferHttpService } from '@gorniv/ngx-transfer-http';
import { of } from 'rxjs';

@Injectable()
export class BlogService {

  private apiUrl = environment.apiUrl;
  public blogUri = 'api/blog/categories';
  public categoryUri = 'api/blog/category/$slug';
  public articleUri = 'api/blog/article/$slug';
  public lastArticleUri = 'api/blog/last-article/';
  public similarArticlesUri = 'api/blog/similar/articles/';

  constructor(private http: TransferHttpService, private httpClient: HttpClient) {}

  public getCategories() {
    return this.http.get(this.apiUrl + this.blogUri);
  }

  public getLastArticle() {
    return this.http.get(this.apiUrl + this.lastArticleUri);
  }

  public getCategory(slug) {
    return this.http.get<{articles}>(this.apiUrl + this.categoryUri.replace('$slug', slug))
        .pipe(
        map(({articles}: any) => {
          articles.map(article => {
            article.categorySlug = slug;
            return article;
          });
          return {articles};
        }),
        catchError(error => of(error))
  );
  }

  public getArticleBySlug(articleSlug) {
    return this.http.get(this.apiUrl + this.articleUri.replace('$slug', articleSlug));
  }

  public getSimilarArticles(categorySlug, articleId) {
    return this.http.get(this.apiUrl + this.similarArticlesUri + categorySlug + '/' + articleId);
  }

}
