import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import * as moment from 'moment-timezone';
import {catchError, map} from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import {of} from 'rxjs/index';

@Injectable()
export class LocationService {
  private apiUrl = environment.apiUrl;

  private getLocationsUri = 'api/seller/locations';
  private getLocationUri = 'api/seller/location/$id';
  private createUri = 'api/seller/location';
  private updateUri = 'api/seller/location/$id';
  private deleteUri = 'api/seller/location/$id';

  private fstListUri = 'api/facilities';
  private uploadMediaUri = 'api/seller/workspace/$id/media';
  private setMainMediaUri = 'api/seller/workspace/$id/cover/$mediaId';
  private deleteMediaUri = 'api/seller/workspace/$id/media/$mediaId';

  private getSellerOfficeUri = 'api/seller/workspace/$id';
  private updateOfficeUri = 'api/seller/workspace/$id';
  private deleteOfficeUri = 'api/seller/workspace/$id';
  private getOfficeUri = 'api/workspace/$id';

  private getTopWorkSpacesUri = 'api/top-workspaces';

  private getBookingListUri = 'api/seller/workspace/$id/booking-list';
  private getViewingListUri = 'api/seller/workspace/$id/viewing-list';

  private getAreaSuggestionsUri = 'api/areas';

  private updateWorkspaceStatus = 'api/seller/workspace/$id/status';

  constructor(private http: HttpClient) {
  }

  /* General data block */
  getLocations() {
    return this.http.get(this.apiUrl + this.getLocationsUri);
  }

  getLocation(id) {
    return this.http.get(
      this.apiUrl + this.getLocationUri.replace('$id', id),
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  createLocation(data) {
    data = this.coordinatesPrepare(data);
    return this.http.post(this.apiUrl + this.createUri, data, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  updateLocation(data, locationId) {
    data = this.coordinatesPrepare(data);
    return this.http.put(
      this.apiUrl + this.updateUri.replace('$id', locationId),
      data,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  deleteLocation(locationId) {
    return this.http.delete(this.apiUrl + this.deleteUri.replace('$id', locationId));
  }

  /* Facilites block */
  getServiceFacilites() {
    return this.http.get(this.apiUrl + this.fstListUri, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  /* Image block */
  uploadImage(wsId, imageData) {
    const uploadData = new FormData();
    for (const image in imageData) {
      if (imageData.hasOwnProperty(image)) {
        uploadData.set(`image[${image}]`, imageData[image]);
      }
    }
    return this.http.post(
      this.apiUrl + this.uploadMediaUri.replace('$id', wsId),
      uploadData
    );
  }

  setMainImage(wsId, imageId) {
    return this.http.patch(
      this.apiUrl + this.setMainMediaUri.replace('$id', wsId).replace('$mediaId', imageId),
      {}
    );
  }

  changeStatus(wsId, status) {
      return this.http.patch(this.apiUrl + this.updateWorkspaceStatus
          .replace('$id', wsId), {
          status
      });
  }

  deleteOfficeImage(wsId, imageId) {
    return this.http.delete(
      this.apiUrl + this.deleteMediaUri.replace('$id', wsId).replace('$mediaId', imageId),
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  /* ==================== Office CRUD ========================================= */

  getSellerOffice(id) {
    return this.http.get(this.apiUrl + this.getSellerOfficeUri.replace('$id', id), {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  getOffice(id) {
    return this.http.get(this.apiUrl + this.getOfficeUri.replace('$id', id), {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  createOffice(wsId, officeData) {
    let data = {...officeData};
    data = this.prepareData(data);
    return this.http.post(
      `${this.apiUrl}${this.createUri}/${wsId}/workspace`,
      data,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }
    );
  }

  updateOffice(data, id) {
    data = this.prepareData(data);
    return this.http.put(this.apiUrl + this.updateOfficeUri.replace('$id', id), data);
  }

  deleteOffice(id) {
    return this.http.delete(this.apiUrl + this.deleteOfficeUri.replace('$id', id));
  }

  getTopWorkSpaces() {
    return this.http.get(this.apiUrl + this.getTopWorkSpacesUri).pipe(catchError(error => of(error)));
  }

  coordinatesPrepare(data) {
    data['latitude'] = Math.abs(data['latitude']) === +data['latitude'] ?
      `+${(+data['latitude']).toFixed(5)}` :
      `${(+data['latitude']).toFixed(5)}`;
    data['longitude'] = Math.abs(data['longitude']) === +data['longitude'] ?
      `+${(+data['longitude']).toFixed(5)}` :
      `${(+data['longitude']).toFixed(5)}`;
    return data;
  }

  // Bookings popup
  getBookingList(workSpaceId) {
    const bookings = this.http.get(this.apiUrl + this.getBookingListUri.replace('$id', workSpaceId))
      .pipe(map(res => res['bookingList']));
    const viewings = this.http.get(this.apiUrl + this.getViewingListUri.replace('$id', workSpaceId))
      .pipe(map(res => res['viewingList']));
    return forkJoin(bookings, viewings).pipe(map(res => {
      res[0].map((booking: any) => {
        booking.type = 'booking';
      }
      );
      res[1].map(viewing => {
        viewing.type = 'viewing';
      });
      res = res[0].concat(res[1]).sort((a, b) => a.startTime - b.startTime);
      return res;
    }));
  }

  getAreaSuggestions() {
    return this.http.get(
      `${this.apiUrl}${this.getAreaSuggestionsUri}`
    );
  }

  prepareData(data) {
    for (const field in data) {
      if (data.hasOwnProperty(field)) {
        if (field === 'price') {
          data[field] = parseFloat(data[field]);
        }
        if (field === 'availableFrom' && data[field] instanceof moment) {
          data[field] = new Date(data[field]);
        }
        if (field === 'availableFrom' && data[field] instanceof Date) {
          // data[field].setHours(23, 59, 59);
          // data[field] = (+data[field].getTime() / 1000).toFixed();
          const tzOffset = Math.abs(data['availableFrom'].getTimezoneOffset());
          data['availableFrom'] = data['availableFrom'].getTime();
          data['availableFrom'] = (data['availableFrom'] + (tzOffset * 60000)) / 1000;
        }
          if (field === 'size') {
              data[field] = parseFloat(data[field]);
          }
        if (typeof data[field] === 'string' && data[field] !== '' && !isNaN(data[field])) {
          data[field] = Number(data[field]);
        }
        if (data['deskType'] && data['deskType'].includes('hourly')) {
          data['minContractLength'] = null;
          data['availableFrom'] = null;
        } else if (data['deskType'] && data['deskType'].includes('monthly')) {
          data['opensFrom'] = null;
          data['closesAt'] = null;
        }
      }
    }
    return data;
  }
}
