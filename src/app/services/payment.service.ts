import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import { environment } from '../../environments/environment';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class PaymentService {
  private env = environment;
  private isBrowser;
  public stripe;

  constructor(@Inject(PLATFORM_ID) private platformId) {
      this.isBrowser = isPlatformBrowser(platformId);
      if (this.isBrowser) {
        this.stripe = Stripe(this.env.stripePublishableKey);
      }
  }

    // public stripe = Stripe(this.env.stripePublishableKey);
}
