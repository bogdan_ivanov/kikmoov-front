import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PostcodesService {
  private host = 'https://api.postcodes.io';

  private getPostcodeDataUri = '/postcodes/$postcode';
  private validatePostcodeUri = '/postcodes/$postcode/validate';

  constructor(private http: HttpClient) {
  }

  getPostcodeData(postcode: string): Observable<PostcodeData> {
    return this.http
      .get<PostcodeData>(
        this.host + this.getPostcodeDataUri
          .replace('$postcode', postcode)
      ).map(response => response['result']);
  }

  validatePostCode(postcode: string): Promise<boolean> {
    return this.http
      .get<boolean>(
        this.host + this.validatePostcodeUri
          .replace('$postcode', postcode)
      ).map(response => response['result'])
      .toPromise();
  }

}

export interface PostcodeData {
  postcode: string;

  // Positional Quality. Shows the status of the assigned grid reference.
  // 1 = within the building of the matched address closest to the postcode mean
  // 2 = as for status value 1, except by visual inspection of Landline maps (Scotland only)
  // 3 = approximate to within 50m
  // 4 = postcode unit mean (mean of matched addresses with the same postcode, but not snapped to a building)
  // 5 = imputed by ONS, by reference to surrounding postcode grid references
  // 6 = postcode sector mean, (mainly | PO Boxes)
  // 8 = postcode terminated prior to Gridlink® initiative, last known ONS postcode grid reference1
  // 9 = no grid reference available
  quality: number;

  eastings: number | null;

  northings: number | null;

  country: string;

  nhs_ha: string | null;

  admin_county: string | null;

  admin_district: string | null;

  admin_ward: string | null;

  longitude: number;

  latitude: number;

  parliamentary_constituency: string | null;

  european_electoral_region: string | null;

  primary_care_trust: string | null;

  region: string | null;

  parish: string | null;

  lsoa: string | null;

  msoa: string | null;

  ced: string | null;

  ccg: string | null;

  nuts: string | null;

  codes: {
    admin_district: string | null;
    admin_county: string | null;
    admin_ward: string | null;
    parish: string | null;
    ccg: string | null;
    nuts: string | null;
  };

}
