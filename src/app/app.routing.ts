import { Routes, RouterModule } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

import { WrapperComponent } from '@shared/layouts/wrapper/wrapper.component';
import {LinkedinAppLoginModule} from './modules/linkedin-app-login/linkedin-app-login.module';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard],
    children: [
      {
        path: 'terms',
        loadChildren: './modules/terms/terms.module#TermsModule'
      },
      {
        path: 'indepth/:id',
        loadChildren: './modules/indepth/indepth.module#IndepthModule'
      },
      {
        path: 'contact',
        loadChildren: './modules/contact/contact.module#ContactModule'
      },
      {
        path: 'pws',
        loadChildren: './modules/pws-landing/pws-landing.module#PwsLandingModule'
      },
      {
        path: 'postspace',
        loadChildren: './modules/postspace-page/postspace-page.module#PostspacePageModule'
      },
      {
        path: 'findaspace',
        loadChildren: './modules/findaspace/findaspace.module#FindaspaceModule'
      },
      {
        path: 'mainportal',
        loadChildren: './modules/mainportal/mainportal.module#MainportalModule',
      },
      {
        path: 'login',
        loadChildren: './modules/login/login.module#LoginModule',
      },
      {
        path: 'login/:key',
        loadChildren: './modules/login/login.module#LoginModule',
      },
      {
        path: 'signup',
        loadChildren: './modules/signup/signup.module#SignupModule',
      },
      {
        path: 'signin/seller',
        loadChildren: './modules/signinseller/signinseller.module#SigninsellerModule',
      },
      {
        path: 'signup/seller',
        loadChildren: './modules/signupseller/signupseller.module#SignupsellerModule',
      },
      {
        path: 'invoicing',
        loadChildren: './modules/signinvoice/signinvoice.module#SigninvoiceModule',
      },
      {
        path: 'confirm/:pin',
        loadChildren: './modules/confirm/confirm.module#ConfirmModule',
      },
      {
        path: 'upload-location/:id/:office/:officeId',
        loadChildren: './modules/upload-location/upload-location.module#UploadLocationModule',
        // canActivate: [AuthGuard, SellerGuard]
      },
      {
          path: 'upload-location/:id/:office',
          loadChildren: './modules/upload-location/upload-location.module#UploadLocationModule',
          // canActivate: [AuthGuard, SellerGuard]
      },
      {
          path: 'upload-location/:id',
          loadChildren: './modules/upload-location/upload-location.module#UploadLocationModule',
          // canActivate: [AuthGuard, SellerGuard]
      },
      {
          path: 'upload-location',
          loadChildren: './modules/upload-location/upload-location.module#UploadLocationModule',
          // canActivate: [AuthGuard, SellerGuard]
      },
      {
          path: 'mainportal/profile/change-password',
          loadChildren: './modules/mainportal-profile/mainportal-profile.module#MainportalProfileModule'
      },
      {
        path: 'mainportal/profile',
        loadChildren: './modules/mainportal-profile/mainportal-profile.module#MainportalProfileModule'
      },
      {
        path: 'profile',
        loadChildren: './modules/buyer-profile/buyer-profile.module#BuyerProfileModule'
      },
      {
        path: 'profile/change-password',
        loadChildren: './modules/buyer-change-password/buyer-change-password.module#BuyerChangePasswordModule'
      },
      {
        path: 'profile/bookings',
        loadChildren: './modules/bookings-profile/bookings-profile.module#BookingsProfileModule'
      },
      {
        path: 'profile/saved-workspaces',
        loadChildren: './modules/saved-workspaces/saved-workspaces.module#SavedWorkspacesModule'
      },
      {
        path: 'mainportal/notifications',
        loadChildren: './modules/mainportal-notifications/mainportal-notifications.module#MainportalNotificationsModule'
      },
      {
        path: 'how-it-works',
        loadChildren: './modules/how-it-works/how-it-works.module#HowItWorksModule'
      },
      {
        path: 'recover/:pin',
        loadChildren: './modules/recover-password/recover-password.module#RecoverPasswordModule'
      },
      {
        path: 'blog/article/:articleSlug',
        loadChildren: './modules/blog/blog-article/blog-article.module#BlogArticleModule'
      },
      {
        path: 'blog',
        loadChildren: './modules/blog/nav-community/nav-community.module#NavCommunityModule'
      },
      {
        path: 'app/login',
        loadChildren: './modules/linkedin-app-login/linkedin-app-login.module#LinkedinAppLoginModule'
      },
      {
        path: '',
        loadChildren: './modules/home/home.module#HomeModule'
      },
      { path: '**', loadChildren: './not-found/not-found.module#NotFoundModule' },
    ],
  },
];
// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes, { initialNavigation: 'enabled' });
