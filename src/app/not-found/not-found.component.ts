import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotFoundService } from './not-found.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, OnDestroy {

  private timer;
  public status: { code: number; message: string };

  constructor(
      private router: Router,
      private _notFoundService: NotFoundService
  ) {
  }

  ngOnInit() {
      this._notFoundService.setStatus(404, 'Not Found');
    this.timer = setTimeout(() => {
      this.router.navigate(['/']);
    }, 5000);
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
  }

}
