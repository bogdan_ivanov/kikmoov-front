// angular
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {BrowserModule, BrowserTransferStateModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// libs
import { CookieService, CookieModule } from 'ngx-cookie';
import { TransferHttpCacheModule } from '@nguniversal/common';
// shared
import { SharedModule } from '@shared/shared.module';
import { TranslatesService } from '@shared/translates';
// components
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { UniversalStorage } from '@shared/storage/universal.storage';
import { CoreModule } from './modules/core/core.module';
import { AuthGuard } from './middleware/is-auth.guard';
import { LoginGuard } from './middleware/login.guard';
import { SellerGuard } from './middleware/is-seller.guard';
import { BuyerGuard } from './middleware/is-buyer.guard';
import { CurrencyPipe } from '@angular/common';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { DragndropfileDirective } from './derective/dragndropfile.directive';

export function initLanguage(translateService: TranslatesService): Function {
  return (): Promise<any> => translateService.initLanguage();
}

@NgModule({
  imports: [
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    HttpClientModule,
    RouterModule,
    AppRoutes,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    SharedModule.forRoot(),
      ShareButtonsModule.withConfig({
          include: ['facebook', 'email', 'linkedin', 'twitter'],
          theme: 'circles-light',
          size: -3,
          prop: {
              twitter: {
                  icon: ['fab', 'twitter']
              },
              facebook: {
                  icon: ['fab', 'facebook-f']
              },
              linkedin: {
                  icon: ['fab', 'linkedin-in']
              },
              email: {
                  icon: ['far', 'envelope']
              }
          }
      }),
    CoreModule
  ],
  declarations: [AppComponent, DragndropfileDirective],
  providers: [
    CookieService,
    UniversalStorage,
    { provide: APP_INITIALIZER, useFactory: initLanguage, multi: true, deps: [TranslatesService] },
      AuthGuard,
      LoginGuard,
      SellerGuard,
      BuyerGuard,
      CurrencyPipe
  ],
})
export class AppModule {}
