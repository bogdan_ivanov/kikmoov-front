// angular
import { NgModule, ViewEncapsulation, Component } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
// libs
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
// shared
import { TranslatesServerModule } from '@shared/translates/translates-server';
// components
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { InlineStyleComponent } from './inline-style/inline-style.component';
import { InlineStyleModule } from './inline-style/inline-style.module';
import { CookieService, CookieBackendService } from 'ngx-cookie';
import { UserService } from './services/user.service';
import { ErrorsService } from './services/errors.service';
import { ProfileService } from './services/profile.service';
import { OauthService } from './services/oauth.service';
import { AuthServiceConfig } from 'angularx-social-login';
import { getAuthServiceConfigs } from './configs/auth-service-config';
import { ContactService } from './services/contact.service';
import { MessageService } from './services/message.service';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    // AppModule - FIRST!!!
    AppModule,
    BrowserModule.withServerTransition({
        appId: 'my-app'
    }),
    ServerModule,
    NoopAnimationsModule,
    ServerTransferStateModule,
    InlineStyleModule,
    ModuleMapLoaderModule,
    TranslatesServerModule,
  ],
  bootstrap: [AppComponent, InlineStyleComponent],
  providers: [
    { provide: CookieService, useClass: CookieBackendService },
      UserService,
      ErrorsService,
      ProfileService,
      OauthService,
      ContactService,
      MessageService,
      {
          provide: AuthServiceConfig,
          useFactory: getAuthServiceConfigs
      },
  ],
})
export class AppServerModule {}
